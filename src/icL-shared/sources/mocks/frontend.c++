#include "frontend.h++"

#include <icL-types/replaces/ic-rect.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-il/structures/cookie-data.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/signal.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce-value-base/base/string-value.h++>

#include <iostream>



namespace icL::shared {

void FrontEnd::pushTarget(const il::TargetData & /*target*/) {
    std::cout << "front-end: pushTarget called: " << std::endl;
}

void FrontEnd::popTarget() {
    std::cout << "front-end: popTarget called: " << std::endl;
}

il::TargetData FrontEnd::getCurrentTarget() {
    std::cout << "front-end: getCurrentTarget called: " << std::endl;
    return {"test-session", "test-window"};
}

void FrontEnd::newSession() {
    std::cout << "front-end: newSession called: " << std::endl;
    sessions++;
}

void FrontEnd::switchSession() {
    std::cout << "front-end: switchSession called: " << std::endl;
}

void FrontEnd::closeSession() {
    std::cout << "front-end: closeSession called: " << std::endl;
    sessions--;
}

int FrontEnd::implicitTimeout() {
    std::cout << "front-end: implicitTimeout called: " << std::endl;
    return m_implicitTimeout;
}

int FrontEnd::pageLoadTimeout() {
    std::cout << "front-end: pageLoadTimeout called: " << std::endl;
    return m_pageLoadTimeout;
}

int FrontEnd::scriptTimeout() {
    std::cout << "front-end: scriptTimeout called: " << std::endl;
    return m_scriptTimeout;
}

void FrontEnd::setImplicitTimeout(int ms) {
    std::cout << "front-end: setImplicitTimeout called: " << std::endl;
    m_implicitTimeout = ms;
}

void FrontEnd::setPageLoadTimeOut(int ms) {
    std::cout << "front-end: setPageLoadTimeOut called: " << std::endl;
    m_pageLoadTimeout = ms;
}

void FrontEnd::setScriptTimeout(int ms) {
    std::cout << "front-end: setScriptTimeout called: " << std::endl;
    m_scriptTimeout = ms;
}

void FrontEnd::focusSession(const il::TargetData & /*session*/) {
    std::cout << "front-end: focusSession called: " << std::endl;
}

icList<il::TargetData> FrontEnd::getSessions() {
    std::cout << "front-end: getSessions called: " << std::endl;
    icList<il::TargetData> ret;

    for (int i = 0; i < sessions; i++) {
        ret += {"test-session", "test-window"};
    }

    return ret;
}

void FrontEnd::load(const icString & url) {
    std::cout << "front-end: load called: " << url.toString() << std::endl;
}

icString FrontEnd::url() {
    std::cout << "front-end: url called: " << std::endl;
    return "http://fake.url";
}

void FrontEnd::back() {
    std::cout << "front-end: back called: " << std::endl;
}

void FrontEnd::forward() {
    std::cout << "front-end: forward called: " << std::endl;
}

void FrontEnd::refresh() {
    std::cout << "front-end: refresh called: " << std::endl;
}

icString FrontEnd::title() {
    std::cout << "front-end: title called: " << std::endl;
    return "Fake title";
}

icString FrontEnd::window() {
    std::cout << "front-end: window called: " << std::endl;
    return "test-window";
}

il::TargetData FrontEnd::newWindow() {
    std::cout << "front-end: new window called: " << std::endl;
    return {"test-session", "test-window"};
}

void FrontEnd::closeWindow() {
    std::cout << "front-end: closeWindow called: " << std::endl;
}

void FrontEnd::focusWindow() {
    std::cout << "front-end: focusWindow called: " << std::endl;
}

icList<il::TargetData> FrontEnd::getWindows() {
    std::cout << "front-end: getWindows called: " << std::endl;
    return {{"test-session", "test-window"}};
}

void FrontEnd::switchToFrame(int /*id*/) {
    std::cout << "front-end: switchToFrame called: " << std::endl;
}

void FrontEnd::switchToFrame(const il::Element & /*el*/) {
    std::cout << "front-end: switchToFrame called: " << std::endl;
}

void FrontEnd::switchToParent() {
    std::cout << "front-end: switchToParent called: " << std::endl;
}

icRect FrontEnd::getWindowRect() {
    std::cout << "front-end: getWindowRect called: " << std::endl;
    return windowRect;
}

icRect FrontEnd::setWindowRect(const icRect & rect) {
    std::cout << "front-end: setWindowRect called: " << std::endl;
    return windowRect = rect;
}

void FrontEnd::maximize() {
    std::cout << "front-end: maximize called: " << std::endl;
}

void FrontEnd::minimize() {
    std::cout << "front-end: minimize called: " << std::endl;
}

void FrontEnd::fullscreen() {
    std::cout << "front-end: fullscreen called: " << std::endl;
}

void FrontEnd::restore() {
    std::cout << "front-end: restore called: " << std::endl;
}

il::Element FrontEnd::query(SearchElement & search, const icString & str) {
    std::cout << "front-end: query called: search:" << search.selectorType
              << " tag:" << search.tagName->toString()
              << " selector:" << str.toString() << std::endl;
    return {};
}

il::Element FrontEnd::query(SearchElement & search, const icRegEx & rex) {
    std::cout << "front-end: query called: search:" << search.selectorType
              << " tag:" << search.tagName->toString()
              << " selector:" << rex.pattern().toString() << std::endl;
    return {};
}

il::Elements FrontEnd::queryAll(
  const SearchElements & /*search*/, const icString & /*str*/) {
    std::cout << "front-end: queryAll called." << std::endl;
    return {};
}

il::Elements FrontEnd::queryAll(
  const SearchElements & /*search*/, const icRegEx & /*rex*/) {
    std::cout << "front-end: queryAll called." << std::endl;
    return {};
}

bool FrontEnd::empty(const il::Elements & /*el*/) {
    std::cout << "front-end: empty called: " << std::endl;
    return true;
}

int FrontEnd::length(const il::Elements & /*el*/) {
    std::cout << "front-end: length called: " << std::endl;
    return {};
}

il::Element FrontEnd::active() {
    std::cout << "front-end: active called: " << std::endl;
    return {};
}

bool FrontEnd::selected(const il::Element & /*el*/) {
    std::cout << "front-end: selected called: " << std::endl;
    return false;
}

icString FrontEnd::attr(const il::Element & /*el*/, const icString & /*name*/) {
    std::cout << "front-end: attr called: " << std::endl;
    return {};
}

icStringList FrontEnd::attr(
  const il::Elements & /*el*/, const icString & /*name*/) {
    std::cout << "front-end: attr called: " << std::endl;
    return {};
}

icVariant FrontEnd::prop(
  const il::Element & /*el*/, const icString & /*name*/) {
    std::cout << "front-end: prop called: " << std::endl;
    return new ce::StringValue{nullptr, icString{"prop"}};
}

icVariant FrontEnd::prop(
  const il::Elements & /*el*/, const icString & /*name*/) {
    std::cout << "front-end: prop called: " << std::endl;
    return new ce::StringValue{nullptr, icString{"prop"}};
}

icString FrontEnd::css(const il::Element & /*el*/, const icString & /*name*/) {
    std::cout << "front-end: css called: " << std::endl;
    return {};
}

icString FrontEnd::text(const il::Element & /*el*/) {
    std::cout << "front-end: text called: " << std::endl;
    return "text";
}

icStringList FrontEnd::text(const il::Elements & /*el*/) {
    std::cout << "front-end: text called: " << std::endl;
    return {};
}

icString FrontEnd::name(const il::Element & /*el*/) {
    std::cout << "front-end: name called: " << std::endl;
    return "name";
}

icStringList FrontEnd::name(const il::Elements & /*el*/) {
    std::cout << "front-end: name called: " << std::endl;
    return {};
}

icRect FrontEnd::rect(const il::Element & /*el*/) {
    std::cout << "front-end: rect called: " << std::endl;
    return {0, 0, 100, 100};
}

icRects FrontEnd::rect(const il::Elements & /*el*/) {
    std::cout << "front-end: rect called: " << std::endl;
    return {{0, 0, 100, 100}};
}

bool FrontEnd::enabled(const il::Element & /*el*/) {
    std::cout << "front-end: enabled called: " << std::endl;
    return true;
}

bool FrontEnd::clickable(const il::Element & /*el*/) {
    std::cout << "front-end: clickable called: " << std::endl;
    return true;
}

bool FrontEnd::visible(const il::Element & /*el*/) {
    std::cout << "front-end: visible called: " << std::endl;
    return true;
}

il::Elements FrontEnd::add_11(
  const il::Element & /*el*/, const il::Element & /*toAdd*/) {
    std::cout << "front-end: add called: " << std::endl;
    return {};
}

il::Elements FrontEnd::add_m1(
  const il::Elements & /*el*/, const il::Element & /*toAdd*/) {
    std::cout << "front-end: add called: " << std::endl;
    return {};
}

il::Elements FrontEnd::add_mm(
  const il::Elements & /*el*/, const il::Elements & /*toAdd*/) {
    std::cout << "front-end: add called: " << std::endl;
    return {};
}

void FrontEnd::click(
  const il::Element * /*el*/, const il::ClickData & /*data*/) {
    std::cout << "front-end: click called: " << std::endl;
}

void FrontEnd::clear(const il::Element & /*el*/) {
    std::cout << "front-end: clear called: " << std::endl;
}

il::Element FrontEnd::copy(const il::Element & el) {
    std::cout << "front-end: copy called: " << std::endl;
    return el;
}

il::Elements FrontEnd::copy(const il::Elements & el) {
    std::cout << "front-end: copy called: " << std::endl;
    return el;
}

void FrontEnd::fastType(const il::Element & /*el*/, const icString & /*text*/) {
    std::cout << "front-end: fastType called: " << std::endl;
}

void FrontEnd::forceClick(
  const il::Element & /*el*/, const il::ClickData & /*data*/) {
    std::cout << "front-end: forceClick called: " << std::endl;
}

void FrontEnd::superClick(const il::Element & /*el*/) {
    std::cout << "front-end: superClick called: " << std::endl;
}

void FrontEnd::forceType(
  const il::Element & /*el*/, const icString & /*text*/) {
    std::cout << "front-end: forceType called: " << std::endl;
}

void FrontEnd::hover(
  const il::Element * /*el*/, const il::HoverData & /*hover*/) {
    std::cout << "front-end: hover called: " << std::endl;
}

void FrontEnd::keyDown(
  const il::Element * /*el*/, int /*modifiers*/, const icString & /*keys*/) {
    std::cout << "front-end: keyDown called: " << std::endl;
}

void FrontEnd::keyPress(
  const il::Element * /*el*/, int /*modifiers*/, int /*delay*/,
  const icString & /*keys*/) {
    std::cout << "front-end: keyPress called: " << std::endl;
}

void FrontEnd::keyUp(
  const il::Element * /*el*/, int /*modifiers*/, const icString & /*keys*/) {
    std::cout << "front-end: keyUp called: " << std::endl;
}

void FrontEnd::mouseDown(
  const il::Element * /*el*/, const il::MouseData & /*data*/) {
    std::cout << "front-end: mouseDown called: " << std::endl;
}

void FrontEnd::mouseUp(
  const il::Element * /*el*/, const il::MouseData & /*data*/) {
    std::cout << "front-end: mouseUp called: " << std::endl;
}

void FrontEnd::paste(const il::Element & /*el*/, const icString & /*text*/) {
    std::cout << "front-end: paste called: " << std::endl;
}

void FrontEnd::sendKeys(
  const il::Element * /*el*/, int /*modifiers*/, const icString & /*keys*/) {
    std::cout << "front-end: sendKeys called: " << std::endl;
}

icString FrontEnd::source() {
    std::cout << "front-end: source called: " << std::endl;
    return "<source></source>";
}

icVariant FrontEnd::executeSync(
  const icString & code, const icVariantList & /*args*/) {
    std::cout << "front-end: executeSync called: " << code.toString()
              << std::endl;
    return icVariant::makeVoid();
}

void FrontEnd::executeAsync(
  const icString & code, const icVariantList & /*args*/) {
    std::cout << "front-end: executeAsync called: " << code.toString()
              << std::endl;
}

void FrontEnd::setUserScript(const icString & code) {
    std::cout << "front-end: setUserScript called: " << code.toString()
              << std::endl;
}

void FrontEnd::setAlwaysScript(const icString & code) {
    std::cout << "front-end: setAlwaysScript called: " << code.toString()
              << std::endl;
}

icStringList FrontEnd::getCookies() {
    std::cout << "front-end: getCookies called: " << std::endl;
    return {};
}

il::CookieData FrontEnd::loadCookie(const icString & name) {
    std::cout << "front-end: loadCookie called: " << std::endl;
    il::CookieData cd;

    cd.name = name;
    return cd;
}

void FrontEnd::saveCookie(const il::CookieData & /*data*/) {
    std::cout << "front-end: saveCookie called: " << std::endl;
}

void FrontEnd::deleteCookie(const icString & /*name*/) {
    std::cout << "front-end: deleteCookie called: " << std::endl;
}

void FrontEnd::deleteAllCookies() {
    std::cout << "front-end: deleteAllCookies called: " << std::endl;
}

void FrontEnd::alertDimiss() {
    std::cout << "front-end: alertDimiss called: " << std::endl;
}

void FrontEnd::alertAccept() {
    std::cout << "front-end: alertAccept called: " << std::endl;
}

icString FrontEnd::alertText() {
    std::cout << "front-end: alertText called: " << std::endl;
    return "alertText";
}

void FrontEnd::alertSendText(const icString & /*text*/) {
    std::cout << "front-end: alertSendText called: " << std::endl;
}

icString FrontEnd::screenshot() {
    std::cout << "front-end: screenshot called: " << std::endl;
    return {};
}

icString FrontEnd::screenshot(const il::Element & /*el*/) {
    std::cout << "front-end: screenshot called: " << std::endl;
    return {};
}

il::Element FrontEnd::get(const il::Elements & /*el*/, int /*n*/) {
    std::cout << "front-end: get called: " << std::endl;
    return {};
}

il::Elements FrontEnd::filter(
  const il::Elements & el, const icString & /*selector*/) {
    std::cout << "front-end: filter called: " << std::endl;
    return el;
}

il::Elements FrontEnd::contains(
  const il::Elements & /*el*/, const icString & /*substring*/) {
    std::cout << "front-end: contains called: " << std::endl;
    return {};
}

il::Element FrontEnd::next(const il::Element & el) {
    std::cout << "front-end: next called: " << std::endl;
    return el;
}

il::Element FrontEnd::prev(const il::Element & el) {
    std::cout << "front-end: prev called: " << std::endl;
    return el;
}

il::Element FrontEnd::parent(const il::Element & el) {
    std::cout << "front-end: parent called: " << std::endl;
    return el;
}

il::Element FrontEnd::child(const il::Element & el, int /*n*/) {
    std::cout << "front-end: child called: " << std::endl;
    return el;
}

il::Elements FrontEnd::children(const il::Element & /*el*/) {
    std::cout << "front-end: children called: " << std::endl;
    return {};
}

il::Element FrontEnd::closest(
  const il::Element & el, const icString & /*selector*/) {
    std::cout << "front-end: closest called: " << std::endl;
    return el;
}

int FrontEnd::getClickTime() {
    std::cout << "front-end: getClickTime called: " << std::endl;
    return clickTime;
}

int FrontEnd::getMoveTime() {
    std::cout << "front-end: getMoveTime called: " << std::endl;
    return moveTime;
}

int FrontEnd::getPressTime() {
    std::cout << "front-end: getPressTime called: " << std::endl;
    return pressTime;
}

bool FrontEnd::getFlashMode() {
    std::cout << "front-end: getFlashMode called: " << std::endl;
    return flashMode;
}

bool FrontEnd::getHumanMode() {
    std::cout << "front-end: getHumanMode called: " << std::endl;
    return humanMode;
}

bool FrontEnd::getSilentMode() {
    std::cout << "front-end: getSilentMode called: " << std::endl;
    return silentMode;
}

void FrontEnd::setClickTime(int clickTime) {
    std::cout << "front-end: setClickTime called: " << std::endl;
    this->clickTime = clickTime;
}

void FrontEnd::setMoveTime(int moveTime) {
    std::cout << "front-end: setMoveTime called: " << std::endl;
    this->moveTime = moveTime;
}

void FrontEnd::setPressTime(int pressTime) {
    std::cout << "front-end: setPressTime called: " << std::endl;
    this->pressTime = pressTime;
}

void FrontEnd::setFlashMode(bool flashMode) {
    std::cout << "front-end: setFlashMode called: " << std::endl;
    this->flashMode = flashMode;
}

void FrontEnd::setHumanMode(bool humanMode) {
    std::cout << "front-end: setHumanMode called: " << std::endl;
    this->humanMode = humanMode;
}

void FrontEnd::setSilentMode(bool silentMode) {
    std::cout << "front-end: setSilentMode called: " << std::endl;
    this->silentMode = silentMode;
}

void FrontEnd::logError(const icString & error) {
    std::cerr << "error: " << error.toString() << std::endl;
}

void FrontEnd::logInfo(const icString & info) {
    std::cout << "info: " << info.toString() << std::endl;
}

void FrontEnd::setAjaxHandler(il::AjaxHandler * /*handler*/) {
    std::cout << "front-end: setAjaxHandler called: " << std::endl;
}

void FrontEnd::resetAjaxHandler() {
    std::cout << "front-end: resetAjaxHandler called: " << std::endl;
}

}  // namespace icL::shared
