#ifndef shared_Engine
#define shared_Engine

#include <icL-types/global/icl-types.h++>

#include <icL-il/main/interlevel.h++>



namespace icL::shared {

/**
 * @brief The Engine class represent a icL engine ready for use
 */
class icL_SHARED Engine
{
    /// \brief m_il represent the interlevel node
    il::InterLevel m_il;

public:
    /**
     * @brief setMemory sets the memory service
     * @param mem is the pointer to memory service
     */
    void setMemory(memory::Memory * mem);

    /**
     * @brief setVMStack sets the vm stack (factory)
     * @param vms is the VM stack
     */
    void setVMStack(il::VMStack * vms);

    /**
     * @brief setFrontEndServer sets the front-end node
     * @param server is the synronization server
     */
    void setFrontEndServer(il::FrontEnd * server);

    /**
     * @brief setDbServer sets the database service
     * @param db is the database service
     */
    void setDbServer(il::DBServer * db);

    /**
     * @brief setFileServer sets the file service
     * @param file is the file service to set
     */
    void setFileServer(il::FileServer * file);

    /**
     * @brief setSourceServer sets the source service
     * @param source is the source service to set
     */
    void setSourceServer(il::SourceServer * source);

    /**
     * @brief setStatefulServer sets the stateful service
     * @param stateful is the stateful service to set
     */
    void setStatefulServer(il::StatefulServer * stateful);

    /**
     * @brief setListenService sets the listener service
     * @param listen is the listener service to set
     */
    void setListenService(il::Listen * listen);

    /**
     * @brief setRequestService sets the request server
     * @param request is the request server to set
     */
    void setRequestService(il::Request * request);

    /**
     * @brief finalize initilize unitialized services
     */
    void finalize();

    /**
     * @brief il gets a pointer to main interlevel node
     * @return a pointer to main interserver node
     */
    il::InterLevel * il();
};

}  // namespace icL::shared

#endif  // shared_Engine
