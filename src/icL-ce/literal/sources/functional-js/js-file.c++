#include "js-file.h++"

#include <icL-types/replaces/ic-file.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-text-stream.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/file-target.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce-base/value/base-value.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-base/system/js-file-value.h++>

#include <icL-memory/state/signals.h++>


namespace icL::ce {

JsFile::JsFile(il::InterLevel * il, const icString & filename)
    : FunctionalJsLiteral(il)
    , filename(filename) {}

icString JsFile::getFileContent() {
    icString     path{il->vms->getResourceDir("") + filename};
    icFile       file{path};
    icTextStream stream{&file};

    if (!file.isReadable()) {
        il->vm->signal({memory::Signals::System, "File not found"});
        return {};
    }

    icString code = stream.readAll();

    if (code.isEmpty()) {
        il->vm->signal({memory::Signals::System, "Empty Javascript file"});
    }

    return code;
}

icString JsFile::toString() {
    return "js:file[" % filename % "]";
}

int JsFile::currentRunRank(bool rtl) {
    return rtl ? -1 : 10;
}

StepType JsFile::runNow() {
    il::JsFile file;
    file.target = std::make_shared<il::ResourceFile>();

    auto * jsfile = new JsFileValue{il, file};
    jsfile->load(filename);

    m_newContext = jsfile;

    return StepType::CommandEnd;
}

Role JsFile::role() {
    return Role::JsFile;
}

}  // namespace icL::ce
