#include "identifier.h++"

namespace icL::ce {

Identifier::Identifier(il::InterLevel * il, const icString & name)
    : StaticLiteral(il)
    , name(name) {}

const icString & Identifier::getName() const {
    return name;
}

icString Identifier::toString() {
    return name;
}

Role Identifier::role() {
    return Role::Identifier;
}

bool Identifier::hasValue() {
    return false;
}

const icSet<Role> & Identifier::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole, Role::Comma};
    return roles;
}

const icSet<Role> & Identifier::acceptedNexts() {
    static const icSet<Role> roles{Role::Assign, Role::Cast};
    return roles;
}

}  // namespace icL::ce
