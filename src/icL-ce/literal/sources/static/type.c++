#include "type.h++"

#include <icL-memory/structures/packed-value-item.h++>



namespace icL::ce {

TypeToken::TypeToken(il::InterLevel * il, memory::Type type)
    : StaticLiteral(il)
    , type(type) {}

memory::Type TypeToken::getType() {
    return type;
}

icString TypeToken::toString() {
    return memory::typeToString(type);
}

Role TypeToken::role() {
    return Role::Type;
}

memory::PackedValueItem TypeToken::packNow() {
    memory::PackedValueItem ret;

    ret.itemType = memory::PackedValueType::Type;
    ret.type     = type;

    return ret;
}

}  // namespace icL::ce
