#include "property.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>

#include <utility>

namespace icL::ce {

Property::Property(il::InterLevel * il, Prefix prefix, const icString & name)
    : FunctionalLiteral(il)
    , prefix(prefix)
    , name(name) {}

icString Property::toString() {
    return '\'' % name;
}

int Property::currentRunRank(bool rtl) {
    bool runnable =
      m_prev->role() == Role::Value || m_prev->role() == Role::SystemValue;
    return !rtl && runnable ? 7 : -1;
}

StepType Property::runNow() {
    auto * value = dynamic_cast<Value *>(m_prev);
    value->runProperty(prefix, name);
    m_newContext = value->newCE();

    return StepType::CommandEnd;
}

CE * Property::firstToReplace() {
    return m_prev;
}

Role Property::role() {
    return Role::Property;
}

const icSet<Role> & Property::acceptedPrevs() {
    static const icSet<Role> roles{Role::Value, Role::SystemValue,
                                   Role::Property, Role::LimitedContext,
                                   Role::ValueContext};
    return roles;
}

const icSet<Role> & Property::acceptedNexts() {
    static const icSet<Role> roles{Role::Property, Role::Method, Role::Assign,
                                   Role::Comma,    Role::Cast,   Role::Operator,
                                   Role::NoRole};
    return roles;
}

}  // namespace icL::ce
