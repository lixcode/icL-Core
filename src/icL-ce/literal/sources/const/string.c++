#include "string.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::ce {

String::String(il::InterLevel * il, const icString & pattern)
    : ConstLiteral(il, pattern) {}

icString String::toString() {
    return '"' % pattern % '"';
}

icVariant String::getValueOf() {
    icString ret = pattern;

    ret.replace(R"(\t)", "\t");
    ret.replace(R"(\n)", "\n");
    ret.replace(R"(\b)", "\b");
    ret.replace(R"(\")", R"(")");
    ret.replace(R"(\\)", R"(\)");

    return ret;
}

}  // namespace icL::ce
