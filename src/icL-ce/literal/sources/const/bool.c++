#include "bool.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>

namespace icL::ce {

Bool::Bool(il::InterLevel * il, const icString & pattern)
    : ConstLiteral(il, pattern) {}

icVariant Bool::getValueOf() {
    bool ret;

    if (pattern == "true") {
        ret = true;
    }
    else if (pattern == "false") {
        ret = false;
    }
    else {
        ret = false;  // elude warning
        il->vm->signal({memory::Signals::System, "Wrong bool literal"});
    }

    return ret;
}

}  // namespace icL::ce
