#ifndef ce_JsRun
#define ce_JsRun

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce-base/literal/functional-js-literal.h++>



namespace icL::ce {

class JsRun : public FunctionalJsLiteral
{
public:
    JsRun(
      il::InterLevel * il, const icString & code, icString sessionId,
      icString windowsId);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    Role     role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // fields
private:
    /// \brief code is the javascript code to run
    icString code;
    /// \brief async is true if code need to be executed asynchronously
    bool async = false;

    // padding
    long : 56;
};

}  // namespace icL::ce

#endif  // ce_JsRun
