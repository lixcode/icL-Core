#ifndef ce_Property
#define ce_Property

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce-base/literal/functional-literal.h++>
#include <icL-ce-base/main/value.h++>



namespace icL::ce {

class Property : public FunctionalLiteral
{
public:
    Property(il::InterLevel * il, Prefix prefix, const icString & name);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    CE * firstToReplace() override;
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // fields
private:
    /// \brief prefix is the prefix of the property
    Prefix prefix;

    // padding
    int : 32;

    /// \brief name is the name of the property
    icString name;
};

}  // namespace icL::ce

#endif  // ce_Property
