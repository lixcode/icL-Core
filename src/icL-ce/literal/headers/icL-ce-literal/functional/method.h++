#ifndef ce_Method
#define ce_Method

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce-base/literal/functional-literal.h++>



namespace icL::ce {

class Method : public FunctionalLiteral
{
public:
    Method(il::InterLevel * il, const icString &name);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    CE * firstToReplace() override;
    CE * lastToReplace() override;
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // fields
private:
    /// \brief name is the method name
    icString name;
};

}  // namespace icL::ce

#endif  // ce_Method
