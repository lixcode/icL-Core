#ifndef ce_Parameter
#define ce_Parameter

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce-base/literal/static-literal.h++>

#include <icL-memory/structures/type.h++>



namespace icL::ce {

/**
 * @brief The Parameter class represents a parameter token `@name = value`
 */
class Parameter : public StaticLiteral
{
public:
    Parameter(il::InterLevel * il, const icString &name, memory::Type type);

    /**
     * @brief getName gets the name of the parameter
     * @return the name of the parameter
     */
    const icString & getName();

    /**
     * @brief getType gets the type of the parameter
     * @return the type of the parameter
     */
    memory::Type getType();

    /**
     * @brief isDefault checks if is a default parameter
     * @return true if is a default parameter, otherwise false
     */
    virtual bool isDefault() const;

    // CE interface
public:
    icString toString() override;
    Role     role() override;

    memory::PackedValueItem packNow() override;

private:
    /// \brief name is the name of parameter
    icString name;
    /// \brief type is the type of parameter
    memory::Type type;

    // padding
    int : 32;
};

}  // namespace icL::ce

#endif  // ce_Parameter
