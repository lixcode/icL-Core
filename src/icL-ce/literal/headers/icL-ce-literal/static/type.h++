#ifndef ce_Type
#define ce_Type

#include <icL-ce-base/literal/static-literal.h++>

#include <icL-memory/structures/type.h++>



namespace icL::ce {

/**
 * @brief The Type class represent a type token - `type`
 */
class TypeToken : public StaticLiteral
{
public:
    TypeToken(il::InterLevel * il, memory::Type type);

    /**
     * @brief getType gets the contined type
     * @return the contained type
     */
    memory::Type getType();

    // CE interface
public:
    icString toString() override;
    Role     role() override;

    memory::PackedValueItem packNow() override;

private:
    /// \brief type is the contianed type
    memory::Type type;

    // align bytes
    int : 32;
};

}  // namespace icL::ce

#endif  // ce_Type
