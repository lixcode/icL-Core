#ifndef ce_DefaultParameter
#define ce_DefaultParameter

#include "parameter.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::ce {

class DefaultParameter : public Parameter
{
public:
    DefaultParameter(
      il::InterLevel * il, const icString & name, const icVariant & value);

    /**
     * @brief getDefaultValue gets the default value of parameter
     * @return the default value of parameter
     */
    const icVariant & getDefaultValue();

    // CE interface
public:
    memory::PackedValueItem packNow() override;

    icString toString() override;

    // Parameter interface
public:
    bool isDefault() const override;

private:
    /// \brief defaultValue is the default value of parameter
    icVariant defaultValue;
};

}  // namespace icL::ce

#endif  // ce_DefaultParameter
