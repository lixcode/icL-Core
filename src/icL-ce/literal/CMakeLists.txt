cmake_minimum_required(VERSION 3.8.0)

project(-icL-ce-literal LANGUAGES CXX)

file(GLOB HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/headers/icL-ce-literal/*/*.h++")
file(GLOB SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/sources/*/*.c++")

add_library(${PROJECT_NAME} STATIC
  ${HEADERS}
  ${SOURCES}
)

target_include_directories(${PROJECT_NAME}
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/headers/icL-ce-literal/const
	${CMAKE_CURRENT_SOURCE_DIR}/headers/icL-ce-literal/functional
	${CMAKE_CURRENT_SOURCE_DIR}/headers/icL-ce-literal/functional-js
	${CMAKE_CURRENT_SOURCE_DIR}/headers/icL-ce-literal/static
  PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/headers
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
	-icL-memory
	-icL-ce-base
	-icL-ce-value-base
	)
