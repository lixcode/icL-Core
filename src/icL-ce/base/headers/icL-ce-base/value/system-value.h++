#ifndef ce_SystemValue
#define ce_SystemValue

#include "../main/value.h++"



namespace icL::ce {

class SystemValue : public Value
{
public:
    SystemValue(il::InterLevel * il);
    // CE interface
public:
    icString toString() override;
};

}  // namespace icL::ce

#endif  // ce_SystemValue
