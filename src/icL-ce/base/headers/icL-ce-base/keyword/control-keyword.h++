#ifndef ce_ControlKeyword
#define ce_ControlKeyword

#include "../main/keyword.h++"



namespace icL::ce {

class ControlKeyword : public Keyword
{
public:
    ControlKeyword(il::InterLevel * il);

    // CE interface
protected:
    CE * lastToReplace() override;
};

}  // namespace icL::ce

#endif  // ce_ControlKeyword
