#ifndef ce_IncludingOperator
#define ce_IncludingOperator

#include "advanced-operator.h++"



namespace icL::ce {

class IncludingOperator : public AdvancedOperator
{
public:
    IncludingOperator(il::InterLevel * il);

    // CE interface
public:
    int currentRunRank(bool rtl) override;
};

}  // namespace icL::ce

#endif  // ce_IncludingOperator
