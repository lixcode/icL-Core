#ifndef ce_Value_runMethodNow
#define ce_Value_runMethodNow

#include "value.h++"

namespace icL::ce {

template <typename This, typename ParentClass>
void Value::runMethodNow(
  const icObject<icString, void (This::*)(const memory::ArgList &)> & methods,
  const icString & name, const memory::ArgList & args) {
    auto this_ = dynamic_cast<This *>(this);
    auto it    = methods.find(name);

    if (it != methods.end()) {
        (this_->*it.value())(args);
    }

    if (m_newContext == nullptr) {
        this_->ParentClass::runMethod(name, args);
    }
}

}  // namespace icL::ce

#endif  // ce_Value_runMethodNow
