#ifndef ce_FunctionalLiteral
#define ce_FunctionalLiteral

#include "../main/literal.h++"



namespace icL::ce {

class FunctionalLiteral : public Literal
{
public:
    FunctionalLiteral(il::InterLevel * il);
};

}  // namespace icL::ce

#endif  // ce_FunctionalLiteral
