#ifndef ce_ConstLiteral
#define ce_ConstLiteral

#include "../main/literal.h++"

#include <icL-types/replaces/ic-string.h++>



class icVariant;

namespace icL::ce {

class ConstLiteral : public Literal
{
public:
    ConstLiteral(il::InterLevel * il, const icString &pattern);

    /**
     * @brief getValueOf gets the value of this literal
     * @return the value of this constant literal
     */
    virtual icVariant getValueOf() = 0;

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    Role     role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // fields
protected:
    icString pattern;
};

}  // namespace icL::ce

#endif  // ce_ConstLiteral
