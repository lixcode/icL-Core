#include "loop-keyword.h++"

namespace icL::ce {

LoopKeyword::LoopKeyword(il::InterLevel * il)
    : Keyword(il) {}

CE * LoopKeyword::lastToReplace() {
    il::CE * it = this;

    while (it->next() != nullptr) {
        it = it->next();
    }

    return dynamic_cast<CE *>(it);
}

}  // namespace icL::ce
