#include "browser-command.h++"

#include <icL-types/replaces/ic-set.h++>



namespace icL::ce {

BrowserCommand::BrowserCommand(il::InterLevel * il)
    : BrowserValue(il) {}

const icSet<Role> & BrowserCommand::acceptedNexts() {
    static icSet<Role> roles{BrowserValue::acceptedNexts(), {Role::NoRole}};
    return roles;
}

}  // namespace icL::ce
