#include "base-value.h++"

#include <icL-types/replaces/ic-debug.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-service-main/factory/factory.h++>
#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/state/datacontainer.h++>
#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>

#include <memory>
#include <utility>

namespace icL::ce {

BaseValue::BaseValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : Value(il)
    , container(container)
    , varName(varName)
    , valuetype(ValueType::L)
    , readonly(readonly) {}

BaseValue::BaseValue(il::InterLevel * il, icVariant rvalue)
    : Value(il)
    , rvalue(std::move(rvalue))
    , valuetype(ValueType::R)
    , readonly(true) {}

BaseValue::BaseValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : Value(il)
    , setter(setter)
    , getter(getter)
    , valuetype(ValueType::Js) {
    readonly = setter.isEmpty();
}

BaseValue::BaseValue(BaseValue * value)
    : Value(value->il) {
    valuetype = value->valuetype;
    readonly  = value->readonly;

    if (valuetype == ValueType::L) {
        container = value->container;
        varName   = value->varName;
    }
    else if (valuetype == ValueType::R) {
        rvalue = value->rvalue;
        fsetter = value->fsetter;
        fgetter = value->fgetter;
    }
    else /* valuetype == ValueType::Js */ {
        setter = value->setter;
        getter = value->getter;
    }
}

void BaseValue::makeFunctional(
  const std::function<icVariant(BaseValue *)> &       fgetter,
  const std::function<void(BaseValue *, icVariant)> & fsetter) {
    this->fgetter = fgetter;
    this->fsetter = fsetter;
    valuetype     = ValueType::R;
    readonly      = false;
}

bool BaseValue::isLValue() const {
    return valuetype == ValueType::L;
}

bool BaseValue::isRValue() const {
    return valuetype == ValueType::R;
}

void BaseValue::rebind() {
    auto * it = il->mem->stackIt().stack();

    while (!it->isContainer()) {
        it = it->getPrev();
    }

    container = it;
}

CE * BaseValue::ensureRValue() {
    return service::Factory::fromValue(il, getValue());
}

void BaseValue::runEnsureRValue(const memory::ArgList & args) {
    if (args.isEmpty()) {
        m_newContext = ensureRValue();
    }
    else {
        Value::runMethod("ensureRValue", args);
    }
}

bool BaseValue::hasValue() {
    return true;
}

icString BaseValue::toString() {
    memory::Argument arg;
    arg.type      = type();
    arg.value     = getValue();
    arg.varName   = varName;
    arg.container = container;
    return service::Stringify::argConstructor(arg, container);
}

icVariant BaseValue::getValue(bool excludeFunctional) {

    if (excludeFunctional || fgetter == nullptr) {
        if (valuetype == ValueType::R) {
            return rvalue;
        }
        if (valuetype == ValueType::L) {
            return container->getValue(varName);
        }

        /* value == Value::Js */
        rvalue = il->server->executeSync(getter, icVariantList());
        return rvalue;
    }

    return fgetter(this);
}

void BaseValue::setValue(const icVariant & value, bool excludeFunctional) {
    if (readonly) {
        // The Assign Block must check the readonly property
        icWarning() << "Value changed for readonly icObject!!!";
    }

    if (excludeFunctional || fsetter == nullptr) {
        if (valuetype == ValueType::R) {
            rvalue = value;
        }
        else if (valuetype == ValueType::L) {
            container->setValue(varName, value);
        }
        else { /* this.value == Value.Js */
            il->server->executeSync(
              setter.replace("@{value}", "arguments[0]"),
              icVariantList({{value}}));
        }
    }
    else {
        fsetter(this, value);
    }
}

Role BaseValue::role() {
    return Role::Value;
}

void BaseValue::runMethod(const icString & name, const memory::ArgList & args) {
    if (name == "ensureRValue") {
        runEnsureRValue(args);
    }
    else {
        Value::runMethod(name, args);
    }
}

memory::PackedValueItem BaseValue::packNow() {
    memory::PackedValueItem ret;

    ret.itemType  = memory::PackedValueType::Value;
    ret.value     = getValue();
    ret.name      = varName;
    ret.container = container;
    ret.type      = type();

    return ret;
}

const icSet<Role> & BaseValue::acceptedPrevs() {
    static icSet<Role> roles{
      Role::NoRole,   Role::Any,    Role::If,       Role::For,    Role::Range,
      Role::Filter,   Role::Method, Role::Function, Role::Assign, Role::Comma,
      Role::Operator, Role::JsRun,  Role::Now};
    return roles;
}

const icSet<Role> & BaseValue::acceptedNexts() {
    static icSet<Role> roles{Role::NoRole,     Role::Assign,  Role::Comma,
                             Role::RunContext, Role::Cast,    Role::Operator,
                             Role::Method,     Role::Property};
    return roles;
}

}  // namespace icL::ce
