#include "functional-js-literal.h++"

#include <icL-types/replaces/ic-set.h++>



namespace icL::ce {

FunctionalJsLiteral::FunctionalJsLiteral(il::InterLevel * il)
    : Literal(il) {}

const icSet<Role> & FunctionalJsLiteral::acceptedPrevs() {
    static icSet<Role> roles = {Role::NoRole, Role::Method, Role::Function,
                                Role::Assign, Role::Comma,  Role::Operator,
                                Role::JsRun};
    return roles;
}

const icSet<Role> & FunctionalJsLiteral::acceptedNexts() {
    static icSet<Role> roles = {Role::Method, Role::Property};
    return roles;
}

}  // namespace icL::ce
