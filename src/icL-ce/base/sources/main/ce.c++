#include "ce.h++"

#include "base-value.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-cp/result/structs.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>

#include <cassert>



namespace icL::ce {

CE::CE(il::InterLevel * il)
    : Node(il) {}

CE * CE::newCE() {
    return m_newContext;
}

CE * CE::firstToReplace() {
    return this;
}

CE * CE::lastToReplace() {
    return this;
}

memory::PackedValueItem CE::packNow() {
    // no value in this node
    assert(false);
    return {};
}

BaseValue * CE::asValue() {
    return dynamic_cast<BaseValue *>(this);
}

CE * CE::getFirst() {
    auto * it = this;

    while (it->m_prev != nullptr) {
        it = it->m_prev;
    }

    return it;
}

CE * CE::getLast() {
    auto * it = this;

    while (it->m_next != nullptr) {
        it = it->m_next;
    }

    return it;
}

il::InterLevel * CE::_il() {
    return il;
}

bool CE::checkPrev(il::CE * ilCE) {
    const auto & accepted = acceptedPrevs();

    CE * ce   = dynamic_cast<CE *>(ilCE);
    Role role = ce != nullptr ? ce->role() : Role::NoRole;

    return accepted.contains(role);
}

bool CE::checkNext(il::CE * ilCE) {
    const auto & accepted = acceptedNexts();

    CE * ce   = dynamic_cast<CE *>(ilCE);
    Role role = ce != nullptr ? ce->role() : Role::NoRole;

    return accepted.contains(role);
}

void CE::release(il::CE *& lastCE) {
    CE * first = firstToReplace();
    CE * last  = lastToReplace();

    m_newContext->m_fragmentData     = first->m_fragmentData;
    m_newContext->m_fragmentData.end = last->m_fragmentData.end;

    if (last == lastCE) {
        lastCE = m_newContext;
    }

    if (first == last && last == this) {
        service::Replace::replace(this, m_newContext);
    }
    else {
        service::Replace::replace(first, last, m_newContext);
    }
}

void CE::linkAfter(il::CE * ce) {
    service::Replace::linkAfter(dynamic_cast<CE *>(ce), this);
}

il::CE * CE::prev() {
    return m_prev;
}

il::CE * CE::next() {
    return m_next;
}

const il::CodeFragment & CE::fragmentData() {
    return m_fragmentData;
}

il::CE * CE::link(
  const cp::FlyResult & token, const il::CodeFragment & origin) {
    m_fragmentData.source = origin.source;
    m_fragmentData.begin  = token.begin;
    m_fragmentData.end    = token.end;
    return this;
}

bool CE::hasValue() {
    return false;
}

icVariant CE::getValue(bool /*excludeFunctional*/) {
    return {};
}

}  // namespace icL::ce
