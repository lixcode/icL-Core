#include "keyword.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

namespace icL::ce {

Keyword::Keyword(il::InterLevel * il)
    : CE(il) {}

void Keyword::giveModifiers(const icStringList & modifiers) {
    if (!modifiers.isEmpty()) {
        il->vm->signal(
          {memory::Signals::System, "No such modifier: " + modifiers[0]});
    }
}

}  // namespace icL::ce
