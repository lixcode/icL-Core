#include "operator.h++"

#include "operator-run-now.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-service-main/args/listify.h++>

#include <icL-ce-base/value/base-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/function-call.h++>
#include <icL-vm/vmlayer.h++>

namespace icL::ce {

Operator::Operator(il::InterLevel * il)
    : CE(il) {}

const icSet<Role> & Operator::byContext(
  const icSet<Role> & run, const icSet<Role> & limited,
  const icSet<Role> & value) {
    using memory::ContextType;

    auto * vm = dynamic_cast<vm::VMLayer *>(il->vm);

    switch (vm->getType()) {
    case ContextType::Run:
        return run;

    case ContextType::Limited:
        return limited;

    case ContextType::Value:
        return value;
    }
}

Role Operator::role() {
    return Role::Operator;
}

StepType Operator::runNow() {
    auto * leftPtr  = dynamic_cast<BaseValue *>(m_prev);
    auto * rightPtr = dynamic_cast<BaseValue *>(m_next);

    memory::ArgList left;
    memory::ArgList right;

    if (leftPtr != nullptr) {
        left = service::Listify::toArgList(il, leftPtr);
    }

    if (rightPtr != nullptr) {
        right = service::Listify::toArgList(il, rightPtr);
    }

    run(left, right);
    return StepType::CommandEnd;
}

const icSet<Role> & Operator::acceptedPrevs() {
    static const icSet<Role> roles{Role::Value,          Role::Property,
                                   Role::LimitedContext, Role::ValueContext,
                                   Role::Function,       Role::JsValue};
    return roles;
}

const icSet<Role> & Operator::acceptedNexts() {
    static const icSet<Role> roles{Role::Value,        Role::SystemValue,
                                   Role::Function,     Role::LimitedContext,
                                   Role::ValueContext, Role::JsValue,
                                   Role::Operator};
    return roles;
}

CE * Operator::firstToReplace() {
    return m_prev;
}

CE * Operator::lastToReplace() {
    return m_next;
}

}  // namespace icL::ce
