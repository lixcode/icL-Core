#include "compare-operator.h++"

namespace icL::ce {

CompareOperator::CompareOperator(il::InterLevel * il)
    : AdvancedOperator(il) {}

int CompareOperator::currentRunRank(bool rtl) {
    bool runnalble =
      m_prev->role() == Role::Value && m_next->role() == Role::Value;
    return runnalble && !rtl ? 3 : -1;
}

}  // namespace icL::ce
