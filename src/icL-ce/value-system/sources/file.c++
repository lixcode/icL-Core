#include "file.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/int-value.h++>



namespace icL::ce {

File::File(il::InterLevel * il)
    : SystemValue(il) {}

void File::runCsv() {
    m_newContext = new IntValue{il, csv()};
}

void File::runNone() {
    m_newContext = new IntValue{il, none()};
}

void File::runTsv() {
    m_newContext = new IntValue{il, tsv()};
}

Type File::type() const {
    return Type::File;
}

icString File::typeName() {
    return "File";
}

void File::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (File::*)()> properties{
      {"csv", &File::runCsv}, {"none", &File::runNone}, {"tsv", &File::runTsv}};

    runPropertyWithPrefixCheck<File, SystemValue>(properties, prefix, name);
}

}  // namespace icL::ce
