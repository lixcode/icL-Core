#include "files.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/structures/file-target.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-base/system/file-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Files::Files(il::InterLevel * il)
    : SystemValue(il) {}

void Files::runCreate(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new FileValue{il, create(args[0])};
    }
}

void Files::runCreateDir(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        createDir(args[0]);
        m_newContext = new VoidValue{il};
    }
}

void Files::runCreatePath(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        createPath(args[0]);
        m_newContext = new VoidValue{il};
    }
}

void Files::runOpen(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new FileValue{il, open(args[0])};
    }
}

Type Files::type() const {
    return Type::Files;
}

icString Files::typeName() {
    return "Files";
}

void Files::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Files::*)(const memory::ArgList &)> methods{
      {"create", &Files::runCreate},
      {"createDir", &Files::runCreateDir},
      {"createPath", &Files::runCreatePath}};

    runMethodNow<Files, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
