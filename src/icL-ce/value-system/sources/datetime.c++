#include "datetime.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-value-base/complex/datetime-value.h++>



namespace icL::ce {

icDateTime::icDateTime(il::InterLevel * il)
    : SystemValue(il) {}

void icDateTime::runCurrent(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new DatetimeValue{il, current()};
    }
}

void icDateTime::runCurrentUTC(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new DatetimeValue{il, currentUTC()};
    }
}

Type icDateTime::type() const {
    return Type::Datetime;
}

icString icDateTime::typeName() {
    return "DateTime";
}

void icDateTime::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (icDateTime::*)(const memory::ArgList &)>
      methods{{"current", &icDateTime::runCurrent},
              {"currentUTC", &icDateTime::runCurrentUTC}};

    runMethodNow<icDateTime, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
