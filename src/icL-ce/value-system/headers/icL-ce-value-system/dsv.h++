#ifndef ce_DSV
#define ce_DSV

#include <icL-service-value-system/dsv.h++>

#include <icL-ce-base/value/system-value.h++>

namespace icL::ce {

class DSV
    : public SystemValue
    , public service::DSV
{
public:
    DSV(il::InterLevel * il);

    // methods level 2

    /// `DSV.append`
    void runAppend(const memory::ArgList & args);

    /// `DSV.load`
    void runLoad(const memory::ArgList & args);

    /// `DSV.loadCSV`
    void runLoadCSV(const memory::ArgList & args);

    /// `DSV.loadTSV`
    void runLoadTSV(const memory::ArgList & args);

    /// `DSV.sync`
    void runSync(const memory::ArgList & args);

    /// `DSV.write`
    void runWrite(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_DSV
