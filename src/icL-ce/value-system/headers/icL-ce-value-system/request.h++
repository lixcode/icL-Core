#ifndef ce_Request
#define ce_Request

#include <icL-service-value-system/request.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class Request
    : public SystemValue
    , service::Request
{
public:
    Request(il::InterLevel * il);

    // methods level 2
    /// `Request.confirm`
    void runConfirm(const memory::ArgList & args);

    /// `Request.ask`
    void runAsk(const memory::ArgList & args);

    /// `Request.int`
    void runInt(const memory::ArgList & args);

    /// `Request.double`
    void runDouble(const memory::ArgList & args);

    /// `Request.string`
    void runString(const memory::ArgList & args);

    /// `Request.list`
    void runList(const memory::ArgList & args);

    /// `Request.tab`
    void runTab(const memory::ArgList & args);

    // Value interface
public:
    memory::Type type() const;
    icString     typeName();
    void         runMethod(const icString & name, const memory::ArgList & args);
};

}  // namespace icL::ce

#endif  // ce_Request
