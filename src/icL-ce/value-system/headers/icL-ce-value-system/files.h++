#ifndef ce_Files
#define ce_Files

#include <icL-service-value-system/files.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class Files
    : public SystemValue
    , public service::Files
{
public:
    Files(il::InterLevel * il);

    // methods level 2

    /// `Files.create`
    void runCreate(const memory::ArgList & args);

    /// `Files.createDir`
    void runCreateDir(const memory::ArgList & args);

    /// `Files.createPath`
    void runCreatePath(const memory::ArgList & args);

    /// `Files.open`
    void runOpen(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_Files
