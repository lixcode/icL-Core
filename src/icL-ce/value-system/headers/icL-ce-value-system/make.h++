#ifndef ce_Make
#define ce_Make

#include <icL-service-value-system/make.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class Make
    : public SystemValue
    , public service::Make
{
public:
    Make(il::InterLevel * il);

    // methods level 2

    /// `Make.image`
    void runImage(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_Make
