#include "windows.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-index-check.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/browser/session-value.h++>
#include <icL-ce-value-base/browser/window-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Windows::Windows(il::InterLevel * il, il::TargetData target)
    : BrowserValue(il)
    , service::Windows(std::move(target)) {
    if (this->target.sessionId.isEmpty()) {
        this->target = il->server->getCurrentTarget();
    }
}

void Windows::runCurrent() {
    m_newContext = new WindowValue{il, current()};
}

void Windows::runLength() {
    m_newContext = new IntValue{il, length()};
}

void Windows::runSession() {
    m_newContext = new SessionValue{il, session()};
}

void Windows::runGet(const memory::ArgList & args) {
    m_newContext = new WindowValue{il, get(args[0])};
}

memory::Type Windows::type() const {
    return Type::Windows;
}

icString Windows::typeName() {
    return "windows";
}

void Windows::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Windows::*)()> properties{
      {"current", &Windows::runCurrent},
      {"length", &Windows::runLength},
      {"session", &Windows::runSession}};

    il->server->pushTarget(target);
    runPropertyWithIndexCheck<Windows, BrowserValue, WindowValue>(
      properties, prefix, name, [this](int i) -> icVariant { return get(i); });
    il->server->popTarget();
}

void Windows::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Windows::*)(const memory::ArgList &)>
      methods{{"get", &Windows::runGet}};

    il->server->pushTarget(target);
    runMethodNow<Windows, BrowserValue>(methods, name, args);
    il->server->popTarget();
}

}  // namespace icL::ce
