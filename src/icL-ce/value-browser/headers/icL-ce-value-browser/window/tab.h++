#ifndef ce_Tab
#define ce_Tab

#include <icL-ce-base/value/browser-command.h++>



namespace icL::ce {

class Tab : public BrowserCommand
{
public:
    Tab(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_Tab
