#ifndef ce_Session
#define ce_Session

#include <icL-ce-base/value/browser-command.h++>



namespace icL::ce {

class Session : public BrowserCommand
{
public:
    Session(il::InterLevel * il);


    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_Session
