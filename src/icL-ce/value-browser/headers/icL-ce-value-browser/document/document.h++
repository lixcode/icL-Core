#ifndef ce_Document
#define ce_Document

#include <icL-ce-base/value/browser-command.h++>



namespace icL::ce {

class Document : public BrowserCommand
{
public:
    Document(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    memory::Type type() const override;
    icString     typeName() override;
};

}  // namespace icL::ce

#endif  // ce_Document
