#include "type-of.h++"

#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

TypeOf::TypeOf(il::InterLevel * il)
    : CastOperator(il) {}

icString TypeOf::toString() {
    return "::";
}

void TypeOf::runCast(const memory::Argument & left, memory::Type right) {
    bool isTypeOf = left.type == right;

    if (m_newContext == nullptr) {
        m_newContext = new BoolValue{il, isTypeOf};
    }
    else {
        auto * asValue = dynamic_cast<BaseValue *>(m_newContext);
        asValue->setValue(isTypeOf && asValue->getValue().toBool());
    }
}

void TypeOf::runCast(
  const memory::ArgList & /*left*/, const memory::ArgList & /*right*/) {
    // `type of` doesn't accepts values on right
}

}  // namespace icL::ce
