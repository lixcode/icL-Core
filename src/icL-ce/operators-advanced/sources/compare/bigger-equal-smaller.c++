#include "bigger-equal-smaller.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

BiggerEqualSmaller::BiggerEqualSmaller(il::InterLevel * il)
    : CompareOperator(il) {}

void BiggerEqualSmaller::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, intIntInt(left[0], right[0], right[1])};
}

void BiggerEqualSmaller::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext =
      new BoolValue{il, doubleDoubleDouble(left[0], right[0], right[1])};
}

icString BiggerEqualSmaller::toString() {
    return ">=<";
}

void BiggerEqualSmaller::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (BiggerEqualSmaller::*)(
        const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue, Type::IntValue}},
                 &BiggerEqualSmaller::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue, Type::DoubleValue}},
                 &BiggerEqualSmaller::runDoubleDouble}};

    runNow<BiggerEqualSmaller>(operators, left, right);
}

}  // namespace icL::ce
