#include "smaller.h++"

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Smaller::Smaller(il::InterLevel * il)
    : CompareOperator(il) {}

void Smaller::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, intInt(left[0], right[0])};
}

void Smaller::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, doubleDouble(left[0], right[0])};
}

icString Smaller::toString() {
    return "<";
}

void Smaller::run(const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (Smaller::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue}}, &Smaller::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue}},
                 &Smaller::runDoubleDouble}};

    runNow<Smaller>(operators, left, right);
}

}  // namespace icL::ce
