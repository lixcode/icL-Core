#include "bigger-equal.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

BiggerEqual::BiggerEqual(il::InterLevel * il)
    : CompareOperator(il) {}

void BiggerEqual::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, intInt(left[0], right[0])};
}

void BiggerEqual::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, doubleDouble(left[0], right[0])};
}

icString BiggerEqual::toString() {
    return ">=";
}

void BiggerEqual::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (BiggerEqual::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue}}, &BiggerEqual::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue}},
                 &BiggerEqual::runDoubleDouble}};

    runNow<BiggerEqual>(operators, left, right);
}

}  // namespace icL::ce
