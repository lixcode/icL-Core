#include "smaller-equal.h++"

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

SmallerEqual::SmallerEqual(il::InterLevel * il)
    : CompareOperator(il) {}

void SmallerEqual::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, intInt(left[0], right[0])};
}

void SmallerEqual::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, doubleDouble(left[0], right[0])};
}

icString SmallerEqual::toString() {
    return "<=";
}

void SmallerEqual::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (SmallerEqual::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{Type::IntValue}, {Type::IntValue}}, &SmallerEqual::runIntInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}},
         &SmallerEqual::runDoubleDouble}};

    runNow<SmallerEqual>(operators, left, right);
}

}  // namespace icL::ce
