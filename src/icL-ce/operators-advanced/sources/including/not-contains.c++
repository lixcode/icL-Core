#include "not-contains.h++"

#include <icL-ce-value-base/base/bool-value.h++>

namespace icL::ce {

NotContains::NotContains(il::InterLevel * il)
    : Contains(il) {}

icString NotContains::toString() {
    return "!<";
}

void NotContains::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    Contains::run(left, right);

    if (m_newContext == nullptr) {
        return;
    }

    auto * asBool = dynamic_cast<BoolValue *>(m_newContext);

    asBool->setValue(!asBool->getValue().toBool());
}

}  // namespace icL::ce
