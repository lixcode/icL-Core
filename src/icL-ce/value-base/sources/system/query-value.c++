#include "query-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/db-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-own-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/base/string-value.h++>
#include <icL-ce-value-base/base/void-value.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>



namespace icL::ce {

QueryValue::QueryValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

QueryValue::QueryValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

QueryValue::QueryValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

QueryValue::QueryValue(BaseValue * value)
    : BaseValue(value) {}

CE * QueryValue::roProperty(const icString & name) {
    return service::Factory::fromValue(il, il->db->get(name));
}

CE * QueryValue::woProperty(const icString & name) {
    auto * ret = new VoidValue{this};

    // Copy values to lambdas
    const auto & _name = name;
    auto         _il   = il;

    ret->makeFunctional(
      [_il](BaseValue *) -> icVariant {
          _il->vm->signal({memory::Signals::System, "Try to read w/o value"});
          return {};
      },
      [_name, _il](BaseValue * _this, const icVariant & value) {
          _il->db->pushTarget(*il::Query(_this->getValue()).target);
          _il->db->set(_name, value);
          _il->db->popTarget();
      });

    return ret;
}

CE * QueryValue::property(const icString & name) {
    CE * ret;

    il->db->pushTarget(*_value().target);

    if (il->db->isExecuted()) {
        ret = roProperty(name);
    }
    else {
        ret = woProperty(name);
    }

    il->db->popTarget();

    return ret;
}

void QueryValue::runPropertyLevel2(const icString & name) {
    m_newContext = property(name);
}

void QueryValue::runExec(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new BoolValue{il, exec()};
    }
}

void QueryValue::runFirst(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new BoolValue{il, first()};
    }
}

void QueryValue::runGet(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = service::Factory::fromValue(il, get(args[0]));
    }
}

void QueryValue::runGetError(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new StringValue{il, getError()};
    }
}

void QueryValue::runGetLength(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new IntValue{il, getLength()};
    }
}

void QueryValue::runGetRowsAffected(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new IntValue{il, getRowsAffected()};
    }
}

void QueryValue::runLast(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new BoolValue{il, last()};
    }
}

void QueryValue::runNext(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new BoolValue{il, service::QueryValue::next()};
    }
}

void QueryValue::runPrevious(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new BoolValue{il, last()};
    }
}

void QueryValue::runSeek(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = new BoolValue{il, seek(args[0])};
    }
    else if (checkArgs(args, {Type::IntValue, Type::BoolValue})) {
        m_newContext = new BoolValue{il, seek(args[0], args[1])};
    }
}

void QueryValue::runSet(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::AnyValue})) {
        icSet(args[0], args[1].value);
        m_newContext = new VoidValue{il};
    }
}

Type QueryValue::type() const {
    return Type::QueryValue;
}

icString QueryValue::typeName() {
    return "query";
}

void QueryValue::runProperty(Prefix prefix, const icString & name) {
    runOwnPropertyWithPrefixCheck<QueryValue, BaseValue>(
      &QueryValue::runPropertyLevel2, prefix, name);
}

void QueryValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (QueryValue::*)(const memory::ArgList &)>
      methods{{"exec", &QueryValue::runExec},
              {"first", &QueryValue::runFirst},
              {"get", &QueryValue::runGet},
              {"getError", &QueryValue::runGetError},
              {"getLength", &QueryValue::runGetLength},
              {"getRowsAffected", &QueryValue::runGetRowsAffected},
              {"last", &QueryValue::runLast},
              {"next", &QueryValue::runNext},
              {"previous", &QueryValue::runPrevious},
              {"seek", &QueryValue::runSeek},
              {"icSet", &QueryValue::runSet}};

    runMethodNow<QueryValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
