#include "handler-value.h++"

#include "void-value.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/listen.h++>
#include <icL-il/structures/lambda-target.h++>
#include <icL-il/structures/listen-handler.h++>

#include <icL-ce-base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

HandlerValue::HandlerValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

HandlerValue::HandlerValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

HandlerValue::HandlerValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

HandlerValue::HandlerValue(BaseValue * value)
    : BaseValue(value) {}

void HandlerValue::runSetup(const memory::ArgList & args) {
    if (checkArgs(args, {Type::LambdaValue})) {
        m_newContext = new HandlerValue{il, setup(args[0])};
    }
}

void HandlerValue::runActivate(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new HandlerValue{il, activate()};
    }
}

void HandlerValue::runDeactivate(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new HandlerValue{il, deactivate()};
    }
}

void HandlerValue::runKill(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        kill();
        m_newContext = new VoidValue{il};
    }
}

memory::Type HandlerValue::type() const {
    return memory::Type::HandlerValue;
}

icString HandlerValue::typeName() {
    return "handler";
}

void HandlerValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (HandlerValue::*)(const memory::ArgList &)>
      methods{{"setup", &HandlerValue::runSetup},
              {"ativate", &HandlerValue::runActivate},
              {"deactivate", &HandlerValue::runDeactivate},
              {"kill", &HandlerValue::runKill}};

    runMethodNow<HandlerValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
