#include "listener-value.h++"

#include "handler-value.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/listen.h++>
#include <icL-il/structures/listen-handler.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

ListenerValue::ListenerValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

ListenerValue::ListenerValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

ListenerValue::ListenerValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

ListenerValue::ListenerValue(BaseValue * value)
    : BaseValue(value) {}

void ListenerValue::runHandle(const memory::ParamList & params) {
    m_newContext = new HandlerValue{il, handle(params)};
}

memory::Type ListenerValue::type() const {
    return memory::Type::ListenerValue;
}

icString ListenerValue::typeName() {
    return "listener";
}

void ListenerValue::runMeta(
  const icString & name, const memory::ParamList & params) {
    if (name == "handle") {
        runHandle(params);
    }
    else {
        BaseValue::runMeta(name, params);
    }
}

}  // namespace icL::ce
