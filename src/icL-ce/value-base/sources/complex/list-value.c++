#include "list-value.h++"

#include "bool-value.h++"
#include "int-value.h++"
#include "string-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-index-check.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

ListValue::ListValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

ListValue::ListValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

ListValue::ListValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

ListValue::ListValue(BaseValue * value)
    : BaseValue(value) {}

void ListValue::runEmpty() {
    m_newContext = new BoolValue{il, empty()};
}

void ListValue::runLength() {
    m_newContext = new IntValue(il, length());
}

void ListValue::runLast() {
    m_newContext = new StringValue{il, last()};
}

void ListValue::runAppend(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        append(icString(args[0]));
        m_newContext = new ListValue{this};
    }
}

void ListValue::runAt(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = new StringValue{il, at(args[0])};
    }
}

void ListValue::runContains(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new BoolValue{il, contains(args[0])};
    }
    else if (checkArgs(args, {Type::StringValue, Type::BoolValue})) {
        m_newContext = new BoolValue{il, contains(args[0], args[1])};
    }
}

void ListValue::runClear(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        clear();
        m_newContext = new ListValue{this};
    }
}

void ListValue::runCount(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new IntValue{il, count(args[0])};
    }
}

void ListValue::runFilter(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new ListValue{il, filter(icString(args[0]))};
    }
    else if (checkArgs(args, {Type::StringValue, Type::BoolValue})) {
        m_newContext = new ListValue{il, filter(icString(args[0]), args[1])};
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = new ListValue{il, filter(icRegEx(args[0]))};
    }
}

void ListValue::runIndexOf(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new IntValue{il, indexOf(icString(args[0]))};
    }
    else if (checkArgs(args, {Type::StringValue, Type::IntValue})) {
        m_newContext = new IntValue{il, indexOf(icString(args[0]), args[1])};
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = new IntValue{il, indexOf(icRegEx(args[0]))};
    }
    else if (checkArgs(args, {Type::RegexValue, Type::IntValue})) {
        m_newContext = new IntValue{il, indexOf(icRegEx(args[0]), args[1])};
    }
}

void ListValue::runInsert(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        insert(args[0], args[1]);
        m_newContext = new ListValue{this};
    }
}

void ListValue::runJoin(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new StringValue{il, join(args[0])};
    }
}

void ListValue::runLastIndexOf(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new IntValue{il, lastIndexOf(icString(args[0]))};
    }
    else if (checkArgs(args, {Type::StringValue, Type::IntValue})) {
        m_newContext =
          new IntValue{il, lastIndexOf(icString(args[0]), args[1])};
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = new IntValue{il, lastIndexOf(icRegEx(args[0]))};
    }
    else if (checkArgs(args, {Type::RegexValue, Type::IntValue})) {
        m_newContext = new IntValue{il, lastIndexOf(icRegEx(args[0]), args[1])};
    }
}

void ListValue::runMid(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = new ListValue{il, mid(args[0])};
    }
    else if (checkArgs(args, {Type::IntValue, Type::IntValue})) {
        m_newContext = new ListValue{il, mid(args[0], args[1])};
    }
}

void ListValue::runPrepend(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        prepend(args[1]);
        m_newContext = new ListValue{this};
    }
}

void ListValue::runMove(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue})) {
        move(args[0], args[1]);
        m_newContext = new ListValue{this};
    }
}

void ListValue::runRemoveAll(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        removeAll(args[0]);
        m_newContext = new ListValue{this};
    }
}

void ListValue::runRemoveAt(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        removeAt(args[0]);
        m_newContext = new ListValue{this};
    }
}

void ListValue::runRemoveDublicates(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        removeDuplicates();
        m_newContext = new ListValue{this};
    }
}

void ListValue::runRemoveFirst(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        removeFirst();
        m_newContext = new ListValue{this};
    }
}

void ListValue::runRemoveLast(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        removeLast();
        m_newContext = new ListValue{this};
    }
}

void ListValue::runRemoveOne(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        removeOne(args[0]);
    }
}

void ListValue::runReplaceInStrings(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::StringValue})) {
        m_newContext = new ListValue{il, replaceInStrings(args[0], args[1])};
    }
}

void ListValue::runSort(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        sort(true);
        m_newContext = new ListValue{il, getValue()};
    }
    else if (checkArgs(args, {Type::BoolValue})) {
        sort(args[0]);
        m_newContext = new ListValue{il, getValue()};
    }
}

Type ListValue::type() const {
    return Type::ListValue;
}

icString ListValue::typeName() {
    return "list";
}

void ListValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (ListValue::*)()> properties{
      {"empty", &ListValue::runEmpty},
      {"length", &ListValue::runLength},
      {"last", &ListValue::runLast}};

    runPropertyWithIndexCheck<ListValue, BaseValue, StringValue>(
      properties, prefix, name, [this](int i) -> icVariant { return at(i); });
}

void ListValue::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (ListValue::*)(const memory::ArgList &)>
      methods{{"append", &ListValue::runAppend},
              {"at", &ListValue::runAt},
              {"contains", &ListValue::runContains},
              {"clear", &ListValue::runClear},
              {"count", &ListValue::runCount},
              {"filter", &ListValue::runFilter},
              {"indexOf", &ListValue::runIndexOf},
              {"insert", &ListValue::runInsert},
              {"join", &ListValue::runJoin},
              {"lastIndexOf", &ListValue::runLastIndexOf},
              {"mid", &ListValue::runMid},
              {"prepend", &ListValue::runPrepend},
              {"move", &ListValue::runMove},
              {"removeAll", &ListValue::runRemoveAll},
              {"removeAt", &ListValue::runRemoveAt},
              {"removeDuplicates", &ListValue::runRemoveDublicates},
              {"removeFirst", &ListValue::runRemoveFirst},
              {"removeLast", &ListValue::runRemoveLast},
              {"removeOne", &ListValue::runRemoveOne},
              {"replaceInStrings", &ListValue::runReplaceInStrings},
              {"sort", &ListValue::runSort}};

    runMethodNow<ListValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
