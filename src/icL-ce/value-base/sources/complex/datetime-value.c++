#include "datetime-value.h++"

#include "bool-value.h++"
#include "int-value.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

DatetimeValue::DatetimeValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

DatetimeValue::DatetimeValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

DatetimeValue::DatetimeValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

DatetimeValue::DatetimeValue(BaseValue * value)
    : BaseValue(value) {}

CE * DatetimeValue::runDateProperty(
  const std::function<icVariant(const icDateTime &)> & getter,
  const std::function<void(icDateTime &, int)> &       setter) {
    auto * ret = new IntValue(this);

    ret->makeFunctional(
      [getter](BaseValue * dt) -> icVariant {
          return getter(dt->getValue(true).toDateTime());
      },
      [setter](BaseValue * dt, const icVariant & property) {
          auto value = dt->getValue(true).toDateTime();

          setter(value, property.toInt());
          dt->setValue(value);
      });

    return ret;
}

CE * DatetimeValue::runTimeProperty(
  const std::function<icVariant(const icDateTime &)> & getter,
  const std::function<void(icDateTime &, int)> &       setter) {
    auto * ret = new IntValue(this);

    ret->makeFunctional(
      [getter](BaseValue * dt) -> icVariant {
          return getter(dt->getValue(true).toDateTime());
      },
      [setter](BaseValue * dt, const icVariant & property) {
          auto value = dt->getValue(true).toDateTime();

          setter(value, property.toInt());
          dt->setValue(value);
      });

    return ret;
}

CE * DatetimeValue::day() {
    return runDateProperty(
      [](const icDateTime & date) { return date.day(); },
      [](icDateTime & date, int day) {
          date.setDate(date.year(), date.month(), day);
      });
}

CE * DatetimeValue::hour() {
    return runTimeProperty(
      [](const icDateTime & time) { return time.hour(); },
      [](icDateTime & time, int hour) {
          time.setHMS(hour, time.minute(), time.second());
      });
}

CE * DatetimeValue::minute() {
    return runTimeProperty(
      [](const icDateTime & time) { return time.hour(); },
      [](icDateTime & time, int minute) {
          time.setHMS(time.hour(), minute, time.second());
      });
}

CE * DatetimeValue::month() {
    return runDateProperty(
      [](const icDateTime & date) { return date.day(); },
      [](icDateTime & date, int month) {
          date.setDate(date.year(), month, date.day());
      });
}

CE * DatetimeValue::second() {
    return runTimeProperty(
      [](const icDateTime & time) { return time.hour(); },
      [](icDateTime & time, int second) {
          time.setHMS(time.hour(), time.minute(), second);
      });
}

CE * DatetimeValue::year() {
    return runDateProperty(
      [](const icDateTime & date) { return date.year(); },
      [](icDateTime & date, int year) {
          date.setDate(year, date.month(), date.day());
      });
}

void DatetimeValue::runDay() {
    m_newContext = day();
}

void DatetimeValue::runHour() {
    m_newContext = hour();
}

void DatetimeValue::runMinute() {
    m_newContext = minute();
}

void DatetimeValue::runMonth() {
    m_newContext = month();
}

void DatetimeValue::runSecond() {
    m_newContext = second();
}

void DatetimeValue::runValid() {
    m_newContext = new BoolValue{il, valid()};
}

void DatetimeValue::runYear() {
    m_newContext = year();
}

void DatetimeValue::runAddDays(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        addDays(args[0]);
        m_newContext = new DatetimeValue{this};
    }
}

void DatetimeValue::runAddMonths(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        addMonths(args[0]);
        m_newContext = new DatetimeValue{this};
    }
}

void DatetimeValue::runAddSecs(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        addSecs(args[0]);
        m_newContext = new DatetimeValue{this};
    }
}

void DatetimeValue::runAddYears(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        addYears(args[0]);
        m_newContext = new DatetimeValue{this};
    }
}

void DatetimeValue::runDaysTo(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DatetimeValue})) {
        m_newContext = new IntValue{il, daysTo(args[0])};
    }
}

void DatetimeValue::runSecsTo(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DatetimeValue})) {
        m_newContext = new IntValue{il, secsTo(args[0])};
    }
}

void DatetimeValue::runToTimeZone(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue})) {
        m_newContext = new DatetimeValue{il, toTimeZone(args[0], args[1])};
    }
}

void DatetimeValue::runToUTC(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new DatetimeValue{il, toUTC()};
    }
}

Type DatetimeValue::type() const {
    return Type::DatetimeValue;
}

icString DatetimeValue::typeName() {
    return "icDateTime";
}

void DatetimeValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (DatetimeValue::*)()> properties{
      {"day", &DatetimeValue::runDay},
      {"hour", &DatetimeValue::runHour},
      {"minute", &DatetimeValue::runMinute},
      {"month", &DatetimeValue::runMonth},
      {"second", &DatetimeValue::runSecond},
      {"valid", &DatetimeValue::runValid},
      {"year", &DatetimeValue::runYear}};

    runPropertyWithPrefixCheck<DatetimeValue, BaseValue>(
      properties, prefix, name);
}

void DatetimeValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (DatetimeValue::*)(const memory::ArgList &)>
      methods{{"addDays", &DatetimeValue::runAddDays},
              {"addMonths", &DatetimeValue::runAddMonths},
              {"addSecs", &DatetimeValue::runAddSecs},
              {"addYears", &DatetimeValue::runAddYears},
              {"daysTo", &DatetimeValue::runDaysTo},
              {"secsTo", &DatetimeValue::runSecsTo},
              {"toTimeZone", &DatetimeValue::runToTimeZone},
              {"toUTC", &DatetimeValue::runToUTC}};

    runMethodNow<DatetimeValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
