#include "lambda-js-value.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/lambda-target.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>

#include <void-value.h++>



namespace icL::ce {

LambdaJsValue::LambdaJsValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : LambdaValueBase(il, container, varName, readonly) {}

LambdaJsValue::LambdaJsValue(il::InterLevel * il, const icVariant & rvalue)
    : LambdaValueBase(il, rvalue) {}

LambdaJsValue::LambdaJsValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : LambdaValueBase(il, getter, setter) {}

LambdaJsValue::LambdaJsValue(BaseValue * value)
    : LambdaValueBase(value) {}

void LambdaJsValue::runRun(const memory::ArgList & args) {
    if (checkArgs(args)) {
        m_newContext = service::Factory::fromValue(
          il, il->server->executeSync(getCode().getCode(), prepareArgs(args)));
    }
}

void LambdaJsValue::runRunAsync(const memory::ArgList & args) {
    if (checkArgs(args)) {
        il->server->executeAsync(getCode().getCode(), prepareArgs(args));
        m_newContext = new VoidValue(il);
    }
}

icList<icVariant> LambdaJsValue::prepareArgs(const memory::ArgList & args) {
    icVariantList ret;

    for (const auto & arg : args) {
        ret.append(arg.value);
    }

    return ret;
}

Type LambdaJsValue::type() const {
    return Type::JavaScriptLambdaValue;
}

icString LambdaJsValue::typeName() {
    return "lambda-js";
}

void LambdaJsValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (LambdaJsValue::*)(const memory::ArgList &)>
         methods{{"run", &LambdaJsValue::runRun},
              {"runAsync", &LambdaJsValue::runRunAsync}};
    auto & lambda = _value();

    il->server->pushTarget(*lambda.data);
    runMethodNow<LambdaJsValue, LambdaValueBase>(methods, name, args);
    il->server->popTarget();
}

const il::JsLambda & LambdaJsValue::_value() {
    return getValue().toJsLambda();
}

il::CodeFragment LambdaJsValue::getCode() {
    return *getValue().toJsLambda().target;
}

}  // namespace icL::ce
