#include "element-value.h++"

#include "bool-value.h++"
#include "document-value.h++"
#include "int-value.h++"
#include "object-value.h++"
#include "string-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>

namespace icL::ce {

ElementValue::ElementValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

ElementValue::ElementValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

ElementValue::ElementValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

ElementValue::ElementValue(BaseValue * value)
    : BaseValue(value) {}

void ElementValue::runAttr(const icString & name) {
    m_newContext = new StringValue{il, attr(name)};
}

void ElementValue::runCss(const icString & name) {
    m_newContext = new StringValue{il, css(name)};
}

void ElementValue::runProp(const icString & name) {
    m_newContext = service::Factory::fromValue(il, prop(name));
}

void ElementValue::runDocument() {
    m_newContext = new DocumentValue{il, document()};
}

void ElementValue::runEnabled() {
    m_newContext = new BoolValue{il, enabled()};
}

void ElementValue::runRect() {
    m_newContext = new ObjectValue{il, rect()};
}

void ElementValue::runSelected() {
    m_newContext = new BoolValue{il, selected()};
}

void ElementValue::runTag() {
    m_newContext = new StringValue{il, tag()};
}

void ElementValue::runText() {
    m_newContext = new StringValue{il, text()};
}

void ElementValue::runVisible() {
    m_newContext = new BoolValue{il, visible()};
}

void ElementValue::runChild(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = new ElementValue{il, child(args[0])};
    }
}

void ElementValue::runChildren(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new ElementValue{il, children()};
    }
}

void ElementValue::runClear(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        clear();
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runClick(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::ClickData cd;

        cd.loadDictionary(args[0]);
        click(cd);

        m_newContext = new ElementValue{this};
    }
    else if (checkArgs(args, {})) {
        click({});
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runClosest(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new ElementValue{il, closest(args[0])};
    }
}

void ElementValue::runCopy(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new ElementValue{il, copy()};
    }
}

void ElementValue::runFastType(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        fastType(args[0]);
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runForceClick(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::ClickData cd;

        cd.loadDictionary(args[0]);
        forceClick(cd);

        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runForceType(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        forceType(args[0]);
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runHover(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::HoverData hd;

        hd.loadDictionary(args[0]);
        hover(hd);

        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runKeyDown(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        keyDown(args[0], args[1]);
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runKeyPress(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue, Type::StringValue})) {
        keyPress(args[0], args[1], args[2]);
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runKeyUp(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        keyUp(args[0], args[1]);
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runMouseDown(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::MouseData md;

        md.loadDictionary(args[0]);
        mouseDown(md);

        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runMouseUp(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::MouseData md;

        md.loadDictionary(args[0]);
        mouseUp(md);

        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runNext(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new ElementValue{il, service::ElementValue::next()};
    }
}

void ElementValue::runParent(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new ElementValue{il, parent()};
    }
}

void ElementValue::runPaste(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        paste(args[0]);
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runPrev(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new ElementValue{il, service::ElementValue::prev()};
    }
}

void ElementValue::runScreenshot(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new StringValue{il, screenshot()};
    }
}

void ElementValue::runSendKeys(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        sendKeys(args[0], args[1]);
        m_newContext = new ElementValue{this};
    }
    else if (checkArgs(args, {Type::StringValue})) {
        sendKeys(0, args[0]);
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runSuperClick(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        superClick();
        m_newContext = new ElementValue{this};
    }
}

void ElementValue::runOwnProperty(const icString & name) {
    static icObject<icString, void (ElementValue::*)()> properties{
      {"doc", &ElementValue::runDocument},
      {"enabled", &ElementValue::runEnabled},
      {"rect", &ElementValue::runRect},
      {"selected", &ElementValue::runSelected},
      {"tag", &ElementValue::runTag},
      {"text", &ElementValue::runText},
      {"visible", &ElementValue::runVisible}};

    Prefix prefix = Prefix::None;
    runPropertyNow<ElementValue, BaseValue>(properties, prefix, name);
}

Type ElementValue::type() const {
    return Type::ElementValue;
}

icString ElementValue::typeName() {
    return "element";
}

void ElementValue::runProperty(Prefix prefix, const icString & name) {
    il->server->pushTarget(_value().target);

    switch (prefix) {
    case Prefix::Attr:
        runAttr(name);
        break;

    case Prefix::CSS:
        runCss(name);
        break;

    case Prefix::Prop:
        runProp(name);
        break;

    case Prefix::None:
        runOwnProperty(name);
        break;
    }

    il->server->popTarget();
}

void ElementValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (ElementValue::*)(const memory::ArgList &)>
      methods{{"child", &ElementValue::runChild},
              {"children", &ElementValue::runChildren},
              {"clear", &ElementValue::runClear},
              {"click", &ElementValue::runClick},
              {"closest", &ElementValue::runClosest},
              {"copy", &ElementValue::runCopy},
              {"fastType", &ElementValue::runFastType},
              {"forceClick", &ElementValue::runClick},
              {"forceType", &ElementValue::runForceType},
              {"hover", &ElementValue::runHover},
              {"keyDown", &ElementValue::runKeyUp},
              {"keyPress", &ElementValue::runKeyPress},
              {"keyUp", &ElementValue::runKeyUp},
              {"mouseDown", &ElementValue::runMouseDown},
              {"mouseUp", &ElementValue::runMouseUp},
              {"next", &ElementValue::runNext},
              {"parent", &ElementValue::runParent},
              {"paste", &ElementValue::runPaste},
              {"prev", &ElementValue::runPrev},
              {"screenshot", &ElementValue::runScreenshot},
              {"sendKeys", &ElementValue::runSendKeys},
              {"superClick", &ElementValue::runSuperClick}};

    il->server->pushTarget(_value().target);
    runMethodNow<ElementValue, BaseValue>(methods, name, args);
    il->server->popTarget();
}


}  // namespace icL::ce
