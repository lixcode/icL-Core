#include "session-value.h++"

#include "../base/int-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/string-value.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-browser/window/alert.h++>
#include <icL-ce-value-browser/window/tabs.h++>
#include <icL-ce-value-browser/window/windows.h++>



namespace icL::ce {

SessionValue::SessionValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

SessionValue::SessionValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

SessionValue::SessionValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

SessionValue::SessionValue(BaseValue * value)
    : BaseValue(value) {}

CE * SessionValue::runIntProperty(
  int (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(int)) {
    auto * ret = new IntValue{this};
    auto * il  = this->il;

    ret->makeFunctional(
      [il, getter](BaseValue *) -> icVariant {
          return (il->server->*getter)();
      },
      [il, setter](BaseValue *, const icVariant & var) {
          (il->server->*setter)(var.toInt());
      });

    return ret;
}

CE * SessionValue::alert() {
    return new Alert{il, *_value().data};
}

CE * SessionValue::implicitTimeout() {
    return runIntProperty(
      &il::FrontEnd::implicitTimeout, &il::FrontEnd::setImplicitTimeout);
}

CE * SessionValue::pageLoadTimeout() {
    return runIntProperty(
      &il::FrontEnd::pageLoadTimeout, &il::FrontEnd::setPageLoadTimeOut);
}

CE * SessionValue::scriptTimeout() {
    return runIntProperty(
      &il::FrontEnd::scriptTimeout, &il::FrontEnd::setScriptTimeout);
}

CE * SessionValue::tabs() {
    return new Tabs{il, *_value().data};
}

CE * SessionValue::url() {
    auto * ret = new IntValue{this};
    auto * il  = this->il;

    ret->makeFunctional(
      [il](BaseValue *) -> icVariant { return il->server->url(); },
      [il](BaseValue *, const icVariant & value) {
          il->server->load(value.toString());
      });

    return ret;
}

CE * SessionValue::windows() {
    return new Windows{il, *_value().data};
}

void SessionValue::runAlert() {
    m_newContext = alert();
}

void SessionValue::runImplicitTimeout() {
    m_newContext = implicitTimeout();
}

void SessionValue::runPageLoadTimeout() {
    m_newContext = pageLoadTimeout();
}

void SessionValue::runScriptTimeout() {
    m_newContext = scriptTimeout();
}

void SessionValue::runSource() {
    m_newContext = new StringValue{il, source()};
}

void SessionValue::runTabs() {
    m_newContext = tabs();
}

void SessionValue::runTitle() {
    m_newContext = new StringValue{il, title()};
}

void SessionValue::runUrl() {
    m_newContext = url();
}

void SessionValue::runWindows() {
    m_newContext = windows();
}

void SessionValue::runBack(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        back();
        m_newContext = new SessionValue{this};
    }
}

void SessionValue::runClose(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = new VoidValue{il};
    }
}

void SessionValue::runForward(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        forward();
        m_newContext = new SessionValue{this};
    }
}

void SessionValue::runRefresh(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        refresh();
        m_newContext = new SessionValue{this};
    }
}

void SessionValue::runScreenshot(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new StringValue{il, screenshot()};
    }
}

void SessionValue::runSwitchTo(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        switchTo();
        m_newContext = new SessionValue{this};
    }
}

void SessionValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (SessionValue::*)()> properties{
      {"alert", &SessionValue::runAlert},
      {"implicitTimeout", &SessionValue::runImplicitTimeout},
      {"pageLoadTimeout", &SessionValue::runPageLoadTimeout},
      {"scriptTimeout", &SessionValue::runScriptTimeout},
      {"source", &SessionValue::runSource},
      {"tabs", &SessionValue::runTabs},
      {"title", &SessionValue::runTitle},
      {"url", &SessionValue::runUrl},
      {"windows", &SessionValue::runWindows}};

    il->server->pushTarget(*_value().data);
    runPropertyWithPrefixCheck<SessionValue, BaseValue>(
      properties, prefix, name);
    il->server->popTarget();
}

void SessionValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (SessionValue::*)(const memory::ArgList &)>
      methods{{"back", &SessionValue::runBack},
              {"close", &SessionValue::runClose},
              {"forward", &SessionValue::runForward},
              {"refresh", &SessionValue::runRefresh},
              {"screenshot", &SessionValue::runScreenshot},
              {"switchTo", &SessionValue::runSwitchTo}};

    il->server->pushTarget(*_value().data);
    runMethodNow<SessionValue, BaseValue>(methods, name, args);
    il->server->popTarget();
}

il::Session icL::ce::SessionValue::_value() {
    return getValue();
}

memory::Type SessionValue::type() const {
    return Type::SessionValue;
}

icString SessionValue::typeName() {
    return "session";
}

}  // namespace icL::ce
