#include "int-value.h++"

namespace icL::ce {

IntValue::IntValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

IntValue::IntValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

IntValue::IntValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

IntValue::IntValue(BaseValue * value)
    : BaseValue(value) {}

Type IntValue::type() const {
    return Type::IntValue;
}

icString IntValue::typeName() {
    return "int";
}

}  // namespace icL::ce
