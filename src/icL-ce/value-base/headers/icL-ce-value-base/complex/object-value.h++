#ifndef ce_ObjectValue
#define ce_ObjectValue

#include <icL-ce-base/value/base-value.h++>



class icRect;

namespace icL::ce {

/**
 * @brief The ObjectValue class represents an `object` value
 */
class ObjectValue : public BaseValue
{
public:
    /// @brief ObjectValue calls BaseValue(il, container, varName, readonly)
    ObjectValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief ObjectValue calls BaseValue(il, rvalue)
    ObjectValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief ObjectValue calls BaseValue(il, getter, setter)
    ObjectValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief ObjectValue calls BaseValue(il, rvalue)
    ObjectValue(il::InterLevel * il, const icRect & rect);

    /// @brief ObjectValue calls BaseValue(value)
    ObjectValue(BaseValue * value);

    /// `object.get (name : string) : any`
    void runGet(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
    void runMethod(
      const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_ObjectValue
