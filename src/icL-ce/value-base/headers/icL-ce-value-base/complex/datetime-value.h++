#ifndef ce_DatetimeValue
#define ce_DatetimeValue

#include <icL-service-value-base/complex/datetime-value.h++>

#include <icL-ce-base/value/base-value.h++>



class icDateTime;

namespace icL::ce {

/**
 * @brief The DatetimeValue class represents a `icDateTime` value
 */
class DatetimeValue
    : public BaseValue
    , public service::DatetimeValue
{
public:
    /// @brief DatetimeValue calls BaseValue(il, container, varName, readonly)
    DatetimeValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief DatetimeValue calls BaseValue(il, rvalue)
    DatetimeValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief DatetimeValue calls BaseValue(il, getter, setter)
    DatetimeValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief DatetimeValue calls BaseValue(value)
    DatetimeValue(BaseValue * value);

private:
    CE * runDateProperty(
      const std::function<icVariant(const icDateTime &)> & getter,
      const std::function<void(icDateTime &, int)> &       setter);
    CE * runTimeProperty(
      const std::function<icVariant(const icDateTime &)> & getter,
      const std::function<void(icDateTime &, int)> &       setter);

public:
    // properties level 1

    /// `[r/w] icDateTime'day : int`
    CE * day();

    /// `[r/w] icDateTime'hour : int`
    CE * hour();

    /// `[r/w] icDateTime'minute : int `
    CE * minute();

    /// `[r/w] icDateTime'month : int`
    CE * month();

    /// `[r/w] icDateTime'second : int`
    CE * second();

    /// `[r/w] icDateTime'year : int`
    CE * year();

    // properties level 2

    /// `icDateTime'day`
    void runDay();

    /// `icDateTime'hour`
    void runHour();

    /// `icDateTime'minute`
    void runMinute();

    /// `icDateTime'month`
    void runMonth();

    /// `icDateTime'second`
    void runSecond();

    /// `icDateTime'valid`
    void runValid();

    /// `icDateTime'year`
    void runYear();

    // methods level 2

    /// `icDateTime.adddDays`
    void runAddDays(const memory::ArgList & args);

    /// `icDateTime.addMonth`
    void runAddMonths(const memory::ArgList & args);

    /// `icDateTime.addSecs`
    void runAddSecs(const memory::ArgList & args);

    /// `icDateTime.addYear`
    void runAddYears(const memory::ArgList & args);

    /// `icDateTime.daysTo`
    void runDaysTo(const memory::ArgList & args);

    /// `icDateTime.secsTo`
    void runSecsTo(const memory::ArgList & args);

    /// `icDateTime.toTimeZone`
    void runToTimeZone(const memory::ArgList & args);

    /// `icDateTime.toUTC`
    void runToUTC(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
    void runMethod(
      const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_DatetimeValue
