#ifndef ce_SessionValue
#define ce_SessionValue

#include <icL-service-value-base/browser/session-value.h++>

#include <icL-ce-base/value/base-value.h++>



namespace icL {

namespace il {
struct Session;
class FrontEnd;
}  // namespace il

namespace ce {

class SessionValue
    : public BaseValue
    , public service::SessionValue
{
public:
    /// @brief SessionValue calls BaseValue(il, container, varName, readonly)
    SessionValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief SessionValue calls BaseValue(il, rvalue)
    SessionValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief SessionValue calls BaseValue(il, getter, setter)
    SessionValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief SessionValue calls BaseValue(value)
    SessionValue(BaseValue * value);

private:
    /**
     * @brief runIntProperty runs a int property
     * @return a contextual entity for needed int propeperty
     */
    CE * runIntProperty(
      int (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(int));

public:
    // properties level 1

    /// `session'alert : Alert`
    CE * alert();

    /// `[r/w] session'implicitTimeout : int`
    CE * implicitTimeout();

    /// `[r/w] session'pageLoadTimeout : int`
    CE * pageLoadTimeout();

    /// `[r/w] session'scriptTimeout : int`
    CE * scriptTimeout();

    /// `session'tabs : Tabs`
    CE * tabs();

    /// `[r/w] session'url : string`
    CE * url();

    /// `session'windows : Windows`
    CE * windows();

    // properties level 2

    /// `session'alert`
    void runAlert();

    /// `session'implicitTimeout`
    void runImplicitTimeout();

    /// `session'loadTimeout`
    void runPageLoadTimeout();

    /// `session'scriptTimeout`
    void runScriptTimeout();

    /// `session'source`
    void runSource();

    /// `session'tabs`
    void runTabs();

    /// `session'title`
    void runTitle();

    /// `session'url`
    void runUrl();

    /// `session'windows`
    void runWindows();

    // methods level 2

    /// `session.back`
    void runBack(const memory::ArgList & args);

    /// `session.close`
    void runClose(const memory::ArgList & args);

    /// `session.forward`
    void runForward(const memory::ArgList & args);

    /// `session.refresh`
    void runRefresh(const memory::ArgList & args);

    /// `session.screenshot`
    void runScreenshot(const memory::ArgList & args);

    /// `session.switchTo`
    void runSwitchTo(const memory::ArgList & args);

protected:
    il::Session _value();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_SessionValue
