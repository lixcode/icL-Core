#ifndef ce_BoolValue
#define ce_BoolValue

#include <icL-ce-base/value/base-value.h++>



namespace icL::ce {

/**
 * @brief The BoolValue class represents a `bool` value
 */
class BoolValue : public BaseValue
{
public:
    /// @brief BoolValue calls BaseValue(il, container, varName, readonly)
    BoolValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief BoolValue calls BaseValue(il, rvalue)
    BoolValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief BoolValue calls BaseValue(il, getter, setter)
    BoolValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief BoolValue calls BaseValue(value)
    BoolValue(BaseValue * value);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_BoolValue
