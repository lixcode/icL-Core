#ifndef ce_IntValue
#define ce_IntValue

#include <icL-ce-base/value/base-value.h++>



namespace icL::ce {

/**
 * @brief The IntValue class represents an `int` value
 */
class IntValue : public BaseValue
{
public:
    /// @brief IntValue calls BaseValue(il, container, varName, readonly)
    IntValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief IntValue calls BaseValue(il, rvalue)
    IntValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief IntValue calls BaseValue(il, getter, setter)
    IntValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief IntValue calls BaseValue(value)
    IntValue(BaseValue * value);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_IntValue
