#ifndef ce_LambdaJsValue
#define ce_LambdaJsValue

#include "lambda-base-value.h++"



namespace icL::ce {

class LambdaJsValue : public LambdaValueBase
{
public:
    /// @brief LambdaJsValue calls BaseValue(il, container, varName, readonly)
    LambdaJsValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief LambdaJsValue calls BaseValue(il, rvalue)
    LambdaJsValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief LambdaJsValue calls BaseValue(il, getter, setter)
    LambdaJsValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief LambdaJsValue calls BaseValue(value)
    LambdaJsValue(BaseValue * value);

private:
    // level 2

    /// `lambda-js.run (any..) : any`
    void runRun(const memory::ArgList & args);

    /// `lambda-js.runAsync (any..) : void`
    void runRunAsync(const memory::ArgList & args);

    icList<icVariant> prepareArgs(const memory::ArgList & args);

    // Value interface
public:
    memory::Type type() const override;
    icString     typeName() override;

    void runMethod(
      const icString & name, const memory::ArgList & args) override;

private:
    const il::JsLambda & _value();

    // LambdaValueBase interface
public:
    il::CodeFragment getCode() override;
};

}  // namespace icL::ce

#endif  // ce_LambdaJsValue
