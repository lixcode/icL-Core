#ifndef ce_LambdaValueBase
#define ce_LambdaValueBase

#include <icL-ce-base/value/base-value.h++>


namespace icL::ce {

/**
 * @brief The LambdaValueBase class represent a common interface for all lambda
 * values: `lambda-icl`, `lambda-js`, `lambda-sql`
 */
class LambdaValueBase : public BaseValue
{
public:
    /// @brief LambdaValueBase calls BaseValue(il, container, varName, readonly)
    LambdaValueBase(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief LambdaValueBase calls BaseValue(il, rvalue)
    LambdaValueBase(il::InterLevel * il, const icVariant & rvalue);

    /// @brief LambdaValueBase calls BaseValue(il, getter, setter)
    LambdaValueBase(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief LambdaValueBase calls BaseValue(value)
    LambdaValueBase(BaseValue * value);

    /**
     * @brief getCode gets the code of lambda expression
     * @return the code of lambda expression
     */
    virtual il::CodeFragment getCode() = 0;
};

}  // namespace icL::ce

#endif  // ce_LambdaValueBase
