#ifndef ce_FileValue
#define ce_FileValue

#include <icL-service-value-base/system/file-value.h++>

#include <icL-ce-base/value/base-value.h++>


namespace icL {

namespace il {
struct File;
}

namespace ce {

class FileValue
    : public BaseValue
    , public service::FileValue
{
public:
    /// @brief FileValue calls BaseValue(il, container, varName, readonly)
    FileValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief FileValue calls BaseValue(il, rvalue)
    FileValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief FileValue calls BaseValue(il, getter, setter)
    FileValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief FileValue calls BaseValue(value)
    FileValue(BaseValue * value);

    // properties level 2

    /// `file'format`
    void runFormat();

    // methods level 2

    /// `file.close`
    void runClose(const memory::ArgList & args);

    /// `file.delete`
    void runDelete(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_FileValue
