#ifndef ce_ListenerValue
#define ce_ListenerValue

#include <icL-service-value-base/system/listener-value.h++>

#include <icL-ce-base/value/base-value.h++>



namespace icL::ce {

class ListenerValue
    : public BaseValue
    , public service::ListenerValue
{
public:
    /// @brief QueryValue calls BaseValue(il, container, varName, readonly)
    ListenerValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief QueryValue calls BaseValue(il, rvalue)
    ListenerValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief QueryValue calls BaseValue(il, getter, setter)
    ListenerValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief QueryValue calls BaseValue(value)
    ListenerValue(BaseValue * value);

    // methods level 2

    /// `listener.handle (params...) : handler`
    void runHandle(const memory::ParamList & params);

    // Value interface
public:
    memory::Type type() const override;
    icString     typeName() override;
    void         runMeta(
              const icString & name, const memory::ParamList & params) override;
};

}  // namespace icL::ce

#endif  // ce_ListenerValue
