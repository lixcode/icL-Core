#ifndef ce_Range
#define ce_Range

#include <icL-service-keyword/loop/range.h++>

#include <icL-ce-base/keyword/loop-keyword.h++>



namespace icL::ce {

class Range
    : public LoopKeyword
    , public service::Range
{
public:
    Range(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;

    // INode interface
protected:
    il::InterLevel * _il() override;
};

}  // namespace icL::ce

#endif  // ce_Range
