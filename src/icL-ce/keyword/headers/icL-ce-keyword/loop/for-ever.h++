#ifndef ce_ForEver
#define ce_ForEver

#include "for-base.h++"

#include <icL-service-keyword/loop/for-ever.h++>



namespace icL::ce {

class ForEver
    : public ForBase
    , public service::ForEver
{
public:
    ForEver(il::InterLevel * il, int xTimes);

    // CE interface
public:
    StepType runNow() override;
    icString toString() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::ce

#endif  // ce_ForEver
