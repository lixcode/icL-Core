#ifndef ce_ForProxy
#define ce_ForProxy

#include "for-base.h++"



namespace icL::ce {

class ForProxy : public ForBase
{
public:
    ForProxy(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    icString toString() override;

    // CE interface
protected:
    CE * firstToReplace() override;
    CE * lastToReplace() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;

private:
    void checkFlags(int mask);

    ///< flags of keyword modifiers
    int flags = None;

    /**
     * @brief The Flags enum defines the possibile `for` modificators
     */
    enum Flags {
        None    = 0x0,   ///< No flag selected
        Alt     = 0x01,  ///< Use the alternative for
        Ever    = 0x02,  ///< Infinite loop
        XTimes  = 0x04,  ///< Fixed count loop
        MaxX    = 0x08,  ///< Miximum number of conllection iteratiions
        Reverse = 0x10,  ///< Iterates the collection from end to beggining
        AllOn   = 0x1f   ///< All flags activated
    };

    int maxXnumber   = -1;  ///< number of maxX modifier
    int xTimesNumber = -1;  ///< number of Xtimes modifier
};

}  // namespace icL::ce

#endif  // ce_ForProxy
