#ifndef ce_Listen
#define ce_Listen

#include <icL-ce-base/keyword/advanced-keyword.h++>



namespace icL::ce {

class Listen : public AdvancedKeyword
{
public:
    Listen(il::InterLevel * il);
};

}  // namespace icL::ce

#endif  // ce_Listen
