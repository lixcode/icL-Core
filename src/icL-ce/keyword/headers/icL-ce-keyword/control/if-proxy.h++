#ifndef ce_IfProxy
#define ce_IfProxy

#include "if-base.h++"



namespace icL::ce {

class IfProxy : public IfBase
{
    bool notModifier = false;

public:
    IfProxy(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

protected:
    CE * firstToReplace() override;
    CE * lastToReplace() override;

    // IfBase interface
protected:
    bool hasNotModifier() override;
    void setNotModifier() override;
};

}  // namespace icL::ce

#endif  // ce_IfProxy
