﻿#ifndef ce_Exists
#define ce_Exists

#include <icL-service-keyword/control/exists.h++>

#include <icL-ce-base/keyword/control-keyword.h++>



namespace icL::ce {

class Exists
    : public ControlKeyword
    , public service::Exists
{
public:
    Exists(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    bool      hasValue() override;
    icVariant getValue(bool excludeFunctional) override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    CE * lastToReplace() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::ce

#endif  // ce_Exists
