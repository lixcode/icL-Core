#ifndef ce_Any
#define ce_Any

#include <icL-ce-base/keyword/control-keyword.h++>



namespace icL::ce {

class Any : public ControlKeyword
{
public:
    Any(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;
    CE * firstToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

private:
    bool executed = false;  ///< executed is true if the code was executed
};

}  // namespace icL::ce

#endif  // ce_Any
