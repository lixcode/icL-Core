#include "now.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-ce-base/value/base-value.h++>
#include <icL-ce-value-base/base/void-value.h++>



namespace icL::ce {

Now::Now(il::InterLevel * il)
    : AdvancedKeyword(il) {}

int Now::currentRunRank(bool rtl) {
    return rtl ? 9 : -1;
}

StepType Now::runNow() {
    auto * value = dynamic_cast<BaseValue *>(m_next);

    if (value->isLValue()) {
        value->rebind();
        m_newContext = new VoidValue{value};
    }

    return StepType::CommandEnd;
}

icString Now::toString() {
    return "now";
}

Role Now::role() {
    return Role::Now;
}

CE * Now::lastToReplace() {
    return m_next;
}

const icSet<Role> & Now::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole, Role::Assign, Role::Comma};
    return roles;
}

const icSet<Role> & Now::acceptedNexts() {
    static const icSet<Role> roles{Role::Value};
    return roles;
}

}  // namespace icL::ce
