#include "assert.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce-operators-ALU/system/context.h++>
#include <icL-ce-value-base/base/void-value.h++>



namespace icL::ce {

Assert::Assert(il::InterLevel * il)
    : AdvancedKeyword(il) {}

icString Assert::toString() {
    return "assert";
}

int Assert::currentRunRank(bool) {
    return m_next->next() == nullptr ? 9 : -1;
}

StepType Assert::runNow() {
    return transact();
}

Role Assert::role() {
    return Role::Assert;
}

const icSet<Role> & Assert::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole};
    return roles;
}

const icSet<Role> & Assert::acceptedNexts() {
    static const icSet<Role> roles{Role::ValueContext};
    return roles;
}

CE * Assert::lastToReplace() {
    return m_next;
}

void Assert::initialize() {
    auto commands =
      il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

    if (commands.length() == 1) {
        condition = commands[0];
    }
    else if (commands.length() == 2) {
        condition = commands[0];
        message   = commands[1];
    }
    else {
        il->vm->syssig("assert: Wrong number of commands");
    }
}

void Assert::finalize() {
    m_newContext = new VoidValue{il};
}

}  // namespace icL::ce
