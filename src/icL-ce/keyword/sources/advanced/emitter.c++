#include "emitter.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce-operators-ALU/system/context.h++>
#include <icL-ce-value-base/base/int-value.h++>

namespace icL::ce {

Emitter::Emitter(il::InterLevel * il)
    : AdvancedKeyword(il) {}

icString Emitter::toString() {
    icString ret = "emitter";

    if (alive)
        ret += "-alive";

    return ret;
}

int Emitter::currentRunRank(bool rtl) {
    return rtl ? 9 : -1;
}

StepType Emitter::runNow() {
    return transact();
}

Role Emitter::role() {
    return Role::Emitter;
}

CE * Emitter::lastToReplace() {
    return getLast();
}

const icSet<Role> & Emitter::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole};
    return roles;
}

const icSet<Role> & Emitter::acceptedNexts() {
    static const icSet<Role> roles{Role::RunContext};
    return roles;
}

void Emitter::initialize() {
    bool ok   = true;
    CE * it   = m_next;
    Role prev = Role::Emitter;

    while (it != nullptr && ok) {
        switch (it->role()) {
        case Role::RunContext:
            ok = prev == Role::Emitter || prev == Role::Slot;
            break;

        case Role::Slot:
            ok = prev == Role::RunContext;
            break;

        default:
            ok = false;
        }

        prev = it->role();
        it   = dynamic_cast<CE *>(it->next());
    }

    if (!ok || prev != Role::RunContext) {
        il->vm->syssig("Syntax error in emitter-slot statement");
        return;
    }

    emmiter = dynamic_cast<Context *>(m_next)->getCode();

    it = dynamic_cast<CE *>(m_next->next());

    while (it != nullptr) {
        if (it->role() == Role::Slot) {
            slotsPtr.append(dynamic_cast<service::Slot *>(it));
        }
        else {
            slotsCode.append(dynamic_cast<Context *>(it)->getCode());
        }

        it = dynamic_cast<CE *>(it->next());
    }
}

void Emitter::finalize() {
    m_newContext = new IntValue{il, error.code};
}

void Emitter::giveModifiers(const icStringList & modifiers) {
    for (auto & mod : modifiers) {
        if (mod == "alive") {
            if (alive) {
                il->vm->cpe_sig("emitter: `alive` modifier setted twince");
            }
            else {
                alive = true;
            }
        }
        else {
            il->vm->cpe_sig("emitter: unknown modifier: " % mod);
        }
    }
}


}  // namespace icL::ce
