#include "any.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-base/value/base-value.h++>
#include <icL-ce-operators-ALU/system/context.h++>

#include <icL-memory/structures/function-call.h++>

#include <cassert>



namespace icL::ce {

Any::Any(il::InterLevel * il)
    : ControlKeyword(il) {}

icString Any::toString() {
    return "any";
}

int Any::currentRunRank(bool /*rtl*/) {
    return m_next->role() == Role::Value ? 9 : -1;
}

StepType Any::runNow() {
    if (executed) {
        m_newContext = service::Factory::fromValue(
          il, dynamic_cast<BaseValue *>(m_next)->getValue());
        return StepType::CommandEnd;
    }

    auto * value = dynamic_cast<BaseValue *>(m_next);

    if (value->type() == Type::PackedValue) {
        il->vm->syssig(
          "for any: round brackets content must return a single value");
    }
    else {
        memory::FunctionCall fcall;

        fcall.code = dynamic_cast<Context *>(m_next->next())->getCode();
        fcall.args.append({"@", dynamic_cast<BaseValue *>(m_next)->getValue()});

        il->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != memory::Signals::NoError) {
                il->vm->signal(ret.signal);
            }
            else {
                executed = true;
            }
            return false;
        });
    }

    return StepType::CommandIn;
}

Role Any::role() {
    return Role::Any;
}

CE * Any::firstToReplace() {
    return m_prev;
}

const icSet<Role> & Any::acceptedPrevs() {
    static const icSet<Role> roles{Role::For};
    return roles;
}

const icSet<Role> & Any::acceptedNexts() {
    static const icSet<Role> roles{Role::ValueContext, Role::Value};
    return roles;
}

}  // namespace icL::ce
