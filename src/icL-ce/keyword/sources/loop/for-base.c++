#include "for-base.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-keyword/loop/for-collection.h++>
#include <icL-service-keyword/loop/for-ever.h++>
#include <icL-service-keyword/loop/for-parametric.h++>

#include <icL-ce-operators-ALU/system/context.h++>
#include <icL-ce-value-base/base/int-value.h++>

#include <functional>



namespace icL::ce {

ForBase::ForBase(il::InterLevel * il)
    : LoopKeyword(il) {}

int ForBase::currentRunRank(bool /*rtl*/) {
    return m_prev == nullptr && m_next->next() != nullptr &&
               dynamic_cast<CE *>(m_next->next())->role() == Role::RunContext
             ? 9
             : -1;
}

Role ForBase::role() {
    return Role::For;
}

const icSet<Role> & ForBase::acceptedPrevs() {
    static icSet<Role> roles{Role::NoRole};
    return roles;
}

const icSet<Role> & ForBase::acceptedNexts() {
    static icSet<Role> roles{Role::ValueContext, Role::Any};
    return roles;
}

}  // namespace icL::ce
