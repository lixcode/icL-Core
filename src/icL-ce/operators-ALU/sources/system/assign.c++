#include "assign.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/cookie-data.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/file-target.h++>
#include <icL-il/structures/signal.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-service-main/args/listify.h++>

#include <icL-ce-base/value/packed-value.h++>
#include <icL-ce-literal/static/default-parameter.h++>
#include <icL-ce-literal/static/field.h++>
#include <icL-ce-literal/static/identifier.h++>
#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/double-value.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/base/string-value.h++>
#include <icL-ce-value-base/browser/cookie-value.h++>
#include <icL-ce-value-base/browser/element-value.h++>
#include <icL-ce-value-base/browser/elements-value.h++>
#include <icL-ce-value-base/browser/session-value.h++>
#include <icL-ce-value-base/browser/tab-value.h++>
#include <icL-ce-value-base/browser/window-value.h++>
#include <icL-ce-value-base/complex/datetime-value.h++>
#include <icL-ce-value-base/complex/list-value.h++>
#include <icL-ce-value-base/complex/object-value.h++>
#include <icL-ce-value-base/complex/regex-value.h++>
#include <icL-ce-value-base/complex/set-value.h++>
#include <icL-ce-value-base/system/db-value.h++>
#include <icL-ce-value-base/system/file-value.h++>
#include <icL-ce-value-base/system/query-value.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>
#include <icL-vm/vmlayer.h++>

namespace icL::ce {

using memory::ContainerType;
using memory::Type;

Assign::Assign(il::InterLevel * il)
    : SystemOperator(il) {}

template <typename ValueClass>
void Assign::tryToAssign(
  const memory::Argument & arg, memory::Type type, const icVariant & value) {
    auto * var = dynamic_cast<BaseValue *>(m_prev);

    if (var->type() == Type::PackedValue) {
        if (
          arg.container != nullptr &&
          arg.container->getType(arg.varName) == type) {
            arg.container->setValue(arg.varName, value);
        }

        if (m_newContext == nullptr) {
            m_newContext = new PackedValue{dynamic_cast<BaseValue *>(m_next)};
        }
    }
    else if (var->type() == Type::VoidValue || var->type() == type) {
        var->setValue(value);
        m_newContext = new ValueClass{var};
    }
}

void Assign::assignBool(const memory::Argument & arg, bool value) {
    tryToAssign<BoolValue>(arg, Type::BoolValue, value);
}

void Assign::assignDouble(const memory::Argument & arg, double value) {
    tryToAssign<DoubleValue>(arg, Type::DoubleValue, value);
}

void Assign::assignInt(const memory::Argument & arg, int value) {
    tryToAssign<IntValue>(arg, Type::IntValue, value);
}

void Assign::assignString(
  const memory::Argument & arg, const icString & value) {
    tryToAssign<StringValue>(arg, Type::StringValue, value);
}

void Assign::assignList(
  const memory::Argument & arg, const icStringList & value) {
    tryToAssign<ListValue>(arg, Type::ListValue, value);
}

void Assign::assignDatetime(
  const memory::Argument & arg, const icDateTime & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<DatetimeValue>(arg, Type::DatetimeValue, value);
    }
}

void Assign::assignRegex(const memory::Argument & arg, const icRegEx & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<RegexValue>(arg, Type::RegexValue, value);
    }
}

void Assign::assignObject(
  const memory::Argument & arg, const memory::Object & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<ObjectValue>(arg, Type::ObjectValue, value);
    }
}

void Assign::assignSet(
  const memory::Argument & arg, const memory::Set & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<SetValue>(arg, Type::SetValue, value);
    }
}

void Assign::assignCookie(
  const memory::Argument & arg, const il::Cookie & value) {
    if (arg.container->getContainerType() == ContainerType::Stack) {
        tryToAssign<CookieValue>(arg, Type::CookieValue, value);
    }
}

void Assign::assignElement(
  const memory::Argument & arg, const il::Element & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<ElementValue>(arg, Type::ElementValue, value);
    }
}

void Assign::assignElements(
  const memory::Argument & arg, const il::Elements & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<ElementsValue>(arg, Type::ElementsValue, value);
    }
}

void Assign::assignSession(
  const memory::Argument & arg, const il::Session & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<SessionValue>(arg, Type::SessionValue, value);
    }
}

void Assign::assignTab(const memory::Argument & arg, const il::Tab & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<TabValue>(arg, Type::TabValue, value);
    }
}

void Assign::assignWindow(
  const memory::Argument & arg, const il::Window & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<WindowValue>(arg, Type::WindowValue, value);
    }
}

void Assign::assignDB(const memory::Argument & arg, const il::DB & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<DBValue>(arg, Type::VoidValue, value);
    }
}

void Assign::assignFile(const memory::Argument & arg, const il::File & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<FileValue>(arg, Type::VoidValue, value);
    }
}

void Assign::assignQuery(
  const memory::Argument & arg, const il::Query & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign<QueryValue>(arg, Type::VoidValue, value);
    }
}

void Assign::assignAny(
  const memory::Argument & arg, const memory::Argument & value) {
    switch (value.type) {
    case Type::BoolValue:
        assignBool(arg, value);
        break;

    case Type::DoubleValue:
        assignDouble(arg, value);
        break;

    case Type::IntValue:
        assignInt(arg, value);
        break;

    case Type::StringValue:
        assignString(arg, value);
        break;

    case Type::ListValue:
        assignList(arg, value);
        break;

    case Type::DatetimeValue:
        assignDatetime(arg, value);
        break;

    case Type::RegexValue:
        assignRegex(arg, value);
        break;

    case Type::ObjectValue:
        assignObject(arg, value);
        break;

    case Type::SetValue:
        assignSet(arg, value);
        break;

    case Type::CookieValue:
        assignCookie(arg, value);
        break;

    case Type::ElementValue:
        assignElement(arg, value);
        break;

    case Type::ElementsValue:
        assignElements(arg, value);
        break;

    case Type::SessionValue:
        assignSession(arg, value);
        break;

    case Type::TabValue:
        assignTab(arg, value);
        break;

    case Type::WindowValue:
        assignWindow(arg, value);
        break;

    case Type::DatabaseValue:
        assignDB(arg, value);
        break;

    case Type::FileValue:
        assignFile(arg, value);
        break;

    case Type::QueryValue:
        assignQuery(arg, value);
        break;

    default:
        // Elude unused enums warning
        break;
    }
}

void Assign::runAssign(
  const memory::ArgList & left, const memory::ArgList & right) {
    auto * vm = dynamic_cast<vm::VMLayer *>(il->vm);
    using memory::ContextType;

    if (vm->getType() == ContextType::Run) {
        // case 1:1 and case m:m
        if (left.length() == right.length()) {
            for (int i = 0; i < left.length(); i++) {
                auto & arg   = left[i];
                auto & value = right[i];

                assignAny(arg, value);
            }
        }
        // case m:1
        else if (left.length() >= 1 && right.length() == 1) {
            auto & value = right[0];

            for (const auto & arg : left) {
                assignAny(arg, value);
            }
        }

        if (m_newContext == nullptr) {
            il->vm->signal({memory::Signals::System, "No such operator"});
        }
    }
    else if (vm->getType() == ContextType::Value) {
        if (left.length() == 1 && right.length() == 1) {
            if (left[0].varName.isEmpty()) {
                il->vm->syssig("Parameter name cannot be empty");
            }
            else if (right[0].type == memory::Type::VoidValue) {
                il->vm->syssig("Parameter default value cannot be void");
            }
            else {
                m_newContext =
                  new DefaultParameter(il, left[0].varName, right[0].value);
            }
        }
        else {
            il->vm->syssig("Column syntax is wrong, expected: name : type");
        }
    }
}

icString Assign::toString() {
    return "=";
}

int Assign::currentRunRank(bool rtl) {
    bool runnable =
      (m_prev->role() == Role::Value || m_prev->role() == Role::Identifier) &&
      m_next->role() == Role::Value;
    return rtl && runnable ? 1 : -1;
}

StepType Assign::runNow() {
    auto * asValue = dynamic_cast<Value *>(m_prev);
    auto * asId    = dynamic_cast<Identifier *>(m_prev);

    memory::ArgList right =
      service::Listify::toArgList(il, dynamic_cast<Value *>(m_next));

    if (asValue != nullptr) {
        memory::ArgList left = service::Listify::toArgList(il, asValue);

        runAssign(left, right);
    }
    else if (asId != nullptr) {
        if (right.length() == 1) {
            if (right[0].type != memory::Type::VoidValue) {
                m_newContext = new Field(il, asId->getName(), right[0].value);
            }
            else {
                il->vm->syssig("Column value cannot be void");
            }
        }
        else {
            il->vm->syssig("Column value cannot be packed");
        }
    }

    return StepType::CommandEnd;
}

Role Assign::role() {
    return Role::Assign;
}

const icSet<Role> & Assign::acceptedPrevs() {
    static const icList<icSet<Role>> roles{
      {Role::Value, Role::Function, Role::ValueContext, Role::Property},
      {Role::NoRole, Role::Identifier},
      {Role::Value}};

    return byContext(roles[0], roles[1], roles[2]);
}

const icSet<Role> & Assign::acceptedNexts() {
    static const icList<icSet<Role>> roles{
      {Role::Value, Role::SystemValue, Role::Exists, Role::Function,
       Role::ValueContext, Role::LimitedContext, Role::Operator},
      {Role::Comma, Role::Value, Role::ValueContext},
      {Role::Value}};

    return byContext(roles[0], roles[1], roles[2]);
}

void Assign::run(const memory::ArgList & left, const memory::ArgList & right) {
    runAssign(left, right);
}

}  // namespace icL::ce
