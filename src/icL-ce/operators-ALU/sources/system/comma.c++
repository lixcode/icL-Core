#include "comma.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-ce-base/value/packed-value.h++>

namespace icL::ce {

Comma::Comma(il::InterLevel * il)
    : SystemOperator(il) {}

int Comma::currentRunRank(bool rtl) {
    static const icSet<Role> roles{Role::Value, Role::Column, Role::Field,
                                   Role::Value, Role::Type,   Role::Parameter};

    bool runnable =
      roles.contains(m_prev->role()) && roles.contains(m_next->role());
    return !rtl && runnable ? 0 : -1;
}

CE * Comma::firstToReplace() {
    return m_prev;
}

CE * Comma::lastToReplace() {
    return m_next;
}

icString Comma::toString() {
    return ",";
}

StepType Comma::runNow() {
    m_newContext = new PackedValue{il, m_prev, m_next};
    return StepType::CommandEnd;
}

Role Comma::role() {
    return Role::Comma;
}

const icSet<Role> & Comma::acceptedPrevs() {
    static const icList<icSet<Role>> roles{
      {},
      {Role::Value, Role::Field, Role::Column, Role::Parameter,
       Role::ValueContext, Role::JsValue, Role::Type, Role::LimitedContext,
       Role::Property},
      {Role::Value, Role::Property, Role::Type, Role::Operator}};
    return byContext(roles[0], roles[1], roles[2]);
}

const icSet<Role> & Comma::acceptedNexts() {
    static const icList<icSet<Role>> roles{
      {},
      {Role::Value, Role::Type, Role::Field, Role::Column, Role::Parameter,
       Role::Function, Role::ValueContext, Role::LimitedContext, Role::JsValue,
       Role::Identifier},
      {Role::Value, Role::Operator}};
    return byContext(roles[0], roles[1], roles[2]);
}

void Comma::run(const memory::ArgList &, const memory::ArgList &) {
    assert(false);
}

}  // namespace icL::ce
