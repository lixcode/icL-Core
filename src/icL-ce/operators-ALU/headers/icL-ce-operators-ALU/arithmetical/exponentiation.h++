#ifndef ce_Exponentiation
#define ce_Exponentiation

#include <icL-service-operators-ALU/arithmetical/exponentiation.h++>

#include <icL-ce-base/alu-operator/ambiguous-arithmetical-operator.h++>



class icStringList;
class icRegEx;

namespace icL {

namespace memory {
struct Set;
struct Object;
}  // namespace memory

namespace ce {

class Exponentiation
    : public AmbiguousArithmeticalOperator
    , public service::Exponentiation
{
public:
    Exponentiation(il::InterLevel * il);

    // level 2

    /// `int**`
    void runInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double**`
    void runDouble(const memory::ArgList & left, const memory::ArgList & right);

    /// `int ** int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double ** int`
    void runDoubleInt(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `double ** double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `string ** string`
    void runStringString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `list ** list`
    void runListList(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set ** set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    /// `string ** regex`
    void runStringRegex(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    int  currentRunRank(bool rtl) override;
    CE * firstToReplace() override;
    CE * lastToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;

    // AmbiguousArithmeticalOperator interface
protected:
    int runAbmiguousRank() override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Exponentiation
