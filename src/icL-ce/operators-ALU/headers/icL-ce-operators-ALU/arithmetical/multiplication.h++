#ifndef ce_Multiplication
#define ce_Multiplication

#include <icL-service-operators-ALU/arithmetical/multiplication.h++>

#include <icL-ce-base/alu-operator/arithmetical-operator.h++>



class icStringList;

namespace icL {

namespace memory {
struct Set;
}

namespace ce {

class Multiplication
    : public ArithmeticalOperator
    , public service::Multiplication
{
public:
    Multiplication(il::InterLevel * il);

    // level 2

    /// `int * int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double * double`
    void runDoulbleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `string * string`
    void runStringString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `list * string`
    void runListString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `list * list`
    void runListList(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set * set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Multiplication
