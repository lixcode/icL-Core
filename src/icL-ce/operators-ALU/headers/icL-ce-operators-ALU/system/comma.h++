#ifndef ce_Comma
#define ce_Comma

#include <icL-ce-base/alu-operator/system-operator.h++>



namespace icL::ce {

/**
 * @brief The Comma class represent a comma (`,`) token
 */
class Comma : public SystemOperator
{
public:
    Comma(il::InterLevel * il);

    // CE interface
public:
    int  currentRunRank(bool rtl) override;
    CE * firstToReplace() override;
    CE * lastToReplace() override;

    icString toString() override;
    StepType runNow() override;
    Role     role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // Operator interface
public:
    void run(const memory::ArgList &, const memory::ArgList &) override;
};

}  // namespace icL::ce

#endif  // ce_Comma
