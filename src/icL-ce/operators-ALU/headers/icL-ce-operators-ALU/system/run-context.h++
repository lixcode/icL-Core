#ifndef ce_RunContext
#define ce_RunContext

#include "context.h++"



namespace icL::ce {

/**
 * @brief The RunContext class represent a run context `{}`
 */
class RunContext : public Context
{
public:
    RunContext(
      il::InterLevel * il, const il::CodeFragment & code,
      const icString & name);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    Role     role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // fields
private:
    icString name;
};

}  // namespace icL::ce

#endif  // ce_RunContext
