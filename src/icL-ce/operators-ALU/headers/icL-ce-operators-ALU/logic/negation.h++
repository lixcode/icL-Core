#ifndef ce_Negation
#define ce_Negation

#include <icL-ce-base/alu-operator/logic-operator.h++>



namespace icL::ce {

class Negation : public LogicOperator
{
public:
    Negation(il::InterLevel * il);

    // CE interface
public:
    int  currentRunRank(bool rtl) override;
    CE * firstToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_Negation
