#ifndef ce_Biconditional
#define ce_Biconditional

#include <icL-ce-base/alu-operator/logic-operator.h++>



class icVariant;

namespace icL::ce {

/**
 * @brief The Biconditional class represents the beconditional token `~`
 */
class Biconditional : public LogicOperator
{
public:
    Biconditional(il::InterLevel * il);

    /**
     * @brief run applicates the logic operation biconditional
     * @param left is the left operand
     * @param right is the right operand
     * @return true if both are false or true, otherwise false
     */
    bool run(bool left, bool right);

    // CE interface
public:
    int currentRunRank(bool rtl) override;

    CE * firstToReplace() override;
    CE * lastToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // CE interface
public:
    icString toString() override;

    // Operator interface

    /**
     * @brief run applicates the secondary select operation
     */
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // fields
private:
    bool asVoidValue = false;

    // padding
    long : 56;
};

}  // namespace icL::ce

#endif  // ce_Biconditional
