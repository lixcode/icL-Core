#include <icL-vm/mock.h++>

#include <QThread>

namespace icL::vm {

void Mock::signal(const il::Signal & signal) {
    m_signal = signal;
}

void Mock::syssig(const icString & message) {
    signal({memory::Signals::System, message});
}

void Mock::cp_sig(const icString & message) {
    syssig(message);
}

void Mock::cpe_sig(const icString & message) {
    syssig(message);
}

void Mock::sendAssert(const icString &) {
    // Do nothing
}

void Mock::sleep(int ms) {
    QThread::msleep(uint64_t(ms));
}

void Mock::addDescription(const icString &) {
    // Do nothing
}

void Mock::markStep(const icString &) {
    // Do nothing
}

void Mock::markTest(const icString &) {
    // Do nothing
}

void Mock::break_() {
    // Do nothing
}

void Mock::continue_() {
    // Do nothing
}

void Mock::return_(const icVariant &) {
    // Do nothing
}

bool Mock::hasOkState() {
    return m_signal.code == memory::Signals::NoError;
}

void Mock::finalize() {
    // Do nothing
}

il::VMLayer * Mock::parent() {
    return nullptr;
}

}  // namespace icL::vm
