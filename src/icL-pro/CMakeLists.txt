project(icL-Pro LANGUAGES CXX)

file(GLOB HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/headers/*.h++")
file(GLOB SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/sources/*.c++")

add_executable(${PROJECT_NAME}
  ${HEADERS}
  ${SOURCES}
)

target_link_libraries(${PROJECT_NAME}
  PRIVATE
  -icL-shared
  stdc++
  m
)
