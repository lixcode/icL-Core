#include "packed-value-item.h++"

namespace icL::memory {

PackedValue::PackedValue(const icVariant & var) {
    assert(var.isPacked());
    *this = var.toPacked();
}

icL::memory::PackedValue::operator icVariant() {
    return icVariant::fromValue(*this);
}

bool PackedValue::operator==(const PackedValue & other) const {
    return data == other.data;
}

}  // namespace icL::memory
