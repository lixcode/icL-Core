#include "set.h++"

namespace icL::memory {

Column::Column(Type type, const icString & name)
    : name(name)
    , type(type) {}

bool Column::operator==(const Column & other) const {
    return name == other.name;
}

Set::Set(const icVariant & var) {
    assert(var.isSet());
    *this = var.toSet();
}

bool Set::operator==(const Set & other) const {
    return setData == other.setData;
}

icL::memory::Set::operator icVariant() const {
    return icVariant::fromValue(*this);
}

Object::Object(const icVariant & var) {
    assert(var.isObject());
    *this = var.toObject();
}

Object::Object(const icVariantObject & hash) {
    data = std::make_shared<DataContainer>();

    for (auto it = hash.begin(); it != hash.end(); it++) {
        data->setValue(it.key(), it.value());
    }
}

bool Object::operator==(const Object & other) const {
    return data == other.data;
}

icL::memory::Object::operator icVariant() const {
    return icVariant::fromValue(*this);
}

}  // namespace icL::memory
