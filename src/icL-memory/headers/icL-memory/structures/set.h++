#ifndef memory_Set
#define memory_Set

#include "../state/datacontainer.h++"
#include "functioncontainer.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>



using icVariantList   = icList<icVariant>;
using icVariantObject = icObject<icString, icVariant>;

namespace icL::memory {

/**
 * @brief The Column struct describes a column of a set
 */
struct Column
{
    /// \brief name is the name of column
    icString name;

    /// \brief type is the type of column
    Type type;

    /**
     * @brief Column creates a column description
     * @param type is the type of column
     * @param name is the name of column
     */
    Column(Type type, const icString & name);

    /**
     * @brief operator == compares two column
     * @param other is the column to compare with
     * @return true if this and other column has the same name
     */
    bool operator==(const Column & other) const;
};

using Columns = icList<Column>;
using SetData = icSet<icVariantList>;

/**
 * @brief The Set struct is an icL set
 */
struct Set
{
    /// \brief header is the header of the set
    std::shared_ptr<Columns> header;

    /// \brief setData is the data of the set
    std::shared_ptr<SetData> setData;

    Set() = default;

    /**
     * @brief Set casts a icVariant to a set value
     * @param var is the variant to cast
     */
    Set(const icVariant & var);

    /// \brief operator icVariant cast a set to icVariant
    operator icVariant() const;

    /**
     * @brief operator == checks if 2 sets are equivalent
     * @param other is the set to compare with
     * @return true if they are equivaent, otherwise false
     */
    bool operator==(const Set & other) const;
};

/**
 * @brief The Object struct is an icL object
 */
struct Object
{
    Object() = default;
    Object(const icVariant & var);
    Object(const icVariantObject & hash);

    /// \brief data is the data of object, an oject is a data container
    std::shared_ptr<DataContainer> data;

    /// \brief operator icVariant casts a object to icVariant
    operator icVariant() const;

    /**
     * @brief operator == checks if 2 objects are equivalent
     * @param other is the object to compare with
     * @return true if they are equivaent, otherwise false
     */
    bool operator==(const Object & other) const;
};

}  // namespace icL::memory

#endif  // memory_Set
