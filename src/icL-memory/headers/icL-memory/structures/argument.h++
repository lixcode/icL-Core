#ifndef memory_Argument
#define memory_Argument

#include "type.h++"

#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <memory>



class icDateTime;
class icRegEx;
class icStringList;
template <typename>
class icList;

namespace icL {

namespace il {
struct Element;
struct File;
struct Cookie;
struct Tab;
struct Session;
struct Window;
struct DB;
struct Query;
struct LambdaTarget;
}  // namespace il

namespace memory {
class DataContainer;
}

namespace memory {

struct Set;
struct Object;

/**
 * @brief The Argument struct represents a argument of function call
 */
struct Argument
{
    /// \brief type is the type of argument value
    Type type = Type::VoidValue;

    /// \brief value is the value itself, will be invalid if argument is a type,
    /// not a value
    icVariant value;

    /// \brief varName is the name of value in container
    icString varName;

    /// \brief container is the container of value
    memory::DataContainer * container = nullptr;

    /// \brief operator bool casts the argument value to bool
    operator bool() const;

    /// \brief operator int casts the argument value to int
    operator int() const;

    /// \brief operator double casts the argument value to double
    operator double() const;

    /// \brief operator icString casts the argument value to icString
    operator icString() const;

    /// \brief operator icDateTime casts the argument value to icDateTime
    operator icDateTime() const;

    /// \brief operator Element casts the argument value to Element
    operator il::Element() const;

    /// \brief operator Element casts the argument value to Element
    operator il::Elements() const;

    /// \brief operator icStringList casts the argument value to icStringList
    operator icStringList() const;

    /// \brief operator Object casts the argument value to Object
    operator Object() const;

    /// \brief operator Set casts the argument value to Set
    operator Set() const;

    /// \brief operator icRegEx casts the argument value to
    /// icRegEx
    operator icRegEx() const;

    /// \brief operator il::File casts the argument value to file reference
    operator il::File() const;

    /// \brief operator il::CookieRef casts the argument to a cookie reference
    operator il::Cookie() const;

    /// \brief operator il::SessionPtr casts the argument to a session pointer
    operator il::Session() const;

    /// \brief operator il::TabPtr casts the arguemnt to a tab pointer
    operator il::Tab() const;

    /// \brief operator il::WindowPtr casts the argument to a window pointer
    operator il::Window() const;

    /// \brief operator il::DB casts the argument to a data base target
    operator il::DB() const;

    /// \brief operator il::Query casts the arguement to a query target
    operator il::Query() const;

    /// \brief operator il::LambdaTarget casts the argument to a lambda target
    operator il::LambdaTarget() const;

    /// \brief operator Type casts the argument value to Type
    /// \note It must be a type value
    operator Type() const;
};

/**
 * ArgList is a list of arguments used for calls of functions and methods
 */
using ArgList = icList<Argument>;


}  // namespace memory
}  // namespace icL

#endif  // memory_Argument
