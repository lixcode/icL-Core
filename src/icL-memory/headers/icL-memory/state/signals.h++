#ifndef memory_Signals
#define memory_Signals

namespace icL::memory {

/**
 * The Signals namespace block the distribution of Signals items to memory
 */
namespace Signals {

/**
 * @brief The Signals enum contains all predefined icL signals
 */
enum Signals {
    System  = -2,             ///< Syntax or semantic error.
    Exit    = -1,             ///< Error.
    NoError = 0,              ///< No error
    NoSessions,               ///< There is not an opened session.
    NoSuchWindow,             ///< No such window.
    NoSuchElement,            ///< No such element.
    NoSuchFrame,              ///< Nu such frame.
    NoSuchCookie,             ///< No such cookie.
    NoSuchAlert,              ///< No such alert.
    NoSuchPlaceholder,        ///< No such placeholder.
    NoSuchDatabase,           ///< No such database.
    NoSuchServer,             ///< No such server.
    WrongUserPassword,        ///< Authentication failed.
    StaleElementReference,    ///< Stale element reference.
    FolderNotFound,           ///< Folder not found.
    FileNotFound,             ///< File not found.
    FieldNotFound,            ///< Field not found.
    FieldAlreadyExists,       ///< Field already exists.
    OutOfBounds,              ///< Out of collection bound.
    UnsupportedOperation,     ///< Web driver does not support this operation.
    EmptyString,              ///< The string is empty.
    EmptyList,                ///< The list is empty.
    MultiList,                ///< The list contains some strings.
    EmptyElement,             ///< Invalid web element.
    MultiElement,             ///< Collection contains some web elements.
    EmptySet,                 ///< The set is empty.
    MultiSet,                 ///< The set contains some items.
    InvalidArgument,          ///< Invalid argument.
    InvalidSelector,          ///< Invalid selector
    InvalidElementState,      ///< Invalid element state.
    InvalidElement,           ///< Operation is not available for the element.
    IncompatibleRoot,         ///< Wrong root of JSON.
    IncompatibleData,         ///< Wrong input data.
    IncompatibleObject,       ///< Wrong input object.
    InvalidSessionId,         ///< Session was closed.
    InvalidCookieDomain,      ///< Wrong domain of cookie.
    InsecureCertificate,      ///< Certificate is insecure.
    UnexpectedAlertOpen,      ///< Unexpected alert was opened.
    UnrealCast,               ///< The requested cast is impossible.
    ParsingFailed,            ///< Parsing failed.
    WrongDelimiter,           ///< Wrong delimiter.
    ComplexField,             ///< The field is complex.
    ElementNotInteractable,   ///< Element is not intractable.
    ElementClickIntercepted,  ///< The click was intercepted.
    MoveTargetOutOfBounds,    ///< Coordinates for mouse are out of screen.
    UnableToSetCookie,        ///< Unable to set cookie.
    UnableToCaptureScreen,    ///< Unable to capture screen.
    JavaScriptError,          ///< Excution of JavaScript code failed.
    ScriptTimeout,            ///< Script execution timeout.
    Timeout,                  ///< Timeout.
    SessionNotCreated,        ///< Session creation failed.
    QueryNotExecutedYet,      ///< The SQL query was not executed yet.
    UnknownCommand,           ///< [w3c] Unknown command.
    UnknownError,             ///< [w3c] Unknown error.
    UnknownMethod,            ///< [w3c] Unknown method.
    User
};
}  // namespace Signals

}  // namespace icL::memory

#endif  // memory_Signals
