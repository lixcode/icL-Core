#ifndef memory_Memory
#define memory_Memory

#include "../structures/functioncontainer.h++"
#include "stackcontainer.h++"
#include "statecontainer.h++"



namespace icL::memory {

class Memory
{
public:
    /**
     * @brief Memory initialize memory with default signals
     */
    Memory() = default;

    /**
     * @brief stateIt gets the state iterator
     * @return the state iterator
     */
    StateIt & stateIt();

    /**
     * @brief stackIt gets the stack iterator
     * @return the stack iterator
     */
    StackIt & stackIt();

    /**
     * @brief functions gets the function continer
     * @return the function container
     */
    FunctionContainer & functions();

private:
    /// \brief m_stateIt the state iterator
    StateIt m_stateIt;

    /// \brief m_stackIt the stack iterator
    StackIt m_stackIt;

    /// \brief m_functions the function container
    FunctionContainer m_functions;
};


}  // namespace icL::memory

#endif  // memory_Memory
