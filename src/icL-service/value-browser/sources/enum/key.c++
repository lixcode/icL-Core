#include "key.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using icL::il::Keys::Keys;

Key::Key() = default;

int Key::alt() {
    return Keys::Alt;
}

int Key::ctrl() {
    return Keys::Ctrl;
}

int Key::shift() {
    return Keys::Shift;
}

}  // namespace icL::service
