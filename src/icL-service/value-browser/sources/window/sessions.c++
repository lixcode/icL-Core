#include "sessions.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-memory/state/signals.h++>



namespace icL::service {

Sessions::Sessions() = default;

il::Session Sessions::current() {
    il::Session session;

    session.data =
      std::make_shared<il::TargetData>(_il()->server->getCurrentTarget());
    return session;
}

int Sessions::length() {
    return _il()->server->getSessions().length();
}

void Sessions::closeAll() {
    _il()->server->closeSession();
}

il::Session Sessions::get(int i) {
    il::Session            session;
    icList<il::TargetData> sessions = _il()->server->getSessions();

    if (i < 0 || i >= sessions.length()) {
        _il()->vm->signal({memory::Signals::OutOfBounds, {}});
    }
    else {
        session.data = std::make_shared<il::TargetData>(sessions[i]);
    }

    return session;
}

il::Session Sessions::new_() {
    il::Session session;

    _il()->server->newSession();
    session.data =
      std::make_shared<il::TargetData>(_il()->server->getCurrentTarget());

    return session;
}

}  // namespace icL::service
