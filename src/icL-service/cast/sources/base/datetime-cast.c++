#include "datetime-cast.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-string.h++>

namespace icL::service {

icString DatetimeCast::toString(const icDateTime & value) {
    return value.toString();
}

icString DatetimeCast::toString(
  const icDateTime & value, const icString & format) {
    return format == "SSXE" ? icString::number(value.toSecsSinceEpoch())
                            : value.toString(format);
}

icDateTime DatetimeCast::toDatetime(const icString & value) {
    return icDateTime::fromString(value);
}

icDateTime DatetimeCast::toDatetime(
  const icString & value, const icString & format) {
    return format == "SSXE" ? icDateTime::fromSecsSinceEpoch(value.toInt())
                            : icDateTime::fromString(value, format);
}

}  // namespace icL::service
