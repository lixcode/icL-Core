#include "set-cast.h++"

#include "object-cast.h++"
#include "void-cast.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/values/set.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/set.h++>

#include <functional>



namespace icL::service {

bool SetCast::toBool(const memory::Set & value) {
    return !value.setData->empty();
}

icString SetCast::toString(const memory::Set & value) {
    return icChar('[') % toList(value).join(',') % ']';
}

memory::Object SetCast::toObject(
  il::InterLevel * il, const memory::Set & value) {
    memory::Object ret = service::VoidCast::toObject();

    if (value.setData->isEmpty()) {
        il->vm->signal({memory::Signals::EmptySet, ""});
    }
    else if (value.setData->size() > 1) {
        il->vm->signal({memory::Signals::MultiSet, ""});
    }
    else {
        service::Set::forEach(
          value, [&ret](const memory::Object & obj) { ret = obj; });
    }

    return ret;
}

icStringList SetCast::toList(const memory::Set & value) {
    icStringList ret;

    service::Set::forEach(value, [&ret](const memory::Object & obj) {
        ret.append(ObjectCast::toString(obj));
    });
    return ret;
}

}  // namespace icL::service
