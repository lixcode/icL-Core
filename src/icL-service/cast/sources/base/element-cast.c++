#include "element-cast.h++"

#include <icL-il/structures/element.h++>

namespace icL::service {

bool ElementCast::toBool(const il::Element & value) {
    return !value.variable.isEmpty();
}

icString ElementCast::toString(const il::Element & value) {
    return value.selector;
}

}  // namespace icL::service
