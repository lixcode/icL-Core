#include "list-cast.h++"

#include "string-cast.h++"
#include "void-cast.h++"

#include <icL-types/json/js-document.h++>
#include <icL-types/json/js-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/values/set.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/set.h++>

namespace icL::service {

using memory::Signals::Signals;

bool ListCast::toBool(const icStringList & value) {
    return !value.isEmpty();
}

icString ListCast::toString(il::InterLevel * il, const icStringList & value) {
    icString ret;

    if (value.isEmpty()) {
        il->vm->signal({Signals::EmptyList, {}});
    }
    else if (value.length() > 1) {
        il->vm->signal({Signals::MultiList, {}});
    }
    else {
        ret = value.first();
    }

    return ret;
}

memory::Set ListCast::toSet(il::InterLevel * il, const icStringList & value) {
    memory::Set ret = service::VoidCast::toSet();

    for (const auto & str : value) {
        jsError error;
        auto    jdoc = jsDocument::fromJson(str, error);

        if (error.error != jsError::NoError) {
            il->vm->signal({Signals::ParsingFailed, error.errorString});
        }
        else {
            if (jdoc.isObject()) {
                memory::Object toAdd = StringCast::toObject(il, jdoc.object());

                if (ret.header->isEmpty()) {
                    ret = service::Set::fromObject(toAdd);
                }
                else {
                    service::Set::insert(il, ret, toAdd);
                }
            }
            else {
                il->vm->signal({Signals::IncompatibleRoot, ""});
            }
        }
    }

    return ret;
}

}  // namespace icL::service
