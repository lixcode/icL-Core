#ifndef service_ListCast
#define service_ListCast



class icString;
class icStringList;

namespace icL {

namespace memory {
struct Set;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class ListCast
{
public:
    /// `list : bool`
    static bool toBool(const icStringList & value);

    /// `list : string`
    static icString toString(il::InterLevel * il, const icStringList & value);

    /// `list : set`
    static memory::Set toSet(il::InterLevel * il, const icStringList & value);
};

}  // namespace service
}  // namespace icL

#endif  // service_ListCast
