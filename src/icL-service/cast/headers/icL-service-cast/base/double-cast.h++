#ifndef service_DoubleCast
#define service_DoubleCast



class icString;

namespace icL::service {

class DoubleCast
{
public:
    /// `double : bool`
    static bool toBool(double value);

    /// `double : int`
    static int toInt(double value);

    /// `double : string`
    static icString toString(double value);
};

}  // namespace icL::service

#endif  // service_DoubleCast
