#ifndef service_SetCast
#define service_SetCast



class icString;
class icStringList;

namespace icL {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class SetCast
{
public:
    /// `set : string`
    static icString toString(const memory::Set & value);

    /// `set : object`
    static memory::Object toObject(
      il::InterLevel * il, const memory::Set & value);

    /// `set : list`
    static icStringList toList(const memory::Set & value);

    /// `set : bool`
    static bool toBool(const memory::Set & value);
};

}  // namespace service
}  // namespace icL

#endif  // service_SetCast
