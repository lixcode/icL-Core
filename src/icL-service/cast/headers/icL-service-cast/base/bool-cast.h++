#ifndef service_BoolCast
#define service_BoolCast



class icString;

namespace icL::service {

class BoolCast
{
public:
    /// `bool : int`
    static int toInt(bool value);

    /// `bool : double`
    static double toDouble(bool value);

    /// `bool : string`
    static icString toString(bool value);
};

}  // namespace icL::service

#endif  // service_BoolCast
