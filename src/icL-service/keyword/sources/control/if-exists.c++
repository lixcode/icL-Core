#include "if-exists.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/ce.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::service {

IfExists::IfExists(bool notMod) {
    notModifier = notMod;
}

StepType IfExists::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        _il()->vms->pushKeepAliveLayer(il::LayerType::Control);
        it      = ifs.begin();
        current = State::ExistsChecking;
        break;

    case State::ExistsChecking:
        if (it != ifs.end()) {
            if (it->exists->transact() == StepType::CommandEnd) {
                value = dynamic_cast<il::CE *>(it->exists)->getValue();
                if (
                  (!notModifier && value.isValid()) ||
                  (notModifier && value.isVoid())) {
                    current = State::CodeRunning;
                }
                else {
                    it++;
                }
            }
        }
        else {
            current =
              elseCode.source == nullptr ? State::End : State::LastResort;
        }
        break;

    case State::CodeRunning:
        release(it->body);
        ret     = StepType::CommandIn;
        current = State::End;
        break;

    case State::LastResort:
        release(elseCode);
        ret     = StepType::CommandIn;
        current = State::End;
        break;

    case State::End:
        finalize();
        _il()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

void IfExists::release(const il::CodeFragment & code) {
    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.createLayer = false;
    fcall.contextName = Stringify::alternative(fcall.code.name, "if");

    if (value.isValid()) {
        fcall.args.append({"@", value});
    }

    _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code != memory::Signals::NoError) {
            _il()->vm->signal(ret.signal);
        }
        return false;
    });
}

}  // namespace icL::service
