#include "exists.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/cookie-data.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/file-target.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>
#include <icL-memory/structures/type.h++>

namespace icL::service {

void Exists::checkByDefaultCondition() {
    using memory::Type;

    switch (memory::variantToType(value)) {
    case Type::BoolValue:
        if (!value.toBool()) {
            value.clear();
        }
        break;

    case Type::IntValue:
        if (value.toInt() == 0) {
            value.clear();
        }
        break;

    case Type::DoubleValue:
        if (value.toDouble() == 0.0) {
            value.clear();
        }
        break;

    case Type::StringValue:
        if (value.toString().isEmpty()) {
            value.clear();
        }
        break;

    case Type::ListValue:
        if (value.toStringList().isEmpty()) {
            value.clear();
        }
        break;

    case Type::ObjectValue:
        if (memory::Object(value).data->countVariables() == 0) {
            value.clear();
        }
        break;

    case Type::SetValue:
        if (memory::Set(value).setData->empty()) {
            value.clear();
        }
        break;

    case Type::ElementValue:
        break;

    case Type::ElementsValue:
        if (il::Elements(value).size() == 0) {
            value.clear();
        }
        break;

    case Type::CookieValue:
        if (il::Cookie(value).data->name.isEmpty()) {
            value.clear();
        }
        break;

    case Type::DatetimeValue:
        if (!value.toDateTime().isValid()) {
            value.clear();
        }
        break;

    case Type::DatabaseValue:
        if (il::DB(value).target->databaseId.isEmpty()) {
            value.clear();
        }
        break;

    case Type::QueryValue:
        if (il::Query(value).target->queryId.isEmpty()) {
            value.clear();
        }
        break;

    case Type::FileValue:
        if (il::File(value).target->fileId.isEmpty()) {
            value.clear();
        }
        break;

    case Type::SessionValue:
        if (il::Session(value).data->sessionId.isEmpty()) {
            value.clear();
        }
        break;

    case Type::TabValue:
        if (il::Tab(value).data->windowId.isEmpty()) {
            value.clear();
        }
        break;

    case Type::WindowValue:
        if (il::Window(value).data->windowId.isEmpty()) {
            value.clear();
        }
        break;

    default:
        value.clear();
    }
}

StepType Exists::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial: {
        initialize();
        _il()->vms->pushKeepAliveLayer(il::LayerType::Control);
        break;
    }
    case State::Inited: {
        memory::FunctionCall fcall;

        fcall.code        = valueCode;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "exists");

        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code == memory::Signals::NoError) {
                value = ret.consoleValue;
            }
            return false;
        });

        current = State::ValueCalculed;
        ret     = StepType::CommandIn;
        break;
    }

    case State::ValueCalculed: {
        if (conditionCode.source == nullptr) {
            checkByDefaultCondition();
        }
        else {
            memory::FunctionCall fcall;

            fcall.code        = conditionCode;
            fcall.createLayer = false;
            fcall.contextName =
              Stringify::alternative(fcall.code.name, "exists");

            _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
                if (ret.signal.code == memory::Signals::NoError) {
                    if (ret.consoleValue.type() == icType::Bool) {
                        _il()->vm->signal(
                          {memory::Signals::System,
                           "Condition expression must return a bool value"});
                    }
                    else if (!ret.consoleValue.toBool()) {
                        value.clear();
                    }
                }
                else {
                    _il()->vm->signal(ret.signal);
                }
                return false;
            });
        }

        current = State::ConditionChecked;
        ret     = StepType::CommandIn;
        break;
    }

    case State::ConditionChecked: {
        finalize();
        _il()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }
    }

    return ret;
}

}  // namespace icL::service
