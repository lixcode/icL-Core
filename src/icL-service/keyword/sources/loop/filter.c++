#include "filter.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-keyword/iterator/iterator-factory.h++>
#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::service {

using memory::Signals::Signals;

void Filter::break_() {
    current = State::End;
}

void Filter::continue_() {
    reverse ? it->toPrev() : it->toNext();
    current = State::CondtitionChecking;
}

StepType Filter::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        _il()->vms->pushKeepAliveLayer(il::LayerType::Loop);
        current = State::ValueExtracting;
        break;

    case State::ValueExtracting: {
        memory::FunctionCall fcall;

        fcall.code        = collectionCode;
        fcall.createLayer = false;

        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code == Signals::NoError) {
                it = IteratorFactory::fromValue(_il(), ret.consoleValue);

                if (it == nullptr) {
                    _il()->vm->signal(
                      {Signals::System, "filter accepts only collections."});
                }
                else {
                    current = State::CondtitionChecking;

                    if (reverse) {
                        it->toLast();
                    }
                }
            }
            else {
                _il()->vm->signal(ret.signal);
            }
            return false;
        });

        ret = StepType::CommandIn;
        break;
    }
    case State::CondtitionChecking:
        // it->atEnd() excludes the error then a collection is empty
        if (it->atEnd() || (max >= 1 && executed >= max)) {
            current = State::End;
        }
        else {
            memory::FunctionCall fcall;

            currentValue = it->getCurrent();

            fcall.code        = conditionCode;
            fcall.createLayer = false;
            fcall.contextName =
              Stringify::alternative(fcall.code.name, "filter");
            fcall.args.append({"@", currentValue});
            fcall.args.append({"#", currentIndex});

            _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
                if (ret.signal.code != Signals::NoError) {
                    _il()->vm->signal(ret.signal);
                }
                else if (ret.consoleValue.type() != icType::Bool) {
                    _il()->vm->signal(
                      {Signals::System,
                       "filter condition must return a bool value"});
                }
                else {
                    if (ret.consoleValue.toBool()) {
                        current = State::CodeExecution;
                    }
                    else {
                        reverse ? it->toPrev() : it->toNext();
                    }
                }
                return false;
            });

            ret = StepType::CommandIn;
        }

        currentIndex++;
        break;

    case State::CodeExecution: {
        memory::FunctionCall fcall;

        fcall.code        = loopBody;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "filter");
        fcall.args.append({"@", currentValue});

        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                _il()->vm->signal(ret.signal);
            }
            else {
                current = reverse && it->atBegin() ? State::End
                                                   : State::CondtitionChecking;
                reverse ? it->toPrev() : it->toNext();
            }
            return false;
        });

        executed++;
        ret = StepType::CommandIn;
        break;
    }

    case State::End:
        finalize();
        _il()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

}  // namespace icL::service
