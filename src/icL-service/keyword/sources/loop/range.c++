#include "range.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-keyword/iterator/iterator-factory.h++>
#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::service {

using memory::Signals::Signals;

void Range::break_() {
    current = State::End;
}

void Range::continue_() {
    if (reverse) {
        if (it->atStop()) {
            current = State::End;
        }
        else {
            it->toPrev();
        }
    }
    else {
        if (it->atEnd()) {
            current = State::End;
        }
        else {
            it->toNext();
        }
    }
}

StepType Range::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        _il()->vms->pushKeepAliveLayer(il::LayerType::Loop);
        initialize();
        current = State::ValueExtracting;
        break;

    case State::ValueExtracting: {
        memory::FunctionCall fcall;

        fcall.code        = collectionCode;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "range");

        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                _il()->vm->signal(ret.signal);
            }
            else {
                it = IteratorFactory::fromValue(_il(), ret.consoleValue);

                if (it == nullptr) {
                    _il()->vm->signal(
                      {Signals::System, "range accepts only collections"});
                }
                else {
                    current = it->atEnd() ? State::End : State::StartFinding;
                    currentIndex = 0;
                }
            }
            return false;
        });
        ret = StepType::CommandIn;
        break;
    }
    case State::StartFinding:
        checkCondtion(startCode, [this](const il::Return & ret) {
            if (ret.consoleValue) {
                it->setStopToCurrent();
                it->toLast();
                currentIndex = it->getSize() - 1;
                current      = State::FinishFinding;
            }
            else if (it->atEnd()) {
                current = State::End;
            }
            else {
                it->toNext();
                currentIndex++;
            }
        });

        ret = StepType::CommandIn;
        break;

    case State::FinishFinding:
        checkCondtion(finishCode, [this](const il::Return & ret) {
            if (ret.consoleValue) {
                it->setEndToCurrent();

                if (!reverse) {
                    while (!it->atStop()) {
                        it->toPrev();
                    }
                }
                current = State::CodeExecution;
            }
            else if (it->atStop()) {
                current = State::End;
            }
            else {
                it->toPrev();
                currentIndex--;
            }
        });

        ret = StepType::CommandIn;
        break;

    case State::CodeExecution: {
        memory::FunctionCall fcall;

        fcall.code        = loopBody;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "range");
        fcall.args.append({"@", it->getCurrent()});

        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                _il()->vm->signal(ret.signal);
            }
            else if (max > 0 && executed >= max) {
                current = State::End;
            }
            else {
                if (reverse) {
                    if (it->atStop()) {
                        current = State::End;
                    }
                    else {
                        it->toPrev();
                    }
                }
                else {
                    if (it->atEnd()) {
                        current = State::End;
                    }
                    else {
                        it->toNext();
                    }
                }
            }
            return false;
        });
        executed++;
        ret = StepType::CommandIn;
        break;
    }
    case State::End:
        _il()->vms->popKeepAliveLayer();
        finalize();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

void Range::checkCondtion(
  const il::CodeFragment &                        code,
  const std::function<void(const il::Return &)> & onsucces) {
    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.createLayer = false;
    fcall.contextName = Stringify::alternative(fcall.code.name, "range");
    fcall.args.append({"@", it->getCurrent()});
    fcall.args.append({"#", currentIndex});

    _il()->vms->interrupt(fcall, [this, onsucces](const il::Return & ret) {
        if (ret.signal.code != Signals::NoError) {
            _il()->vm->signal(ret.signal);
        }
        else {
            if (ret.consoleValue.isBool()) {
                onsucces(ret);
            }
            else {
                _il()->vm->signal({Signals::NoError,
                                   "range condition must return a bool value"});
            }
        }
        return false;
    });
}

}  // namespace icL::service
