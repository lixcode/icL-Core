#include "iterator-factory.h++"

#include "element-iterator.h++"
#include "list-iterator.h++"
#include "set-iterator.h++"

#include <icL-memory/structures/type.h++>



namespace icL::service {

using memory::Type;

Iterator * IteratorFactory::fromValue(
  il::InterLevel * il, const icVariant & value) {
    Type       type = memory::variantToType(value);
    Iterator * ret  = nullptr;

    switch (type) {
    case Type::ElementsValue:
        ret = new ElementIterator(value, il);
        break;

    case Type::ListValue:
        ret = new ListIterator(value.toStringList());
        break;

    case Type::SetValue:
        ret = new SetIterator(value);
        break;

    default:
        ret = nullptr;
    }

    return ret;
}

}  // namespace icL::service
