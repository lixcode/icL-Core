#ifndef service_IteratorFactory
#define service_IteratorFactory


class icVariant;

namespace icL {

namespace il {
struct InterLevel;
}

namespace service {

class Iterator;

/**
 * @brief The IteratorFactory class is a abstract factory for iterators
 */
class IteratorFactory
{
public:
    /**
     * @brief fromValue creates a iterator from a icVariant
     * @param value is the icVariant to create a iterator for
     * @return a pointer to created abstract iterator
     */
    static icL::service::Iterator * fromValue(
      il::InterLevel * il, const icVariant & value);
};

}  // namespace service
}  // namespace icL

#endif  // service_IteratorFactory
