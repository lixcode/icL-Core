#ifndef service_Wait
#define service_Wait

#include "../main/finite-state-machine.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/ajax-handler.h++>
#include <icL-il/structures/code-fragment.h++>

#include <icL-service-main/values/inode.h++>

#include <icL-memory/structures/set.h++>



namespace icL::service {

class Wait
    : public FiniteStateMachine
    , public il::AjaxHandler
    , public virtual INode
{
public:
    // FiniteStateMachine interface
public:
    StepType transact() override;

    // AjaxHandler interface
public:
    void handle(const memory::Object & response, int count) override;

protected:
    enum class State {  ///< Finite state machine for `wait`
        Initial,        ///< Initial state of FMS
        Checking,       ///< Checking for condition to get true
        Sleeping,       ///< Sleep some time
        End             ///< The FMS ends here
    } current = State::Initial;

    /// @brief `wait:Xms`
    int ms = -1;

    /// @brief `wait:Xs`
    int s = -1;

    /// @brief `wait:ajax`
    bool ajax = false;

    /// @brief `wait-try`
    bool try_ = false;

    /**
     * @brief The Request struct represent a request to run code in ajax mode
     */
    struct Request
    {
        memory::Object response;  ///< response of the server
        int            count;     ///< is the count of active requests
    };

    /// @brief list ajax requests
    icList<Request> requests;

    /// @brief `wait {<here>}`
    il::CodeFragment condition;

    uint64_t begin;  ///< time when the wait began
    uint64_t end;    ///< time when will be triggered a exception
};

}  // namespace icL::service

#endif  // service_Wait
