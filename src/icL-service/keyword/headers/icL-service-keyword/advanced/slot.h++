#ifndef service_Slot
#define service_Slot



namespace icL::service {

class Slot
{
public:
    virtual ~Slot() = default;

    /**
     * @brief check ckecks if this error must be handled by this slot
     * @param code is the error code
     * @return true if this slot must handle this error, otherwise false
     */
    virtual bool check(int code) = 0;
};

}  // namespace icL::service

#endif  // service_Slot
