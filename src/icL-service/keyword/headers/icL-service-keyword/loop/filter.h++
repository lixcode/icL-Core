#ifndef service_Filter
#define service_Filter

#include "../main/finite-state-machine.h++"
#include "loop.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/structures/code-fragment.h++>

#include <icL-service-keyword/iterator/iterator.h++>
#include <icL-service-main/values/inode.h++>



namespace icL::service {

class Filter
    : public FiniteStateMachine
    , public Loop
    , public INode
{
    // Loop interface
public:
    void break_() override;
    void continue_() override;

public:
    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    enum class State {       ///< Finite state machine for `filter`
        Initial,             ///< Initial state of FSM
        ValueExtracting,     ///< Getting collection to iterate
        CondtitionChecking,  ///< Checkint the fileter matching
        CodeExecution,       ///< Execution of loop body
        End                  ///< The FMS ends here
    } current = State::Initial;

    int currentIndex = 0;  ///< Is the index of current item

    il::CodeFragment collectionCode;  ///< `filter (<here>; ..) {..}`
    il::CodeFragment conditionCode;   ///< `filter (..; <here>) {..}`
    il::CodeFragment loopBody;        ///< `filter (..; ..) {<here>}`

    int  executed = 0;      ///< currently number of executions
    int  max      = -1;     ///< `:maxX`
    bool reverse  = false;  ///< `:reverse`

    long : 24 + 32;  // padding

    Iterator * it = nullptr;  ///< iterator for collection

    /// currentValue caches the value beetween condition checking and code
    /// execution
    icVariant currentValue;
};

}  // namespace icL::service

#endif  // service_Filter
