#ifndef service_Range
#define service_Range

#include "../main/finite-state-machine.h++"
#include "loop.h++"

#include <icL-il/structures/code-fragment.h++>

#include <icL-service-keyword/iterator/iterator.h++>
#include <icL-service-main/values/inode.h++>



namespace std {
template <typename>
class function;
}

namespace icL {

namespace il {
class Return;
}

namespace service {

class Range
    : public FiniteStateMachine
    , public Loop
    , public INode
{
    // Loop interface
public:
    void break_() override;
    void continue_() override;

    // FiniteStateMachine interface
public:
    StepType transact() override;

private:
    void checkCondtion(
      const il::CodeFragment &                        code,
      const std::function<void(const il::Return &)> & onsucces);

protected:
    enum class State {    ///< Finite state machine for `range`
        Initial,          ///< Initial state of FSM
        ValueExtracting,  ///< Getting collection to iterate
        StartFinding,     ///< Checkint the fileter matching
        FinishFinding,    ///< Checkint the fileter matching
        CodeExecution,    ///< Execution of loop body
        End               ///< The FMS ends here
    } current = State::Initial;

    int currentIndex = 0;  ///< Is the index of current item

    il::CodeFragment collectionCode;  ///< `range (<here>; ..; ..) {..}`
    il::CodeFragment startCode;       ///< `range (..; <here>; ..) {..}`
    il::CodeFragment finishCode;      ///< `range (..; ..; <here>) {..}`
    il::CodeFragment loopBody;        ///< `range (..; ..; ..) {<here>}`

    int  executed = 0;      ///< currently number of executions
    int  max      = -1;     ///< `:maxX`
    bool reverse  = false;  ///< `:reverse`

    long : 24 + 32;  // padding

    Iterator * it = nullptr;  ///< iterator for collection
};

}  // namespace service
}  // namespace icL

#endif  // service_Range
