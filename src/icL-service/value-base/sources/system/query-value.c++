#include "query-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/db-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>

namespace icL::service {

QueryValue::QueryValue() = default;

bool QueryValue::exec() {
    return _il()->db->exec();
}

bool QueryValue::first() {
    return _il()->db->first();
}

icVariant QueryValue::get(const icString & field) {
    return _il()->db->get(field);
}

icString QueryValue::getError() {
    return _il()->db->getError();
}

int QueryValue::getLength() {
    return _il()->db->getLength();
}

int QueryValue::getRowsAffected() {
    return _il()->db->getRowsAffected();
}

bool QueryValue::last() {
    return _il()->db->last();
}

bool QueryValue::next() {
    return _il()->db->next();
}

bool QueryValue::previous() {
    return _il()->db->previous();
}

bool QueryValue::seek(int i, bool relative) {
    return _il()->db->seek(i, relative);
}

void QueryValue::icSet(const icString & field, const icVariant & value) {
    return _il()->db->set(field, value);
}

il::Query QueryValue::_value() {
    return getValue();
}

}  // namespace icL::service
