#include "cookie-value.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/cookie-data.h++>



namespace icL::service {

CookieValue::CookieValue() = default;

void CookieValue::add(
  int years, int months, int days, int hours, int minutes, int seconds) {
    _value().data->expiry +=
      seconds +
      60 * (minutes + 60 * (hours + 24 * (days + 30 * (months + 365 * years))));
}

void CookieValue::load() {
    auto value = _value();

    _il()->server->pushTarget(value.data->target);
    *value.data = _il()->server->loadCookie(value.data->name);
    _il()->server->popTarget();
}

void CookieValue::resetTime() {
    _value().data->expiry = icDateTime::currentSecsSinceEpoch();
}

void CookieValue::save() {
    _il()->server->pushTarget(_value().data->target);
    _il()->server->saveCookie(*_value().data);
    _il()->server->popTarget();
}

il::Cookie CookieValue::_value() {
    return getValue();
}

}  // namespace icL::service
