#include "tab-value.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/signal.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-vm/mock.h++>



namespace icL::service {

TabValue::TabValue() = default;

icString TabValue::screenshot() {
    return _il()->server->screenshot();
}

icString TabValue::source() {
    return _il()->server->source();
}

icString TabValue::title() {
    return _il()->server->title();
}

void TabValue::back() {
    _il()->server->back();
}

void TabValue::close() {
    _il()->server->closeWindow();
}

void TabValue::focus() {
    _il()->server->focusWindow();
}

void TabValue::forward() {
    _il()->server->forward();
}

bool TabValue::get(const icString & url) {
    auto il = *_il();
    il.vm   = new vm::Mock();

    il.server->load(url);
    bool ret = il.vm->hasOkState();
    delete il.vm;

    return ret;
}

void TabValue::load(const icString & url) {
    icRegEx      rx    = R"(\A([\w\-\_]+)/([\w\-\_]+\.\w+)\z)"_rx;
    icRegExMatch match = rx.match(url);

    if (match.hasMatch()) {
        _il()->server->load(
          "file:///" + _il()->vms->getResourceDir(match.captured(1)) +
          match.captured(2));
    }
    else {
        _il()->server->load(url);
    }
}

il::Tab TabValue::_value() {
    return getValue();
}

}  // namespace icL::service
