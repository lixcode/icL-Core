#include "element-value.h++"

#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/set.h++>



namespace icL::service {

ElementValue::ElementValue() = default;

icString ElementValue::attr(const icString & name) {
    return _il()->server->attr(_value(), name);
}

bool ElementValue::clickable() {
    return _il()->server->clickable(_value());
}

icString ElementValue::css(const icString & name) {
    return _il()->server->css(_value(), name);
}

icVariant ElementValue::prop(const icString & name) {
    return _il()->server->prop(_value(), name);
}

il::Document ElementValue::document() {
    il::Document document;
    document.data = std::make_shared<il::TargetData>(_value().target);
    return document;
}

bool ElementValue::enabled() {
    return _il()->server->enabled(_value());
}

icRect ElementValue::rect() {
    return _il()->server->rect(_value());
}

bool ElementValue::selected() {
    return _il()->server->selected(_value());
}

icString ElementValue::tag() {
    return _il()->server->name(_value());
}

icString ElementValue::text() {
    return _il()->server->text(_value());
}

bool ElementValue::visible() {
    return _il()->server->visible(_value());
}

il::Element ElementValue::child(int index) {
    return _il()->server->child(_value(), index);
}

il::Elements ElementValue::children() {
    return _il()->server->children(_value());
}

void ElementValue::clear() {
    return _il()->server->clear(_value());
}

void ElementValue::click(const il::ClickData & data) {
    auto value = _value();
    return _il()->server->click(&value, data);
}

il::Element ElementValue::closest(const icString & cssSelector) {
    return _il()->server->closest(_value(), cssSelector);
}

il::Element ElementValue::copy() {
    return _il()->server->copy(_value());
}

void ElementValue::fastType(const icString & text) {
    _il()->server->fastType(_value(), text);
}

void ElementValue::forceClick(const il::ClickData & data) {
    _il()->server->forceClick(_value(), data);
}

void ElementValue::forceType(const icString & text) {
    _il()->server->forceType(_value(), text);
}

void ElementValue::hover(const il::HoverData & data) {
    auto value = _value();
    return _il()->server->hover(&value, data);
}

void ElementValue::keyDown(int modifiers, const icString & key) {
    auto value = _value();
    return _il()->server->keyDown(&value, modifiers, key);
}

void ElementValue::keyPress(int modifiers, int delay, const icString & keys) {
    auto value = _value();
    return _il()->server->keyPress(&value, modifiers, delay, keys);
}

void ElementValue::keyUp(int modifiers, const icString & key) {
    auto value = _value();
    return _il()->server->keyUp(&value, modifiers, key);
}

void ElementValue::mouseDown(const il::MouseData & data) {
    auto value = _value();
    return _il()->server->mouseDown(&value, data);
}

void ElementValue::mouseUp(const il::MouseData & data) {
    auto value = _value();
    return _il()->server->mouseUp(&value, data);
}

il::Element ElementValue::next() {
    return _il()->server->next(_value());
}

il::Element ElementValue::parent() {
    return _il()->server->parent(_value());
}

void ElementValue::paste(const icString & text) {
    return _il()->server->paste(_value(), text);
}

il::Element ElementValue::prev() {
    return _il()->server->prev(_value());
}

icString ElementValue::screenshot() {
    return _il()->server->screenshot(_value());
}

void ElementValue::sendKeys(int modifiers, const icString & text) {
    auto value = _value();
    return _il()->server->sendKeys(&value, modifiers, text);
}

void ElementValue::superClick() {
    _il()->server->superClick(_value());
}

il::Element ElementValue::_value() {
    return getValue();
}

}  // namespace icL::service
