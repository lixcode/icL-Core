#include "elements-value.h++"

#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/set.h++>



namespace icL::service {

using il::Selectors::Selectors;

ElementsValue::ElementsValue() = default;

icStringList ElementsValue::attr(const icString & name) {
    return _il()->server->attr(_value(), name);
}

bool ElementsValue::empty() {
    return _il()->server->empty(_value());
}

int ElementsValue::length() {
    return _il()->server->length(_value());
}

icVariant ElementsValue::prop(const icString & name) {
    return _il()->server->prop(_value(), name);
}

icRects ElementsValue::rects() {
    return _il()->server->rect(_value());
}

icStringList ElementsValue::tags() {
    return _il()->server->name(_value());
}

icStringList ElementsValue::texts() {
    return _il()->server->text(_value());
}

il::Elements ElementsValue::add_m1(const il::Element & el) {
    return _il()->server->add_m1(_value(), el);
}

il::Elements ElementsValue::add_mm(const il::Elements & el) {
    return _il()->server->add_mm(_value(), el);
}

il::Elements ElementsValue::copy() {
    return _il()->server->copy(_value());
}

il::Elements ElementsValue::filter(const icString & cssSelector) {
    return _il()->server->filter(_value(), cssSelector);
}

il::Element ElementsValue::get(int i) {
    return _il()->server->get(_value(), i);
}

il::Elements ElementsValue::_value() {
    return getValue();
}

}  // namespace icL::service
