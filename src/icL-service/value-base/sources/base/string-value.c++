#include "string-value.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>

namespace icL::service {

StringValue::StringValue() = default;

bool StringValue::empty() {
    return _value().isEmpty();
}

int StringValue::length() {
    return _value().length();
}

icString StringValue::last() {
    auto value = _value();

    if (value.isEmpty()) {
        _il()->vm->signal({memory::Signals::EmptyString, {}});
        return {};
    }

    return {value.at(value.length() - 1)};
}

void StringValue::append(const icString & str) {
    setValue(_value().append(str));
}

icString StringValue::at(int i) {
    auto value = _value();

    if (i < 0 || i >= value.length()) {
        _il()->vm->signal({memory::Signals::OutOfBounds, {}});
        return {};
    }

    return {value.at(i)};
}

bool StringValue::beginsWith(const icString & str) {
    return _value().beginsWith(str);
}

bool StringValue::compare(const icString & str, bool caseSensitive) {
    return _value().compare(str, caseSensitive);
}

int StringValue::count(const icString & str) {
    return _value().count(str);
}

int StringValue::count(const icRegEx & re) {
    return _value().count(re);
}

bool StringValue::endsWith(const icString & str) {
    return _value().endsWith(str);
}

int StringValue::indexOf(const icString & str, int startPos) {
    return _value().indexOf(str, startPos);
}

int StringValue::indexOf(const icRegEx & re, int startPos) {
    return _value().indexOf(re, startPos);
}

void StringValue::insert(const icString & str, int pos) {
    setValue(_value().insert(pos, str));
}

int StringValue::lastIndexOf(const icString & str, int startPos) {
    return _value().lastIndexOf(str, startPos);
}

int StringValue::lastIndexOf(const icRegEx & re, int startPos) {
    return _value().lastIndexOf(re, startPos);
}

icString StringValue::left(int n) {
    return _value().left(n);
}

icString StringValue::leftJustified(
  int width, const icString & fillChar, bool truncate) {
    if (fillChar.isEmpty()) {
        _il()->vm->signal({memory::Signals::EmptyString, {}});
        return {};
    }

    return _value().leftJustified(width, fillChar, truncate);
}

icString StringValue::mid(int pos, int n) {
    return _value().mid(pos, n);
}

void StringValue::prepend(const icString & str) {
    setValue(_value().prepend(str));
}

void StringValue::remove(int pos, int n) {
    setValue(_value().remove(pos, n));
}

void StringValue::remove(const icString & str, bool caseSensitive) {
    setValue(_value().remove(str, caseSensitive));
}

void StringValue::remove(const icRegEx & re) {
    setValue(_value().remove(re));
}

void StringValue::replace(int pos, int n, const icString & after) {
    setValue(_value().replace(pos, n, after));
}

void StringValue::replace(const icString & before, const icString & after) {
    setValue(_value().replace(before, after));
}

void StringValue::replace(const icRegEx & re, const icString & after) {
    setValue(_value().replace(re, after));
}

icString StringValue::right(int n) {
    return _value().right(n);
}

icString StringValue::rightJustified(
  int width, const icString & fillChar, bool truncate) {
    if (fillChar.isEmpty()) {
        _il()->vm->signal({memory::Signals::EmptyString, {}});
        return {};
    }

    return _value().rightJustified(width, fillChar, truncate);
}

icStringList StringValue::split(
  const icString & separator, bool keepEmptyParts, bool caseSensitive) {
    return _value().split(separator, keepEmptyParts, caseSensitive);
}

icStringList StringValue::split(const icRegEx & re, bool keepEmptyParts) {
    return _value().split(re, keepEmptyParts);
}

icString StringValue::substring(int begin, int end) {
    return _value().mid(begin, end - begin);
}

icString StringValue::toLowerCase() {
    return _value().toLower();
}

icString StringValue::toUpperCase() {
    return _value().toUpper();
}

icString StringValue::trim(bool justWhitespace) {
    return justWhitespace ? _value().trimmed() : _value().remove(R"(^\W|\W$)");
}

icString StringValue::_value() {
    return getValue().toString();
}

}  // namespace icL::service
