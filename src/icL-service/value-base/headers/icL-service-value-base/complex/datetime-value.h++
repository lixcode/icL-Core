#ifndef service_DatetimeValue
#define service_DatetimeValue

#include <icL-service-main/values/ivalue.h++>



class icDateTime;

namespace icL::service {

class DatetimeValue : virtual public IValue
{
public:
    DatetimeValue();
    ~DatetimeValue() override = default;

    /// `[r/o] icDateTime'valid : bool`
    bool valid();

    // methods level 1

    /// `icDateTime.addDays (days : int) : icDateTime`
    void addDays(int days);

    /// `icDateTime.addMonths (months : int) : icDateTime`
    void addMonths(int months);

    /// `icDateTime.addSecs (secs : int) : icDateTime`
    void addSecs(int secs);

    /// `icDateTime.addYears (months : int) : icDateTime`
    void addYears(int years);

    /// `icDateTime.daysTo (dt : icDateTime) : int`
    int daysTo(const icDateTime & dt);

    /// `icDateTime.secsTo (dt : icDateTime) : int`
    int secsTo(const icDateTime & dt);

    /// `icDateTime.toTimeZone (hours : int, minutes : int) : icDateTime`
    icDateTime toTimeZone(int hours, int minutes);

    /// `icDateTime.toUTC () : icDateTime`
    icDateTime toUTC();

protected:
    /**
     * @brief _value gets own value
     * @return own values as icDateTime
     */
    icDateTime _value();
};

}  // namespace icL::service

#endif  // service_DatetimeValue
