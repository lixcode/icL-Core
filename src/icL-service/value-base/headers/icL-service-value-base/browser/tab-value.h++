#ifndef service_TabValue
#define service_TabValue

#include <icL-service-main/values/inode.h++>
#include <icL-service-main/values/ivalue.h++>



class icString;

namespace icL {

namespace il {
struct Tab;
}

namespace service {

class TabValue
    : virtual public INode
    , virtual public IValue
{
public:
    TabValue();

    /// `[r/o] tab'screenshot : string`
    icString screenshot();

    /// `[r/o] tab'source : string`
    icString source();

    /// `[r/o] tab'title : string`
    icString title();

    // methods level 1

    /// `tab.back () : void`
    void back();

    /// `tab.close () : void`
    void close();

    /// `tab.focus () : void`
    void focus();

    /// `tab.forward () : void`
    void forward();

    /// `tab.get (url : string) : bool`
    bool get(const icString & url);

    /// `tab.load (url : string) : void`
    void load(const icString & url);

protected:
    /**
     * @brief _value returns the own value
     * @return the own value as tab pointer
     */
    il::Tab _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_TabValue
