#include "math.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-math.h++>

#include <algorithm>
#include <cmath>

namespace icL::service {

Math::Math() = default;

double Math::_1divPi() {
    return M_1_PI;
}

double Math::_1divSqrt2() {
    return M_SQRT1_2;
}

double Math::_2divPi() {
    return M_PI_2;
}

double Math::_2divSqrtPi() {
    return M_2_SQRTPI;
}

double Math::e() {
    return M_E;
}

double Math::ln2() {
    return M_LN2;
}

double Math::ln10() {
    return M_LN10;
}

double Math::log2e() {
    return M_LOG2E;
}

double Math::log10e() {
    return M_LOG10E;
}

double Math::pi() {
    return M_PI;
}

double Math::piDiv2() {
    return M_PI_2;
}

double Math::piDiv4() {
    return M_PI_4;
}

double Math::sqrt2() {
    return M_SQRT2;
}

double Math::acos(double v) {
    return icAcos(v);
}

double Math::asin(double v) {
    return icAsin(v);
}

double Math::atan(double v) {
    return icAtan(v);
}

int Math::ceil(double v) {
    return icCeil(v);
}

double Math::cos(double v) {
    return icCos(v);
}

double Math::degreesToRadians(double v) {
    return icDegreesToRadians(v);
}

double Math::exp(double v) {
    return icExp(v);
}

int Math::floor(double v) {
    return icFloor(v);
}

double Math::ln(double v) {
    return icLn(v);
}

int Math::max(icList<int> arr) {
    return *std::max_element(arr.begin(), arr.end());
}

double Math::max(icList<double> arr) {
    return *std::max_element(arr.begin(), arr.end());
}

int Math::min(icList<int> arr) {
    return *std::min_element(arr.begin(), arr.end());
}

double Math::min(icList<double> arr) {
    return *std::min_element(arr.begin(), arr.end());
}

double Math::radiansToDegrees(double v) {
    return icRadiansToDegrees(v);
}

int Math::round(double v) {
    return icRound(v);
}

double Math::sin(double v) {
    return icSin(v);
}

double Math::tan(double v) {
    return icTan(v);
}

}  // namespace icL::service
