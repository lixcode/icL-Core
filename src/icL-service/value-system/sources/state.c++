#include "state.h++"

#include <icL-il/main/interlevel.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/set.h++>

namespace icL::service {

State::State() = default;

void State::clear() {
    _il()->mem->stateIt().clear();
}

void State::delete_() {
    _il()->mem->stateIt().delete_();
}

void State::new_(const memory::Object & data) {
    _il()->mem->stateIt().appendNewAfter(data.data.get());
}

void State::newAtEnd(const memory::Object & data) {
    _il()->mem->stateIt().appendNewAtEnd(data.data.get());
}

void State::toFirst() {
    _il()->mem->stateIt().iterateToFirst();
}

void State::toLast() {
    _il()->mem->stateIt().iterateToLast();
}

void State::toNext() {
    _il()->mem->stateIt().iterateToNext();
}

void State::toPrev() {
    _il()->mem->stateIt().iterateToPrev();
}

}  // namespace icL::service
