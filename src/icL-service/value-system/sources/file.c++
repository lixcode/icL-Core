#include "file.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using il::FileTypes::FileTypes;

File::File() = default;

int File::csv() {
    return FileTypes::CSV;
}

int File::none() {
    return FileTypes::None;
}

int File::tsv() {
    return FileTypes::TSV;
}

}  // namespace icL::service
