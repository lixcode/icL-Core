#include "stacks.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/state/signals.h++>



namespace icL::service {

Stacks::Stacks() = default;

memory::StackContainer * Stacks::property(const icString & name) {
    auto * it = _il()->mem->stackIt().stack();

    while (it != nullptr && !it->isNamed(name)) {
        it = it->getPrev();
    }

    if (it == nullptr) {
        _il()->vm->signal({memory::Signals::System, "No such signal"});
    }

    return it;
}

}  // namespace icL::service
