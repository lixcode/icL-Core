#include "signal.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/stateful-server.h++>

namespace icL::service {

Signal::Signal() = default;

int Signal::property(const icString & name) {
    return _il()->stateful->getSignal(name);
}

void Signal::add(const icString & name) {
    _il()->stateful->registerSignal(name);
}

}  // namespace icL::service
