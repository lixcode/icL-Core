#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/request.h++>
#include <icL-il/structures/target-data.h++>

#include <request.h++>

namespace icL::service {

void Request::confirm(const icString & text) {
    _il()->request->confirm(text);
}

bool Request::ask(const icString & question) {
    return _il()->request->ask(question);
}

int Request::int_(const icString & text) {
    return _il()->request->int_(text);
}

double Request::double_(const icString & text) {
    return _il()->request->double_(text);
}

icString Request::string(const icString & text) {
    return _il()->request->string(text);
}

icStringList Request::list(const icString & text) {
    return _il()->request->list(text);
}

il::Tab Request::tab(const icString & text) {
    return _il()->request->tab(text);
}

}  // namespace icL::service
