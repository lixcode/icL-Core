#ifndef service_Request
#define service_Request

#include <icL-service-main/values/inode.h++>



class icString;
class icStringList;

namespace icL::il {
struct Tab;
}

namespace icL::service {

/**
 * @brief The Request struct represent a `Request` token
 */
class Request : virtual public INode
{
public:
    // methods level 1

    /// `Request.confirm (text : string) : void`
    void confirm(const icString & text);

    /// `Request.ask (question : string) : bool`
    bool ask(const icString & question);

    /// `Request.int (text : string) : int`
    int int_(const icString & text);

    /// `Request.double (text : string) : double`
    double double_(const icString & text);

    /// `Request.string (text : string) : string`
    icString string(const icString & text);

    /// `Request.list (text : string) : list`
    icStringList list(const icString & text);

    /// `Request.tab (text : string) : tab`
    il::Tab tab(const icString & text);
};

}  // namespace icL::service

#endif  // service_Request
