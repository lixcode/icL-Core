#ifndef service_File
#define service_File


namespace icL::service {

class File
{
public:
    File();

    // properties level 1

    /// `[r/o] File'csv : 1 `
    int csv();

    /// `[r/o] File'none : 0`
    int none();

    /// `[r/o] File'tsv : 2`
    int tsv();
};

}  // namespace icL::service

#endif  // service_File
