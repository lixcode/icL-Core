#ifndef service_Signal
#define service_Signal

#include <icL-service-main/values/inode.h++>



class icString;

namespace icL {

namespace il {
struct InterLevel;
}

namespace service {

class Signal : virtual public INode
{
public:
    Signal();

    // properties level 1

    /// `Signal'(name : string) : int`
    int property(const icString & name);

    // methods level 1

    /// `Signal.add (name : string) : void`
    void add(const icString & name);
};

}  // namespace service
}  // namespace icL

#endif  // service_Signal
