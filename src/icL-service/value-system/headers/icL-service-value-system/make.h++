#ifndef service_Make
#define service_Make



class icString;

namespace icL::service {

class Make
{
public:
    Make();

    // methods level 1

    /// `Make.image (base64 : string, path : string) : void`
    void image(const icString & base64, const icString & path);
};

}  // namespace icL::service

#endif  // service_Make
