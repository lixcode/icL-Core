#ifndef service_LimitedContext
#define service_LimitedContext

#include <icL-service-main/values/inode.h++>

#include <icL-memory/structures/packed-value-item.h++>
#include <icL-memory/structures/type.h++>


class icStringList;
template <typename>
class icList;

namespace icL {

namespace memory {
struct Set;
struct Object;
}  // namespace memory

namespace il {
struct Element;
}

namespace service {

class LimitedContext : virtual public INode
{
public:
    LimitedContext();

    /**
     * @brief makeList makes a icList from packed items values
     * @param items are icString and icList values
     */
    icStringList makeList(const memory::PackedItems & items);

    /**
     * @brief makeSet makes a icSet from packed items values
     * @param items are icObject and icSet values
     */
    memory::Set makeSet(const memory::PackedItems & items);

    /**
     * @brief makeElement makes a element fro packed items values
     * @param items are element values
     */
    il::Elements makeElement(const memory::PackedItems & items);

    /**
     * @brief makeObject makes a icObject from fields
     * @param items are fields
     */
    memory::Object makeObject(const memory::PackedItems & items);

    /**
     * @brief makeEmptySet makes a icSet from columns
     * @param items are columns
     */
    memory::Set makeEmptySet(const memory::PackedItems & items);

    /**
     * @brief checkItems checks if all values in pack are of type t1 or t2
     * @param items are the items to be cheched
     * @param t1 is the first accepted type
     * @param t2 is the second accepted type
     * @return true if values matches the filter
     */
    bool checkItems(
      const memory::PackedItems & items, memory::Type t1, memory::Type t2);

    /**
     * @brief checkItems checks if all items in pack match the requested type
     * @param items are the items to be checked
     * @param type is the packed value type to match
     * @return true if values matches the filter
     */
    bool checkItems(
      const memory::PackedItems & items, memory::PackedValueType type);
};

}  // namespace service
}  // namespace icL

#endif  // service_LimitedContext
