#ifndef service_SelectiveSelect
#define service_SelectiveSelect

#include <icL-service-main/values/inode.h++>



class icString;
class icStringList;

namespace icL {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace service {

class SelectiveSelect : virtual public INode
{
public:
    SelectiveSelect();

    /// `int % int : int`
    int intInt(int left, int right);

    /// `double % double : double`
    double doubleDouble(double left, double right);

    /// `string % string : list`
    icStringList stringString(const icString & left, const icString & right);

    /// `list % string : list`
    icStringList listString(const icStringList & left, const icString & right);

    /// `string % list : list`
    icStringList stringList(const icString & left, const icStringList & right);

    /// `list % list : list`
    class icStringList listList(
      const class icStringList & left, const class icStringList & right);

    /// `object % object : set`
    memory::Set objectObject(
      const memory::Object & left, const memory::Object & right);

    /// `set % object : set`
    memory::Set setObject(
      const memory::Set & left, const memory::Object & right);

    /// `object % set : set`
    memory::Set objectSet(
      const memory::Object & left, const memory::Set & right);

    /// `set % set : set`
    memory::Set setSet(const memory::Set & left, const memory::Set & right);
};

}  // namespace service
}  // namespace icL

#endif  // service_SelectiveSelect
