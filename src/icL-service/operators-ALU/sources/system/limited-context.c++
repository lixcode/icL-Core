#include "limited-context.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/return.h++>

#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>

namespace icL::service {

LimitedContext::LimitedContext() = default;

icStringList LimitedContext::makeList(const memory::PackedItems & items) {
    icStringList value;

    for (auto & item : items) {
        if (item.type == memory::Type::StringValue) {
            value.append(item.value.toString());
        }
        else /* item.type == memory::Type::ListValue */ {
            value.append(item.value.toStringList());
        }
    }

    return value;
}

memory::Set LimitedContext::makeSet(const memory::PackedItems & /*items*/) {
    memory::Set value;

    value.header  = std::make_shared<memory::Columns>();
    value.setData = std::make_shared<memory::SetData>();

    // TODO: Write it later

    return value;
}

il::Elements LimitedContext::makeElement(const memory::PackedItems & items) {
    il::Elements value;

    for (auto & item : items) {
        if (item.type == Type::ElementValue)
            value = _il()->server->add_m1(value, item.value);
        else
            value = _il()->server->add_mm(value, item.value);
    }

    return value;
}

memory::Object LimitedContext::makeObject(const memory::PackedItems & items) {
    memory::Object value;

    value.data = std::make_shared<memory::DataContainer>();

    for (auto & item : items) {
        if (!value.data->contains(item.name)) {
            value.data->setValue(item.name, item.value);
        }
        else {
            _il()->vm->signal(
              {memory::Signals::System, "Repeated icObject field"});
            return value;
        }
    }

    return value;
}

memory::Set LimitedContext::makeEmptySet(const memory::PackedItems & items) {
    memory::Set value;

    value.header  = std::make_shared<memory::Columns>();
    value.setData = std::make_shared<memory::SetData>();

    for (auto & item : items) {
        memory::Column column{item.type, item.name};

        if (!value.header->contains(column)) {
            value.header->append(column);
        }
        else {
            _il()->vm->signal(
              {memory::Signals::System, "Repeated icSet column"});
            return value;
        }
    }

    return value;
}

bool LimitedContext::checkItems(
  const memory::PackedItems & items, memory::Type t1, memory::Type t2) {
    bool ret = !items.isEmpty();

    for (auto & item : items) {
        if (item.type != t1 && item.type != t2) {
            ret = false;
        }
    }

    return ret;
}

bool LimitedContext::checkItems(
  const memory::PackedItems & items, memory::PackedValueType type) {
    bool ret = !items.isEmpty();

    for (auto & item : items) {
        if (item.itemType != type) {
            ret = false;
        }
    }

    return ret;
}

}  // namespace icL::service
