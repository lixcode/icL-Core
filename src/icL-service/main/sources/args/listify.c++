#include "listify.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce-base/main/value.h++>
#include <icL-ce-base/value/packed-value.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>

namespace icL::service {

memory::ArgList Listify::toArgList(il::InterLevel * il, ce::Value * value) {
    memory::ArgList ret;

    auto * packed = dynamic_cast<ce::PackedValue *>(value);

    if (packed != nullptr) {
        for (auto & value : *packed->getValues().data) {
            memory::Argument arg;

            if (
              value.itemType == memory::PackedValueType::Value ||
              value.itemType == memory::PackedValueType::Type) {
                arg.type      = value.type;
                arg.varName   = value.name;
                arg.value     = value.value;
                arg.container = value.container;
            }
            else {
                il->vm->syssig("Wrong arguments list");
                break;
            }

            ret.append(arg);
        }
    }
    else {
        memory::Argument        arg;
        memory::PackedValueItem packedValue = value->packNow();

        arg.type      = packedValue.type;
        arg.varName   = packedValue.name;
        arg.value     = packedValue.value;
        arg.container = packedValue.container;

        ret.append(arg);
    }

    return ret;
}

memory::ArgList Listify::toArgList(
  il::InterLevel * il, const icVariant & value) {
    memory::ArgList ret;

    if (value.isPacked()) {
        for (auto & item : *memory::PackedValue(value).data) {
            memory::Argument arg;

            if (
              item.itemType == memory::PackedValueType::Value ||
              item.itemType == memory::PackedValueType::Type) {
                arg.type      = item.type;
                arg.varName   = item.name;
                arg.value     = item.value;
                arg.container = item.container;
            }
            else {
                il->vm->syssig("Wrong arguments list");
                break;
            }

            ret.append(arg);
        }
    }
    else {
        memory::Argument arg;

        arg.type  = memory::variantToType(value);
        arg.value = value;

        ret.append(arg);
    }

    return ret;
}

memory::ParamList Listify::toParamList(
  il::InterLevel * il, const icVariant & value) {
    memory::ParamList ret;

    if (value.isPacked()) {
        for (auto & item : *memory::PackedValue(value).data) {
            memory::Parameter param;

            if (
              item.itemType == memory::PackedValueType::Parameter ||
              item.itemType == memory::PackedValueType::DefaultParameter) {
                param.type  = item.type;
                param.value = item.value;
                param.name  = item.name;
            }
            else {
                il->vm->syssig("Wrong parameters list");
                break;
            }

            ret.append(param);
        }
    }
    else {
        il->vm->syssig("Parameters list is not a packed value");
    }

    return ret;
}

bool Listify::isArgList(ce::Value * value) {
    auto * packed = dynamic_cast<ce::PackedValue *>(value);

    if (packed == nullptr) {
        return true;
    }

    bool ret = true;

    using memory::PackedValueType;

    for (auto & value : *packed->getValues().data) {
        ret = ret && (value.itemType == PackedValueType::Value ||
                      value.itemType == PackedValueType::Type);
    }

    return ret;
}

bool Listify::isParamList(ce::Value * value) {
    auto * packed = dynamic_cast<ce::PackedValue *>(value);

    if (packed == nullptr) {
        return value->role() == ce::Role::Parameter;
    }

    bool ret = true;

    using memory::PackedValueType;

    for (auto & value : *packed->getValues().data) {
        ret = ret && (value.itemType == PackedValueType::Parameter ||
                      value.itemType == PackedValueType::DefaultParameter);
    }

    return ret;
}

}  // namespace icL::service
