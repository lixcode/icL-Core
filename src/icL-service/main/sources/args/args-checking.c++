#include "args-checking.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::service {

bool ArgsChecking::checkArg(
  const memory::Argument & arg, const memory::Type type) {
    bool ret;

    switch (type) {
    case Type::BoolValue:
    case Type::IntValue:
    case Type::DoubleValue:
    case Type::StringValue:
    case Type::ListValue:
    case Type::ElementValue:
    case Type::DatetimeValue:
    case Type::ObjectValue:
    case Type::RegexValue:
    case Type::SetValue:
        ret = arg.type == type && !arg.value.isNull();
        break;

    case Type::VoidValue:
        ret = arg.type == type || arg.value.isNull();
        break;

    case Type::Type:
        ret = arg.value.isNull();
        break;

    case Type::AnyValue:
        ret = !arg.value.isNull();
        break;

    case Type::Identifier:
        ret = arg.type == Type::Identifier;
        break;

    default:
        ret = false;
    }

    return ret;
}

bool ArgsChecking::checkArgs(
  const memory::ArgList & args, const icList<Type> & standard) {
    if (args.length() != standard.length()) {
        return false;
    }

    bool ret = true;

    for (int i = 0; i < standard.length(); i++) {
        ret = ret && checkArg(args[i], standard[i]);
    }

    return ret;
}

bool ArgsChecking::checkArgs(
  const memory::Type type, const memory::ArgList & args) {
    bool ret = !args.isEmpty();

    for (auto & arg : args) {
        ret = ret && checkArg(arg, type);
    }

    return ret;
}

bool ArgsChecking::checkArgs(const memory::ArgList & args) {
    bool ret = true;

    for (const auto & arg : args) {
        ret = ret && !arg.value.isNull();
    }

    return ret;
}

}  // namespace icL::service
