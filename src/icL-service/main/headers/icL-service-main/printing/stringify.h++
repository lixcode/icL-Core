#ifndef service_Stringify
#define service_Stringify

class icString;
class icVariant;
class icStringList;
class icRegEx;

template <typename>
class icList;

namespace icL {

namespace memory {
struct Argument;
struct Set;
struct Object;
class DataContainer;
using ArgList = icList<Argument>;
struct PackedValue;
}  // namespace memory

namespace service {

class Stringify
{
public:
    /**
     * @brief argsToString casts a icList of arguments to icString
     * @param args is the icList of arguemnts to cast
     * @return a icString representation of argument icList
     */
    static icString fromArgs(const memory::ArgList & args);

    /**
     * @brief varToString casts a icVariant to a icString
     * @param value is the value to casts
     * @return a icString representation of icVariant
     */
    static icString fromVariant(const icVariant & value);

    /**
     * @brief listToString casts a icList to a icString representation
     * @param icList is the icList to cast to icString
     * @return a icString representation of icList
     */
    static icString fromList(const icStringList & icList);

    /**
     * @brief regexToString casts a icRegEx to a icString representation
     * @param icRegEx is the regular expression to cast to icString
     * @return a icString representation of regular expression
     */
    static icString formRegex(const icRegEx & regEx);

    /**
     * @brief setToString casts a icSet to a icString representation
     * @param icSet is the icSet to cast to icString
     * @return a icString representation of icSet
     */
    static icString fromSet(const memory::Set & icSet);

    /**
     * @brief fromPacked casts a packed value to a string representation
     * @param packed is the packed value to cast to string
     * @return a string string representation of packed
     */
    static icString fromPacked(const memory::PackedValue & packed);

    /**
     * @brief objectToString casts a icObject to a icString representation
     * @param icObject is the icObject to cast to icString
     * @return a icString representation of icObject
     */
    static icString fromObject(const memory::Object & icObject);

    /**
     * @brief argToString casts a argument to a icString representation
     * @param arg is the arguemnt to cast to icString
     * @return a icString representation of argument
     */
    static icString arg(
      const memory::Argument & arg, const memory::DataContainer * container);

    /**
     * @brief argSourceToString casts the source of argument to icString
     * @param arg is the arguemnt to extract to source of
     * @return a icString representation of argument container
     */
    static icString argSource(const memory::DataContainer * container);

    /**
     * @brief argConstructorToString casts a argument value to constructor
     * @param arg is the argument to extract value of
     * @return a icString representation of argument constructor
     */
    static icString argConstructor(
      const memory::Argument & arg, const memory::DataContainer * container);

    /**
     * @brief alternative works as icL alternative operator (just for strings)
     * @param str1 is the left argument
     * @param str2 is the right argument
     * @return str1 | str2
     */
    static const icString & alternative(
      const icString & str1, const icString & str2);
};

}  // namespace service
}  // namespace icL

#endif  // service_Stringify
