#ifndef service_Set
#define service_Set

#include <icL-memory/structures/type.h++>



template <typename>
class icList;
class icVariant;
class icString;
class icStringList;

using icVariantList = icList<icVariant>;

namespace std {
template <typename>
class function;
}

namespace icL {

namespace memory {
struct Object;
struct Set;
struct Argument;
using ArgList = icList<Argument>;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class Set
{
public:
    /**
     * @brief castStringTo casts a icString to requested type
     * @param type is the requested type of data
     * @param str is the icString to cast
     * @return the value casted to needed type or nullptr var on fail
     */
    static icVariant castStringTo(memory::Type type, const icString & str);

    /**
     * @brief getFieldIndex gets the index of field named `name`
     * @param name is the name of needed filed
     * @return the field index or -1 on failed
     */
    static int getFieldIndex(const memory::Set & icSet, const icString & name);

    /**
     * @brief checkObject checks the compability of icObject with icSet
     * @param obj is the icObject to check
     * @return true if is compatible, otherwise false
     */
    static bool checkObject(
      il::InterLevel * il, const memory::Set & value,
      const memory::Object & obj);

    /**
     * @brief checkSubObject checks the compability of subobject with icSet
     * @param obj is the icObject to check
     * @return true if is compatible, otherwise false
     */
    static icList<int> checkSubObject(
      il::InterLevel * il, const memory::Set & value,
      const memory::Object & obj);

    /**
     * @brief checkSet checks the compability of icSet with other icSet
     * @param checkSet is the icSet to check
     * @return matching indexes int he second icSet, for example [0, 2, 1]
     */
    static icList<int> checkSet(
      il::InterLevel * il, const memory::Set & s1, const memory::Set & s2);

    /**
     * @brief argsToList casts the icList<Argument> to icList<icVariant>
     * @param args is the arguments icList to cast
     * @return the value of arguments as a icVariant icList
     */
    static icVariantList argsToList(const memory::ArgList & args);

    /**
     * @brief fromObject creates a icSet from a icObject
     * @param obj is the icObject to create icSet from
     * @return a set contains the object `obj`
     */
    static memory::Set fromObject(const memory::Object & obj);

    /**
     * @brief isFullMatch check is the icList is a result of comparing complete
     * compatible sets
     * @param indices is the resutlt of checkSet() call
     * @return true if each indices[i] == i, otherwise false
     */
    static bool isFullMatch(const icList<int> & indices);

    /**
     * @brief forEach runs a function for each icObject in icSet
     * @param icSet is the icSet to interate
     * @param run is the function to run for icObject
     */
    static void forEach(
      const memory::Set &                                 icSet,
      const std::function<void(const memory::Object &)> & run);

    /**
     * @brief objectToList convert a icObject to raw icSet data
     * @param s is the icSet to extract header data
     * @param obj s the icObject to cast
     * @return icObject data as icSet raw data
     */
    static icVariantList objectToList(
      const memory::Set & s, const memory::Object & obj);

    /**
     * @brief rawToObject creates a icObject from raw icSet data
     * @param icSet is the icSet the extract specification
     * @param raw is the raw data of icObject
     * @return a icObject ready to use
     */
    static memory::Object rawToObject(
      const memory::Set & icSet, const icVariantList & raw);

    /// `set.applicate (data : list ...) : set`
    static void applicate(
      il::InterLevel * il, const memory::Set & value,
      const icList<icStringList> & data);

    /// `set.clone () : set`
    static memory::Set clone(const memory::Set & value);

    /// `set.getField (name : string) : list`
    static icStringList getField(
      il::InterLevel * il, const memory::Set & value, const icString & name);

    /// `set.insert (data : any ...) : set`
    static void insert(
      il::InterLevel * il, const memory::Set & value,
      const icVariantList & data);

    /// `set.insert (obj : object) : set`
    static void insert(
      il::InterLevel * il, const memory::Set & value,
      const memory::Object & obj);

    /// `[!] set.insert (set : set) : set`
    static void insert(
      il::InterLevel * il, const memory::Set & where, const memory::Set & what);

    /// `icSet.insertField (name : icString, value : icList, type = icString) :
    /// list`
    static memory::Set insertField(
      il::InterLevel * il, const memory::Set & value, const icString & name,
      const icStringList & values, memory::Type type);

    /// `set.insertField (name : string, value : any, type = void) : list`
    static memory::Set insertField(
      il::InterLevel * il, const memory::Set & value, const icString & name,
      const icVariant & var, memory::Type type);

    /// `set.remove (obj : object) : set`
    static void remove(
      il::InterLevel * il, const memory::Set & value,
      const memory::Object & obj);

    /// `set.remove (s : set) : set`
    static void remove(
      il::InterLevel * il, const memory::Set & from, const memory::Set & what);

    /// `set.remove (data : any ...) : set`
    static void remove(const memory::Set & value, const icVariantList & data);

    /// `set.removeField (name : string) : set`
    static memory::Set removeField(
      il::InterLevel * il, const memory::Set & value, const icString & name);

    /// `set + set : set`
    static memory::Set unite(
      il::InterLevel * il, const memory::Set & s1, const memory::Set & s2);

    /// `set \ set : set`
    static memory::Set complement(
      il::InterLevel * il, const memory::Set & s1, const memory::Set & s2);

    /// `set - set : set`
    static memory::Set symmetricDifference(
      il::InterLevel * il, const memory::Set & s1, const memory::Set & s2);

    /// `set * set : set`
    static memory::Set intersection(
      il::InterLevel * il, const memory::Set & s1, const memory::Set & s2);

    /// `set ** set : bool`
    static bool intersects(
      il::InterLevel * il, const memory::Set & s1, const memory::Set & s2);

    /// `set << set : bool`
    static bool contains(
      il::InterLevel * il, const memory::Set & s1, const memory::Set & s2);

    /// `set << object : bool`
    static bool contains(
      il::InterLevel * il, const memory::Set & s1, const memory::Object & o1);

    /// `set <* object : bool`
    static bool containsTemplate(
      il::InterLevel * il, const memory::Set & s1, const memory::Object & o1);

    /// `object <* object : bool`
    static bool containsTemplate(
      il::InterLevel * il, const memory::Object & o1,
      const memory::Object & o2);

    /// `set == set : bool`
    static bool compare(
      il::InterLevel * il, const memory::Set & s1, const memory::Set & s2);
};

}  // namespace service
}  // namespace icL

#endif  // service_Set
