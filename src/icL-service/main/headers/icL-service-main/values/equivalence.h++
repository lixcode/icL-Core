#ifndef service_Equivalence
#define service_Equivalence

#include <icL-types/replaces/ic-multi.h++>
#include <icL-types/replaces/ic-string.h++>

#include <utility>



template <typename>
class icList;
class icStringList;

namespace icL::service {

class Equivalence
{
private:
    Equivalence() = delete;

    /**
     * @brief The Key struct represent a word which can be paired
     */
    struct Key
    {
        /// \brief top contains the top of possible pairs of key
        icMulti<int, Key *> top;

        /// \brief word contains the word of key
        icString word;

        /// \brief paired becomes true after pairing, block repearted pairing
        bool paired = false;

        Key(icString word)
            : word(std::move(word)) {}
    };

    /**
     * @brief The FindResult struct represent a resunt of a match find
     */
    struct FindResult
    {
        /// index is the index of founded match
        int index;

        /// length is the length of match
        int legnth;
    };

    /**
     * @brief pair pairs to 2 words which are the same
     * @param key1 is the key of first word
     * @param key2 is the key of second word
     */
    static void pair(Key & key1, Key & key2);

    /**
     * @brief find find the next match in icString
     * @param str1 is the first icString
     * @param index1 is the cursor position in first icString
     * @param str2 is the second icString
     * @param index2 is the cursor postition in second icString
     * @return a {index, length} pair of next match
     */
    static FindResult find(
      const icString & str1, int index1, const icString & str2, int index2);

    /**
     * @brief calcMark calculates the number of commom symbols
     * @param str1 is the first icString
     * @param str2 is the second icString
     * @return the number of commom symbols
     */
    static int calcMark(const icString & str1, const icString & str2);

    /**
     * @brief join pairs 2 words which have a partial matching
     * @param key1 is the key of first word
     * @param key2 is the key of second word
     */
    static void join(Key & key1, Key & key2);

    /**
     * @brief normalize solves all partial matching
     * @param keys is the icList of keys with partial matching
     */
    static void normalize(icList<Key> & keys);

    /**
     * @brief mesh try to pair each key of first icList with each key of second
     * @param keys1 is the icList of first keys
     * @param keys2 is the icList of second keys
     */
    static void mesh(icList<Key> & keys1, icList<Key> & keys2);

public:
    /// icList ** icList : double
    static double calculate(const icStringList & l1, const icStringList & l2);

    /// icString ** icString : double
    static double calculate(const icString & s1, const icString & s2);
};

}  // namespace icL::service

#endif  // service_Equivalence
