#ifndef service_SmallerBigger
#define service_SmallerBigger



namespace icL::service {

class SmallerBigger
{
public:
    /// `int <> (int, int) : bool`
    bool intIntInt(int left, int begin, int end);

    /// `double <> (double, double) : bool`
    bool doubleDoubleDouble(double left, double begin, double end);
};

}  // namespace icL::service

#endif  // service_SmallerBigger
