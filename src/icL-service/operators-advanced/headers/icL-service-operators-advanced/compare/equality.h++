#ifndef service_Equality
#define service_Equality


#include <icL-service-main/values/inode.h++>



class icString;
class icStringList;

namespace icL {

namespace il {
struct Session;
struct Tab;
struct Window;
}  // namespace il

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace service {

class Equality : virtual public INode
{
public:
    // level 1

    /// `bool == bool : bool`
    bool boolBool(bool left, bool right);

    /// `int == int : bool`
    bool intInt(int left, int right);

    /// `double == double : bool`
    bool doubleDouble(double left, double right);

    /// `string == string : bool`
    bool stringString(const icString & left, const icString & right);

    /// `list == list : bool`
    bool listList(const icStringList & left, const icStringList & right);

    /// `object == object : bool`
    bool objectObject(
      const memory::Object & left, const memory::Object & right);

    /// `set == set : bool`
    bool setSet(const memory::Set & left, const memory::Set & right);

    /// `session == session : bool`
    bool sessionSession(const il::Session & left, const il::Session & right);

    /// `tab == tab : bool`
    bool tabTab(const il::Tab & left, const il::Tab & right);

    /// `window == window : bool`
    bool windowWindow(const il::Window & left, const il::Window & right);
};

}  // namespace service
}  // namespace icL

#endif  // service_Equality
