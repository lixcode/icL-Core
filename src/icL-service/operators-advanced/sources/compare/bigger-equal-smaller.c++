#include "bigger-equal-smaller.h++"



namespace icL::service {

bool BiggerEqualSmaller::intIntInt(int left, int begin, int end) {
    return left >= begin && left <= end;
}

bool BiggerEqualSmaller::doubleDoubleDouble(
  double left, double begin, double end) {
    return left >= begin && left <= end;
}

}  // namespace icL::service
