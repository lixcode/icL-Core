#include "smaller-equal.h++"



namespace icL::service {

bool SmallerEqual::intInt(int left, int right) {
    return left <= right;
}

bool SmallerEqual::doubleDouble(double left, double right) {
    return left <= right;
}

}  // namespace icL::service
