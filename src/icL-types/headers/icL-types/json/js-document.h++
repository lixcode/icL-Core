#ifndef types_jsDocument
#define types_jsDocument

#include "../replaces/ic-string.h++"



template <typename, typename>
class icPair;

class jsArray;
class jsObject;
class icVariant;

struct jsError
{
    icString errorString;
    enum { NoError, ParseError } error = NoError;
};

enum class jsRoot { Object, Array };

class jsDocument
{
    jsArray *  m_array  = nullptr;
    jsObject * m_object = nullptr;

public:
    jsDocument()                   = default;
    jsDocument(const jsDocument &) = delete;
    jsDocument(jsDocument && doc);

    ~jsDocument();

    bool isObject();
    bool isArray();

    // immutable members

    jsObject & object();
    jsArray &  array();
    icString   toJson();

    // mutable members

    jsDocument & root(jsRoot rootType);
    jsDocument & root(jsObject & root);
    jsDocument & root(jsArray & root);

    // object root

    jsDocument & insert(const icString & key, const icVariant & value);

    // static members

    static jsDocument fromJson(const icString & str, jsError & error);
};

#endif  // types_jsDocument
