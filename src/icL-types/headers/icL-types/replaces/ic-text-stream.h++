#ifndef types_icTextStream
#define types_icTextStream

#include <fstream>
#include <memory>



class icFile;
class icString;
class icChar;

class icTextStream
{
    std::ifstream d;

public:
    icTextStream() = default;
    icTextStream(icFile * stream);

    // immutable

    icString readAll();

    bool atEnd();

    int64_t pos();

    // mutable

    void open(icFile * stream);
    void open(std::shared_ptr<icFile> stream);

    bool seekBy(int position);
    bool seekTo(int position);

    // operators

    icTextStream & operator>>(icChar & ch);

private:
    void getAndCheck(char & byte);
    void checkByte(char byte);
    void printError(char byte);

    bool parsingFailed = false;
};

#endif  // types_icTextStream
