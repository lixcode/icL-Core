#ifndef types_icMath
#define types_icMath


double icPow(double v, double pow);
double icSqrt(double v);
double icAcos(double v);
double icAsin(double v);
double icAtan(double v);
int    icCeil(double v);
double icCos(double v);
double icDegreesToRadians(double v);
double icExp(double v);
int    icFloor(double v);
double icLn(double v);
double icRadiansToDegrees(double v);
int    icRound(double v);
double icSin(double v);
double icTan(double v);

#endif  // types_icMath
