#ifndef types_icPair
#define types_icPair

#include <utility>



template <typename T1, typename T2>
struct icPair
{
public:
    icPair() = default;

    icPair(T1 d1, T2 d2)
        : d1(std::move(d1))
        , d2(std::move(d2)) {}

    T1 & first() {
        return d1;
    }

    const T1 & first() const {
        return d1;
    }

    T2 & second() {
        return d2;
    }

    const T2 & second() const {
        return d2;
    }

    bool operator<(const icPair<T1, T1> & other) const {
        return d1 < other.d1 || (d1 == other.d1 && d2 < other.d2);
    }

    bool operator==(const icPair<T1, T1> & other) const {
        return d1 == other.d1 && d2 == other.d2;
    }

private:
    T1 d1{};
    T2 d2{};
};

#endif  // types_icPair
