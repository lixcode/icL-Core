#ifndef types_Set
#define types_Set

#include "ic-hash-functions.h++"

#include <QSet>


template <typename T>
class icSet
{
    QSet<T> d;

public:
    icSet() = default;

    icSet(std::initializer_list<T> list)
        : d(list) {}

    icSet(const icSet & base, std::initializer_list<T> list)
        : d(list) {
        for (auto & value : base) {
            d.insert(value);
        }
    }

    icSet(const QSet<T> & set) {
        d = set;
    }

    using Iterator      = typename QSet<T>::Iterator;
    using ConstIterator = typename QSet<T>::ConstIterator;

    // properties

    int size() const {
        return d.size();
    }

    bool isEmpty() const {
        return d.isEmpty();
    }

    int capacity() const {
        return d.capacity();
    }

    int count() const {
        return d.count();
    }

    // immutable members

    bool contains(const T & value) const {
        return d.contains(value);
    }

    bool contains(const icSet & other) const {
        return d.contains(other.d);
    }

    bool empty() const {
        return d.empty();
    }

    bool intersects(const icSet & other) const {
        return d.intersects(other.d);
    }

    // mutable members

    icSet & clear() {
        d.clear();
        return *this;
    }

    icSet & insert(const T & value) {
        d.insert(value);
        return *this;
    }

    icSet & remove(const T & value) {
        d.remove(value);
        return *this;
    }

    // operators

    bool operator==(const icSet & other) const {
        return d == other.d;
    }

    icSet operator+(const icSet & other) const {
        return d + other.d;
    }

    icSet operator-(const icSet & other) const {
        return d - other.d;
    }

    icSet operator*(const icSet & other) const {
        return d & other.d;
    }

    // iterators

    Iterator begin() {
        return d.begin();
    }

    Iterator end() {
        return d.end();
    }

    ConstIterator begin() const {
        return d.begin();
    }

    ConstIterator end() const {
        return d.end();
    }
};

#endif  // types_Set
