#include "js-array.h++"

#include "ic-string.h++"
#include "js-object.h++"

#include <QJsonArray>
#include <QJsonObject>



jsArray::jsArray() = default;

QJsonArray jsArray::toJson() const {
    QJsonArray ret;

    for (auto & val : *this) {
        switch (val.type()) {
        case icType::Bool:
            ret.append(val.toBool());
            break;

        case icType::Int:
            ret.append(val.toInt());
            break;

        case icType::Double:
            ret.append(val.toDouble());
            break;

        case icType::String:
            ret.append(val.toString().getData());
            break;

        case icType::JsObject:
            ret.append(val.toJsObject().toJson());
            break;

        case icType::JsArray:
            ret.append(val.toArray().toJson());
            break;

        default:;
            // nothing to do
        }
    }
    return ret;
}

jsArray & jsArray::fromJson(QJsonArray json) {
    for (auto val : json) {
        switch (val.type()) {
        case QJsonValue::Bool:
            append(val.toBool());
            break;

        case QJsonValue::Double:
            append(val.toDouble());
            break;

        case QJsonValue::String:
            append(icString{val.toString()});
            break;

        case QJsonValue::Array:
            append(jsArray{}.fromJson(val.toArray()));
            break;

        case QJsonValue::Object:
            append(jsObject{}.fromJson(val.toObject()));
            break;

        default:
            append(icVariant{});
        }
    }

    return *this;
}
