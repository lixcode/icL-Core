#include "ic-image.h++"

#include <ic-string.h++>

#include "fstream"

icImage::icImage() = default;

void icImage::saveData(const icString & base64, const icString & path) {
    std::ofstream file(path.toString(), std::ios_base::binary);

    assert(file.is_open());

    char buffer;

    uint val  = 0;
    int  valb = -8;
    for (char c : base64.toString()) {
        char code = -1;

        if (c >= 'A' && c <= 'Z') {
            code = c - 'A';
        }
        else if (c >= 'a' && c <= 'z') {
            code = c - 'a' + ('Z' - 'A');
        }
        else if (c >= '0' && c <= '9') {
            code = c - '0' + ('Z' - 'A') + ('z' - 'a');
        }
        else if (c == '+') {
            code = 62;
        }
        else if (c == '/') {
            code = 63;
        }
        else {
            break;
        }

        val = (val << 6) + uint8_t(code);
        valb += 6;

        if (valb >= 0) {
            buffer = char((val >> valb) & 0xFF);
            file.write(&buffer, 1);
            valb -= 8;
        }
    }
}
