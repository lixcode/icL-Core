#include "ic-variant.h++"

#include "ic-datetime.h++"
#include "ic-file.h++"
#include "ic-regex.h++"
#include "ic-string.h++"
#include "js-array.h++"
#include "js-object.h++"

#include <icL-il/structures/cookie-data.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/file-target.h++>
#include <icL-il/structures/lambda-target.h++>

#include <icL-memory/structures/packed-value-item.h++>
#include <icL-memory/structures/set.h++>

#include <cassert>



icVariant::icVariant() {
    m_type = icType::Initial;
}

icVariant::icVariant(const bool value) {
    d      = value;
    m_type = icType::Bool;
}

icVariant::icVariant(const int & value)
    : icVariant() {
    d      = value;
    m_type = icType::Int;
}

icVariant::icVariant(const double & value)
    : icVariant() {
    d      = value;
    m_type = icType::Double;
}

icVariant::icVariant(const icString & string)
    : icVariant() {
    d      = string;
    m_type = icType::String;
}

icVariant::icVariant(const icDateTime & dt)
    : icVariant() {
    d      = dt;
    m_type = icType::DateTime;
}

icVariant::icVariant(const icStringList & list)
    : icVariant() {
    d      = list;
    m_type = icType::StringList;
}

icVariant::icVariant(const icRegEx & re)
    : icVariant() {
    d      = re;
    m_type = icType::RegEx;
}

icVariant::icVariant(const icL::memory::PackedValue & packet)
    : icVariant() {
    d      = packet;
    m_type = icType::Packed;
}

icVariant::icVariant(const jsArray & array)
    : icVariant() {
    d      = array;
    m_type = icType::JsArray;
}

icVariant::icVariant(const jsObject & object)
    : icVariant() {
    d      = object;
    m_type = icType::JsObject;
}

icVariant icVariant::makeVoid() {
    icVariant ret;
    ret.m_type = icType::Void;
    return ret;
}

icType icVariant::type() const {
    return m_type;
}

bool icVariant::isBool() const {
    return m_type == icType::Bool;
}

bool icVariant::isInt() const {
    return m_type == icType::Int;
}

bool icVariant::isDouble() const {
    return m_type == icType::Double;
}

bool icVariant::isString() const {
    return m_type == icType::String;
}

bool icVariant::isCookie() const {
    return m_type == icType::Cookie;
}

bool icVariant::isDB() const {
    return m_type == icType::DB;
}

bool icVariant::isQuery() const {
    return m_type == icType::Query;
}

bool icVariant::isElement() const {
    return m_type == icType::Element;
}

bool icVariant::isElements() const {
    return m_type == icType::Elements;
}

bool icVariant::isDocument() const {
    return m_type == icType::Document;
}

bool icVariant::isSession() const {
    return m_type == icType::Session;
}

bool icVariant::isWindow() const {
    return m_type == icType::Window;
}

bool icVariant::isTab() const {
    return m_type == icType::Tab;
}

bool icVariant::isDateTime() const {
    return m_type == icType::DateTime;
}

bool icVariant::isStringList() const {
    return m_type == icType::StringList;
}

bool icVariant::isRegEx() const {
    return m_type == icType::RegEx;
}

bool icVariant::isObject() const {
    return m_type == icType::Object;
}

bool icVariant::isJsObject() const {
    return m_type == icType::JsObject;
}

bool icVariant::isSet() const {
    return m_type == icType::Set;
}

bool icVariant::isPacked() const {
    return m_type == icType::Packed;
}

bool icVariant::isFile() const {
    return m_type == icType::File;
}

bool icVariant::isJsArray() const {
    return m_type == icType::JsArray;
}

bool icVariant::isPosition() const {
    return m_type == icType::Position;
}

bool icVariant::isLambda() const {
    return m_type == icType::Lamda;
}

bool icVariant::isJsLambda() const {
    return m_type == icType::JsLambda;
}

bool icVariant::isJsFile() const {
    return m_type == icType::JsFile;
}

bool icVariant::isNull() const {
    return m_type == icType::Initial;
}

bool icVariant::isVoid() const {
    return m_type == icType::Void;
}

bool icVariant::isValid() const {
    return m_type != icType::Void && m_type != icType::Initial;
}

template <typename T>
icVariant icVariant::fromValueTemplate(const T & value, icType type) {
    icVariant ret;
    ret.d      = value;
    ret.m_type = type;
    return ret;
}

icVariant icVariant::fromValue(const bool & value) {
    return fromValueTemplate(value, icType::Bool);
}

icVariant icVariant::fromValue(const int & value) {
    return fromValueTemplate(value, icType::Int);
}

icVariant icVariant::fromValue(const double & value) {
    return fromValueTemplate(value, icType::Double);
}

icVariant icVariant::fromValue(const icString & string) {
    return fromValueTemplate(string, icType::String);
}

icVariant icVariant::fromValue(const icL::il::Cookie & cookie) {
    return fromValueTemplate(cookie, icType::Cookie);
}

icVariant icVariant::fromValue(const icL::il::DB & db) {
    return fromValueTemplate(db, icType::DB);
}

icVariant icVariant::fromValue(const icL::il::Query & query) {
    return fromValueTemplate(query, icType::Query);
}

icVariant icVariant::fromValue(const icL::il::Element & element) {
    return fromValueTemplate(element, icType::Element);
}

icVariant icVariant::fromValue(const icL::il::Elements & elements) {
    return fromValueTemplate(elements, icType::Elements);
}

icVariant icVariant::fromValue(const icL::il::Document & document) {
    return fromValueTemplate(document, icType::Document);
}

icVariant icVariant::fromValue(const icL::il::Session & session) {
    return fromValueTemplate(session, icType::Session);
}

icVariant icVariant::fromValue(const icL::il::Window & window) {
    return fromValueTemplate(window, icType::Window);
}

icVariant icVariant::fromValue(const icL::il::Tab & tab) {
    return fromValueTemplate(tab, icType::Tab);
}

icVariant icVariant::fromValue(const icDateTime & dt) {
    return fromValueTemplate(dt, icType::DateTime);
}

icVariant icVariant::fromValue(const icStringList & list) {
    return fromValueTemplate(list, icType::StringList);
}

icVariant icVariant::fromValue(const icRegEx & re) {
    return fromValueTemplate(re, icType::RegEx);
}

icVariant icVariant::fromValue(const icL::memory::Object & obj) {
    return fromValueTemplate(obj, icType::Object);
}

icVariant icVariant::fromValue(const icL::memory::Set & set) {
    return fromValueTemplate(set, icType::Set);
}

icVariant icVariant::fromValue(const icL::memory::PackedValue & packet) {
    return fromValueTemplate(packet, icType::Packed);
}

icVariant icVariant::fromValue(const icL::il::File & file) {
    return fromValueTemplate(file, icType::File);
}

icVariant icVariant::fromValue(const jsArray & array) {
    return fromValueTemplate(array, icType::JsArray);
}

icVariant icVariant::fromValue(const jsObject & object) {
    return fromValueTemplate(object, icType::JsObject);
}

icVariant icVariant::fromValue(const icL::il::Position & postion) {
    return fromValueTemplate(postion, icType::Position);
}

icVariant icVariant::fromValue(const icL::il::LambdaTarget & lamda) {
    return fromValueTemplate(lamda, icType::Lamda);
}

icVariant icVariant::fromValue(const icL::il::JsLambda & lambda) {
    return fromValueTemplate(lambda, icType::JsLambda);
}

icVariant icVariant::fromValue(const icL::il::JsFile & file) {
    return fromValueTemplate(file, icType::JsFile);
}

const bool & icVariant::toBool() const {
    assert(isBool());
    return std::any_cast<const bool &>(d);
}

const int & icVariant::toInt() const {
    assert(isInt());
    return std::any_cast<const int &>(d);
}

const double & icVariant::toDouble() const {
    assert(isDouble());
    return std::any_cast<const double &>(d);
}

const icString & icVariant::toString() const {
    assert(isString());
    return std::any_cast<const icString &>(d);
}

const icL::il::Cookie & icVariant::toCookie() const {
    assert(isCookie());
    return std::any_cast<const icL::il::Cookie &>(d);
}

const icL::il::DB & icVariant::toDB() const {
    assert(isDB());
    return std::any_cast<const icL::il::DB &>(d);
}

const icL::il::Query & icVariant::toQuery() const {
    assert(isQuery());
    return std::any_cast<const icL::il::Query &>(d);
}

const icL::il::Element & icVariant::toElement() const {
    assert(isElement());
    return std::any_cast<const icL::il::Element &>(d);
}

const icL::il::Elements & icVariant::toElements() const {
    assert(isElements());
    return std::any_cast<const icL::il::Elements &>(d);
}

const icL::il::Document & icVariant::toDocument() const {
    assert(isDocument());
    return std::any_cast<const icL::il::Document &>(d);
}

const icL::il::Session & icVariant::toSession() const {
    assert(isSession());
    return std::any_cast<const icL::il::Session &>(d);
}

const icL::il::Window & icVariant::toWindow() const {
    assert(isWindow());
    return std::any_cast<const icL::il::Window &>(d);
}

const icL::il::Tab & icVariant::toTab() const {
    assert(isTab());
    return std::any_cast<const icL::il::Tab &>(d);
}

const icL::il::TargetData icVariant::toTarget() const {
    if (isDocument()) {
        return *toDocument().data;
    }
    else if (isTab()) {
        return *toTab().data;
    }
    else if (isWindow()) {
        return *toWindow().data;
    }
    else if (isSession()) {
        return *toSession().data;
    }

    return {};
}

const icDateTime & icVariant::toDateTime() const {
    assert(isDateTime());
    return std::any_cast<const icDateTime &>(d);
}

const icStringList & icVariant::toStringList() const {
    assert(isStringList());
    return std::any_cast<const icStringList &>(d);
}

const icRegEx & icVariant::toRegEx() const {
    assert(isRegEx());
    return std::any_cast<const icRegEx &>(d);
}

const icL::memory::Object & icVariant::toObject() const {
    assert(isObject());
    return std::any_cast<const icL::memory::Object &>(d);
}

const icL::memory::Set & icVariant::toSet() const {
    assert(isSet());
    return std::any_cast<const icL::memory::Set &>(d);
}

const icL::memory::PackedValue & icVariant::toPacked() const {
    assert(isPacked());
    return std::any_cast<const icL::memory::PackedValue &>(d);
}

const icL::il::File & icVariant::toFile() const {
    assert(isFile());
    return std::any_cast<const icL::il::File &>(d);
}

const jsArray & icVariant::toArray() const {
    assert(isJsArray());
    return std::any_cast<const jsArray &>(d);
}

const jsObject & icVariant::toJsObject() const {
    assert(isJsObject());
    return std::any_cast<const jsObject &>(d);
}

const icL::il::Position & icVariant::toPosition() const {
    assert(isPosition());
    return std::any_cast<const icL::il::Position &>(d);
}

const icL::il::LambdaTarget & icVariant::toLambda() const {
    assert(isLambda());
    return std::any_cast<const icL::il::LambdaTarget &>(d);
}

const icL::il::JsLambda & icVariant::toJsLambda() const {
    assert(isJsLambda());
    return std::any_cast<const icL::il::JsLambda &>(d);
}

const icL::il::JsFile & icVariant::toJsFile() const {
    assert(isJsFile());
    return std::any_cast<const icL::il::JsFile &>(d);
}

icVariant & icVariant::clear() {
    m_type = icType::Void;
    return *this;
}

bool icVariant::operator==(const icVariant & other) const {
    if (m_type != other.m_type)
        return false;

    switch (m_type) {
    case icType::Initial:
    case icType::Void:
        return true;

    case icType::Bool:
        return toBool() == other.toBool();

    case icType::Int:
        return toInt() == other.toInt();

    case icType::Double:
        return toDouble() == other.toDouble();

    case icType::String:
        return toString() == other.toString();

    case icType::Cookie:
        return toCookie() == other.toCookie();

    case icType::DB:
        return toDB() == other.toDB();

    case icType::Query:
        return toQuery() == other.toQuery();

    case icType::Element:
        return toElement() == other.toElement();

    case icType::Elements:
        return toElements() == other.toElements();

    case icType::Document:
        return toDocument() == other.toDocument();

    case icType::Session:
        return toSession() == other.toSession();

    case icType::Window:
        return toWindow() == other.toWindow();

    case icType::Tab:
        return toTab() == other.toTab();

    case icType::DateTime:
        return toDateTime() == other.toDateTime();

    case icType::StringList:
        return toStringList() == other.toStringList();

    case icType::RegEx:
        return toRegEx() == other.toRegEx();

    case icType::Object:
        return toObject() == other.toObject();

    case icType::Set:
        return toSet() == other.toSet();

    case icType::Packed:
        return toPacked() == other.toPacked();

    case icType::File:
        return toFile() == other.toFile();

    case icType::JsArray:
        return toArray() == other.toArray();

    case icType::JsObject:
        return toJsObject() == other.toJsObject();

    case icType::JsLambda:
        return toJsLambda() == other.toJsLambda();

    case icType::Position:
        return toPosition().absolute == other.toPosition().absolute;

    case icType::Lamda:
        return toLambda() == other.toLambda();

    case icType::JsFile:
        return toJsFile() == other.toJsFile();
    }
}

icVariant::operator icRegEx() const {
    assert(isRegEx());
    return std::any_cast<const icRegEx &>(d);
}

icVariant::operator icStringList() const {
    assert(isStringList());
    return std::any_cast<const icStringList &>(d);
}

icVariant::operator icDateTime() const {
    assert(isDateTime());
    return std::any_cast<const icDateTime &>(d);
}

icVariant::operator icString() const {
    assert(isString());
    return std::any_cast<const icString &>(d);
}

icVariant::operator double() const {
    assert(isDouble());
    return std::any_cast<const double &>(d);
}

icVariant::operator int() const {
    assert(isInt());
    return std::any_cast<const int &>(d);
}

icVariant::operator bool() const {
    assert(isBool());
    return std::any_cast<const bool &>(d);
}
