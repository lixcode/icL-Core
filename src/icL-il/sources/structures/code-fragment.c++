#include "code-fragment.h++"

#include <icL-cp/source/source-of-code.h++>



namespace icL::il {

icString CodeFragment::getCode() const {

    SourceOfCode * code = nullptr;

    do {
        try {
            code = source->fragment(begin, end);
        }
        catch (...) {
            continue;
        }
    } while (code == nullptr);

    icString ret;

    do {
        code->next();
        ret += code->current();
    } while (!code->atEnd());

    delete code;
    return ret;
}

CodeFragment & CodeFragment::operator=(const CodeFragment & other) {
    begin  = other.begin;
    end    = other.end;
    source = other.source;
    name   = other.name;
    return *this;
}

}  // namespace icL::il
