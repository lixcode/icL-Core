#include "cookie-data.h++"

#include <icL-types/replaces/ic-variant.h++>



namespace icL::il {

icL::il::Cookie::operator icVariant() const {
    return icVariant::fromValue(*this);
}

Cookie::Cookie(const icVariant & value) {
    assert(value.isCookie());
    data = value.toCookie().data;
}

bool Cookie::operator==(const Cookie & other) const {
    return data->name == other.data->name;
}

}  // namespace icL::il
