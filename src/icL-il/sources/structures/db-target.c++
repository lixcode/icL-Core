#include "db-target.h++"

#include <icL-types/replaces/ic-variant.h++>



namespace icL::il {

DB::DB(const icVariant & value) {
    assert(value.isDB());
    *this = value.toDB();
}

bool DB::operator==(const DB & other) const {
    return target->databaseId == other.target->databaseId;
}

DB::operator icVariant() const {
    return icVariant::fromValue(*this);
}

Query::Query(const icVariant & value) {
    assert(value.isQuery());
    *this = value.toQuery();
}

bool Query::operator==(const Query & other) const {
    return target->queryId == other.target->queryId;
}

Query::operator icVariant() const {
    return icVariant::fromValue(*this);
}

}  // namespace icL::il
