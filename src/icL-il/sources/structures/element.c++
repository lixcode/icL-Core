#include "element.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::il {

Element::operator icVariant() const {
    return icVariant::fromValue(*this);
}

Element::Element(const icVariant & variant) {
    *this = variant.toElement();
}

bool Element::operator==(const Element & other) const {
    return variable == other.variable;
}

icL::il::Elements::Elements(const icVariant & variant) {
    *this = variant.toElements();
}

int icL::il::Elements::size() const {
    return _size;
}

void Elements::setSize(int size) {
    _size = size;
}

icL::il::Elements::operator icVariant() const {
    return icVariant::fromValue(*this);
}

}  // namespace icL::il
