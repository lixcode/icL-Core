#include "target-data.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::il {

bool TargetData::operator==(const TargetData & other) const {
    return sessionId == other.sessionId && windowId == other.windowId;
}

bool TargetData::operator!=(const TargetData & other) const {
    return sessionId != other.sessionId || windowId != other.windowId;
}

Session::operator icVariant() const {
    return icVariant::fromValue(*this);
}

icL::il::Tab::operator icVariant() const {
    return icVariant::fromValue(*this);
}

icL::il::Window::operator icVariant() const {
    return icVariant::fromValue(*this);
}

Session::Session(const icVariant & value) {
    assert(value.isSession());
    *this = value.toSession();
}

bool Session::operator==(const Session & other) const {
    return data == other.data;
}

Tab::Tab(const icVariant & variant) {
    assert(variant.isTab());
    *this = variant.toTab();
}

bool Tab::operator==(const Tab & other) const {
    return data == other.data;
}

Window::Window(const icVariant & value) {
    assert(value.isWindow());
    *this = value.toWindow();
}

bool Window::operator==(const Window & other) const {
    return data == other.data;
}

Document::Document(const icVariant & variant) {
    assert(variant.isDocument());
    *this = variant.toDocument();
}

icL::il::Document::operator icVariant() const {
    return icVariant::fromValue(*this);
}

bool Document::operator==(const Document & other) const {
    return data == other.data;
}

}  // namespace icL::il
