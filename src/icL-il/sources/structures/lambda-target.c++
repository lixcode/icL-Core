#include "lambda-target.h++"



namespace icL::il {

LambdaTarget::LambdaTarget(const icVariant & value) {
    assert(value.isLambda());
    *this = value.toLambda();
}

icL::il::LambdaTarget::operator icVariant() const {
    return icVariant::fromValue(*this);
}

bool LambdaTarget::operator==(const LambdaTarget & other) const {
    return target == other.target;
}

JsLambda::JsLambda(const icVariant & value) {
    *this = value.toJsLambda();
}

bool JsLambda::operator==(const JsLambda & other) const {
    return target == other.target && data == other.data;
}

icL::il::JsLambda::operator icVariant() const {
    return icVariant::fromValue(*this);
}

}  // namespace icL::il
