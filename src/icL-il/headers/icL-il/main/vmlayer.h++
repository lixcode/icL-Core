#ifndef il_VMLayer
#define il_VMLayer

class icString;
class icVariant;

namespace icL {

namespace memory {
class Memory;
}

namespace il {

class Signal;
struct Return;

/**
 * @brief The VMLayer class is a virtual machine linked to a fragment of code
 */
class VMLayer
{
public:
    virtual ~VMLayer() = default;

    /**
     * @brief signal tries to handle a signal
     * @param signal is the signal to handle
     */
    virtual void signal(const Signal & signal) = 0;

    /**
     * @brief syssig tries to handle a system signal
     * @param message is the message of signal
     */
    virtual void syssig(const icString & message) = 0;

    /**
     * @brief cp_sig tries to handle a signal from command processor
     * @param message is the message of signal
     */
    virtual void cp_sig(const icString & message) = 0;

    /**
     * @brief cpw_sig tries to handle a signal from command processor extensions
     * @param message is the message of signal
     */
    virtual void cpe_sig(const icString & message) = 0;

    /**
     * @brief assert send a message about assert fail
     * @param message is the message of assert expression
     */
    virtual void sendAssert(const icString & message) = 0;

    /**
     * @brief sleep delays the switching to new command for fixed ms
     * @param ms is the needed delay in ms
     */
    virtual void sleep(int ms) = 0;

    // any stack

    /**
     * @brief addDescription adds a description to vm layer
     * @param description is the new description of vm layer
     */
    virtual void addDescription(const icString & description) = 0;

    /**
     * @brief markStep mark vm layer as step
     */
    virtual void markStep(const icString & name) = 0;

    /**
     * @brief markTest marks vm layer as test
     */
    virtual void markTest(const icString & name) = 0;

    // loop stack

    /**
     * @brief break_ breaks the loop execution
     */
    virtual void break_() = 0;

    /**
     * @brief continue_ continues with a new iteration
     */
    virtual void continue_() = 0;

    // function stack

    /**
     * @brief return_ returns the value of function execution
     * @param value is the value which must be returned
     */
    virtual void return_(const icVariant & value) = 0;

    /**
     * @brief hasOkState checks if the virtual machine has not crashed yet
     * @return false if a signal was emitted, otherwise true
     */
    virtual bool hasOkState() = 0;

    /**
     * @brief parent gets the upper layer in the VMs stack
     * @return a pointer to the upper level of VMs stack
     */
    virtual VMLayer * parent() = 0;

    /**
     * @brief finalize runs the feedback function of layer
     */
    virtual void finalize() = 0;
};

}  // namespace il
}  // namespace icL

#endif  // il_VMLayer
