#ifndef il_InterLevel
#define il_InterLevel

namespace icL {

namespace memory {
class Memory;
}

namespace il {

class CP;
class VMLayer;
class VMStack;
class FrontEnd;
class DBServer;
class FileServer;
class SourceServer;
class StatefulServer;
class Listen;
class Request;

/**
 * @brief The InterLevel struct contains interfaces to each level
 */
struct InterLevel
{
    ~InterLevel() {}

    /// \brief mem is a pointer to memory level
    memory::Memory * mem = nullptr;

    /// \brief cpu is a pointer to command processor level
    CP * cpu = nullptr;

    /// \brief vm is a pointer to virtual machine layer
    VMLayer * vm = nullptr;

    /// \brief vms is a pointer to highest level - virtual machine stack
    VMStack * vms = nullptr;

    /// \brief server is a pointer to synchronization server
    FrontEnd * server = nullptr;

    /// \brief db is the pointer to database server
    DBServer * db = nullptr;

    /// \brief file is a pointer to the file service
    FileServer * file = nullptr;

    /// \brief source is a pointer to the source server
    SourceServer * source = nullptr;

    /// \brief stateful is a pointer to the setateful server
    StatefulServer * stateful = nullptr;

    /// \brief listen is a syncronization server
    Listen * listen = nullptr;

    /// \brief request is a "requesting" server
    Request * request = nullptr;
};

}  // namespace il
}  // namespace icL


#endif  // il_InterLevel
