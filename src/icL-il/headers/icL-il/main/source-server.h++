#ifndef il_SourceServer
#define il_SourceServer

#include <memory>



class icString;

namespace icL::il {

class SourceOfCode;

class SourceServer
{
public:
    virtual ~SourceServer() = default;

    /**
     * @brief getSource gets a source object of
     * @param path is the path of file in project
     * @return a pointer to source or nullptr if no such file
     */
    virtual std::shared_ptr<SourceOfCode> getSource(const icString & path) = 0;

    /**
     * @brief closeSource closes and remove file from hash
     * @param path is the path to file on HDD
     */
    virtual void closeSource(const icString & path) = 0;
};

}  // namespace icL::il

#endif  // il_SourceServer
