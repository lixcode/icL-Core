#ifndef il_StatefulServer
#define il_StatefulServer

class icString;

namespace icL::il {

class StatefulServer
{
public:
    virtual ~StatefulServer() = default;

    /**
     * @brief registerSignal register a new signal
     * @param name is the name of new signal
     * @return false if so signal exists, otherwise true
     */
    virtual bool registerSignal(const icString & name) = 0;

    /**
     * @brief getSignal gets the integer code of signal
     * @param name is the name of signal
     * @return the integer code of signal, otherwise returns -1
     */
    virtual int getSignal(const icString & name) = 0;
};

}  // namespace icL::il

#endif  // il_StatefulServer
