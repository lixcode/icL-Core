#ifndef il_FileServer
#define il_FileServer

class icString;

namespace icL {

namespace memory {
struct Set;
struct Object;
}  // namespace memory

namespace il {

struct FileTarget;

/**
 * @brief The FileServer class is a server for working with files
 */
class FileServer
{
public:
    virtual ~FileServer() = default;

    /// `Files.create (path : string) : File`
    virtual FileTarget create(const icString & path) = 0;

    /// `Files.createDir (path : string) : void`
    virtual void createDir(const icString & path) = 0;

    /// `Files.createPath (path : string) : void`
    virtual void createPath(const icString & path) = 0;

    /// `Files.open (path : string) : File`
    virtual FileTarget open(const icString & path) = 0;

    /**
     * @brief pushTarget pushes a file target to stack
     * @param target is the file target to push
     */
    virtual void pushTarget(const FileTarget & target) = 0;

    /**
     * @brief popTarget pops the last pushed target
     */
    virtual void popTarget() = 0;

    /// `[r/w] File'format : int`
    virtual int getFormat() = 0;

    /// `File.close () : void`
    virtual void close() = 0;

    /// `File.delete () : void`
    virtual void delete_() = 0;

    /// `DSV.append (f : File, s : set) : File`
    virtual void append(const memory::Set & set) = 0;

    /// `DSV.append (f : File, obj : object) : File`
    virtual void append(const memory::Object & obj) = 0;

    /// `DSV.load (delimiter : string, f : File, base : set) : set`
    virtual memory::Set load(
      const icString & delimiter, const memory::Set & base) = 0;

    /// `DSV.loadCSV (f : File, base : set) : File`
    virtual memory::Set loadCSV(const memory::Set & base) = 0;

    /// `DSV.loadTSV (f : File, base : set) : File`
    virtual memory::Set loadTSV(const memory::Set & base) = 0;

    /// `DSV.sync (f : File, s : set) : set`
    virtual void sync(const memory::Set & set) = 0;

    /// `DSV.write (f : File, s : set) : File`
    virtual void write(const memory::Set & set) = 0;
};

}  // namespace il
}  // namespace icL

#endif  // il_FileServer
