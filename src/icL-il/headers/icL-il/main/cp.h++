#ifndef il_CP
#define il_CP

class icString;
template <typename>
class icList;

namespace icL::il {

struct CodeFragment;

class CP
{
public:
    virtual ~CP() = default;

    /**
     * @brief newSignal registers a new signal in command processor
     * @param name is the name of signal
     */
    virtual void newSignal(const icString & name) = 0;

    /**
     * @brief getSignal gets the code of signal
     * @param name is the name of signal
     * @return the code of signal
     */
    virtual int getSignal(const icString & name) = 0;

    /**
     * @brief splitCommands split code to commands
     * @param code is the code to split
     * @return a list of commands
     */
    virtual icList<CodeFragment> splitCommands(const CodeFragment & code) = 0;

    /**
     * @brief lastTokenPosition gets the postion of last token
     * @return "filename:line:symbol: "
     */
    virtual icString lastTokenPosition() = 0;

    // virtual inter::Flayer & getFlayer() = 0;
};

}  // namespace icL::il

#endif  // il_CP
