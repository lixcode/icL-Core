#ifndef il_Enums
#define il_Enums

namespace icL::il {

namespace Selectors {     ///< \brief available selectors for web elements
enum Selectors {          ///< \brief available selectors for web elements
    CssSelector     = 1,  ///< `css selector`
    LinkText        = 2,  ///< `link text`
    PartialLinkText = 3,  ///< `partial link text`
    TagName         = 4,  ///< `tag name`
    XPath           = 5,  ///< `xpath`
    Input           = 6,  ///< `input`
    Field           = 7   ///< `field`
};
}  // namespace Selectors

namespace Keys {  ///< \brief The namespace Keys contains the key modifiers
enum Keys {       ///< \brief The Keys enum enums key modifiers
    Ctrl  = 1,    ///< Control modifier
    Shift = 2,    ///< Shift modifier
    Alt   = 4     ///< Alt modifier
};
}  // namespace Keys

namespace MouseButtons {  ///< \brief mouse buttons
enum MouseButtons {       ///< \brief mouse buttons
    Left   = 1,           ///< the left button
    Middle = 2,           ///< the middle button
    Right  = 3            ///< the right button
};
}

namespace MoveTypes {  ///< \brief move types
enum MoveTypes {       ///< \brief move types
    Teleport  = 1,     ///< instant move
    Linear    = 2,     ///< linear intepolation move (x)
    Quadratic = 3,     ///< quadratic interpolation move (x*x)
    Cubic     = 4,     ///< qubic interpolation move (x*x*x)
    Bezier    = 5      ///< cubic bezier curve move
};
}

namespace FileTypes {  ///< \brief file types
enum FileTypes {       ///< \brief file types
    None = 1,          ///< none (invalid)
    CSV  = 1,          ///< .csv (CSV)
    TSV  = 2           ///< .tsv (TSV)
};
}

namespace Processes {  ///< \brief process modes of numbers
enum Processes {       ///< \brief process modes of numbers
    Sum     = 1,       ///< Sum (`a + b`)
    Product = 2,       ///< Product (`a * b`)
    Min     = 3,       ///< Min (`a < b ? a : b`)
    Max     = 4        ///< Max (`a > b ? a : b`)
};
}

}  // namespace icL::il

#endif  // il_Enums
