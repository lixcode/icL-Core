#ifndef il_SourceOfCode
#define il_SourceOfCode



class icChar;
class icString;

namespace icL::il {

class Position;

/**
 * @brief The SourceOfCode class represent a source of code (string, file, etc.)
 */
class SourceOfCode
{
public:
    virtual ~SourceOfCode() = default;

    /**
     * @brief getFilePath gets the file path for source
     * @return the file path
     */
    virtual icString getFilePath() = 0;

    /**
     * @brief fragment gets a source of ode object for a fragment
     * @param begin is the beginning of fragment
     * @param end is the end of fragemtn
     * @return a pointer to the new source of code
     */
    virtual SourceOfCode * fragment(
      const il::Position & begin, const il::Position & end) = 0;

    /**
     * @brief current returns the current symbol
     * @return the current symbol
     */
    virtual icChar current() = 0;

    /**
     * @brief next switches cursor position to next symbol
     * @return the next symbol
     */
    virtual icChar next() = 0;

    /**
     * @brief prev switches cursor position to previuos position
     * @return the previous symbol
     */
    virtual icChar prev() = 0;

    /**
     * @brief atEnd checkes the end of input file
     * @return true if the cursor is at end, otherwise false
     */
    virtual bool atEnd() = 0;

    /**
     * @brief seek move the postion of cursor
     * @param position is the needed potition
     * @return true if position is correct, otherwise false
     */
    virtual bool seek(il::Position position) = 0;

    /**
     * @brief getPostion gets the current position os cursor
     * @return the currenct position of cursor
     */
    virtual il::Position getPosition() = 0;
};

}  // namespace icL::il

#endif  // il_SourceOfCode
