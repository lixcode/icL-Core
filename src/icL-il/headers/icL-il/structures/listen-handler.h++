#ifndef il_ListenHandler
#define il_ListenHandler

#include <icL-types/replaces/ic-string.h++>

#include <memory>



class icVariant;

namespace icL::il {

/**
 * @brief The ListenerTarget struct represents a target to a listener
 */
struct ListenerTarget
{
    /// \brief target is a UUID
    icString uuid;
};

/**
 * @brief The HandlerTarget struct represents a target to a handler
 */
struct HandlerTarget
{
    /// \brief target is a UUID
    icString uuid;
};

/**
 * @brief The Listener struct is used to save a reference to a listener target
 * in icVariant
 */
struct Listener
{
    /// \brief target is a target to a listener
    std::shared_ptr<ListenerTarget> target;

    Listener() = default;
    Listener(const icVariant & value);

    /**
     * @brief operator icVariant casts a listener value to a variant
     */
    operator icVariant() const;

    /**
     * @brief operator == checks if 2 listeners are the same
     * @param other is the target to compare with
     * @return true if uuids are the same, otherwise false
     */
    bool operator==(Listener & other);
};

/**
 * @brief The Handler struct is used to save a reference to a handler target in
 * icVariant
 */
struct Handler
{
    /// \brief target is a targetto a handler
    std::shared_ptr<HandlerTarget> target;

    Handler() = default;
    Handler(const icVariant & value);

    /**
     * @brief operator icVariant casts a handler to a variant
     */
    operator icVariant() const;

    /**
     * @brief operator == checks is 2 handlers are the same
     * @param other is the target to compare with
     * @return true if uuids are the same, otherwise false
     */
    bool operator==(Handler & other);
};

}  // namespace icL::il

#endif  // il_ListenHandler
