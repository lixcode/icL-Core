#ifndef il_Element
#define il_Element

#include "target-data.h++"

#include <icL-types/replaces/ic-string-list.h++>

class icVariant;

namespace icL::il {

/**
 * @brief The Element struct is a list of web elements
 */
struct Element
{
    /// \brief data about the tab of element
    TargetData target;

    /// \brief variables is the links for each element in collection
    icString variable;

    /// \brief selector is the selector of this elements
    icString selector;

    Element() = default;

    /**
     * @brief Element casts a icVariant to Element
     * @param variant is the variant to cast
     */
    Element(const icVariant & variant);

    /**
     * @brief operator icVariant casts an Element value to a QVarint
     */
    operator icVariant() const;

    /**
     * @brief operator == checks if 2 elements have the same target
     * @param other is the element to compare with
     * @return true it they has the same target, otherwise false
     */
    bool operator==(const Element & other) const;
};

/**
 * @brief The Element struct is a list of web elements
 */
struct Elements : public icList<Element>
{
    Elements() = default;

    /**
     * @brief Element casts a icVariant to Element
     * @param variant is the variant to cast
     */
    Elements(const icVariant & variant);

    /**
     * @brief size returns the manual setted size
     * @return manual setted size
     */
    int size() const;

    /**
     * @brief setSize sets the size of element
     * @param size is the new size of element
     */
    void setSize(int size);

    /**
     * @brief operator icVariant casts an Element value to a QVarint
     */
    operator icVariant() const;

    /// \brief _size is the manual setted size
    int _size = 0;
};

}  // namespace icL::il

#endif  // il_Element
