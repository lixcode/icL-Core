#ifndef cp_Flayer_JS
#define cp_Flayer_JS

#include "i-flyer.h++"

#include <icL-service-main/values/inode.h++>


namespace icL::cp {

class JS
    : virtual public IFlyer
    , public service::INode
{
    JsFlyResult previous;

public:
    JS();

    // IFlyer interface
public:
    JsFlyResult jsFlyNext(bool packContexts = true) override;

private:
    /**
     * @brief flyToNext flies to next not-white character
     * @return the next non-white character or 0x0 at end of source
     */
    icChar flyToNext();

    /**
     * @brief finishContext finish a context parsing
     * @param result is the fly result to evaluate
     */
    void finishContext(JsFlyResult & result);

    void flySlash(JsFlyResult & result);
    void flyIdentifier(JsFlyResult & result);
    void flyString(JsFlyResult & result);
    void flyRoundOp(JsFlyResult & result);
    void flyRoundCl(JsFlyResult & result);
    void flySquareOp(JsFlyResult & result);
    void flySquareCl(JsFlyResult & result);
    void flyCurlyOp(JsFlyResult & result);
    void flyCurlyCl(JsFlyResult & result);
    void flyCommandEnd(JsFlyResult & result);
    void flyToken(JsFlyResult & result);
};

}  // namespace icL::cp

#endif  // cp_Flayer_JS
