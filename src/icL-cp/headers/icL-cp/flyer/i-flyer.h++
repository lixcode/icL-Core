#ifndef cp_IFlyer
#define cp_IFlyer

#include "../result/structs.h++"

#include <icL-il/auxiliary/source-of-code.h++>

#include <memory>


namespace icL::cp {

struct IFlyer
{
    /// virtual desctructor of interface
    virtual ~IFlyer() = default;

    /**
     * @brief source get the source of code
     * @return the souce of code
     */
    virtual std::unique_ptr<il::SourceOfCode> & source() = 0;

    /**
     * @brief flyNext flies the next token
     * @return the result of fly
     */
    virtual FlyResult flyNext(
      bool packContexts = true, bool skipErrors = false) = 0;

    /**
     * @brief jsFlyNext flies the next js token
     * @return the result of fly
     */
    virtual JsFlyResult jsFlyNext(bool packContexts = true) = 0;

    /**
     * @brief issue generate a string of format line:relative
     * @param pos is the postion to evaluate
     * @return a string of format line:relative:
     */
    virtual icString issue(const il::Position & pos) const = 0;

    /**
     * @brief issueWithPath a string of format file:line:relative
     * @param pos is the postion to evaluate
     * @return a string of format file:line:relative:
     */
    virtual icString issueWithPath(const il::Position & pos) const = 0;
};

}  // namespace icL::cp

#endif  // cp_IFlyer
