#ifndef cp_SourceServer
#define cp_SourceServer

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/main/node.h++>
#include <icL-il/main/source-server.h++>



namespace icL::cp {

class SourceServer
    : public il::SourceServer
    , public il::Node
{
    icObject<icString, std::shared_ptr<il::SourceOfCode>> sources;

public:
    SourceServer(il::InterLevel * il);

    // SourceServer interface
public:
    std::shared_ptr<il::SourceOfCode> getSource(const icString & path) override;
    void closeSource(const icString & path) override;
};

}  // namespace icL::cp

#endif  // cp_SourceServer
