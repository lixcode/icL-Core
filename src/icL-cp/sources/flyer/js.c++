#include "js.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>



namespace icL::cp {

JS::JS() {
    previous.type = JsResultType::CurlyCl;
}

JsFlyResult JS::jsFlyNext(bool packContexts) {
    static const icObject<icChar, void (JS::*)(JsFlyResult &)> proxies{
      {'/', &JS::flySlash},     {'"', &JS::flyString},  {'\'', &JS::flyString},
      {'(', &JS::flyRoundOp},   {')', &JS::flyRoundCl}, {'[', &JS::flySquareOp},
      {']', &JS::flySquareCl},  {'{', &JS::flyCurlyOp}, {'}', &JS::flyCurlyCl},
      {';', &JS::flyCommandEnd}};

    source()->seek(source()->getPosition());

    JsFlyResult result;
    icChar      firstChar = flyToNext();
    auto        proxy     = proxies.find(firstChar);

    result.begin = source()->getPosition();
    result.begin.relative -= 1;
    result.begin.absolute -= 1;

    if (proxy != proxies.end()) {
        (this->**proxy)(result);
    }
    else if (firstChar.isLetterOrDigit()) {
        flyIdentifier(result);
    }
    else if (firstChar != '\0') {
        flyToken(result);
    }

    if (packContexts) {
        if (
          result.type == JsResultType::RoundOp ||
          result.type == JsResultType::SquareOp ||
          result.type == JsResultType::CurlyOp) {
            finishContext(result);
        }
    }

    result.end = source()->getPosition();
    previous   = result;

    return result;
}

icChar JS::flyToNext() {
    icChar next = '\0';

    if (source()->atEnd()) {
        return next;
    }

    do {
        next = source()->next();
    } while (next.isWhite() && !source()->atEnd());

    return next.isWhite() ? icChar{'\0'} : next;
}

void JS::finishContext(JsFlyResult & result) {
    struct Match
    {
        JsResultType expected;
        icChar       ch;
        il::Position position;
    };

    icList<Match> matches;
    JsFlyResult   current = result;

    result.data.insert("begin", source()->getPosition());

    while (true) {
        switch (current.type) {
        case JsResultType::RoundOp:
            matches.append({JsResultType::RoundCl, '(', current.begin});
            break;

        case JsResultType::SquareOp:
            matches.append({JsResultType::SquareCl, '[', current.begin});
            break;

        case JsResultType::CurlyOp:
            matches.append({JsResultType::CurlyCl, '{', current.begin});
            break;

        case JsResultType::RoundCl:
        case JsResultType::SquareCl:
        case JsResultType::CurlyCl:
            if (matches.last().expected == current.type) {
                matches.removeLast();
            }
            else {
                auto & last = matches.last();
                _il()->vm->cp_sig(
                  source()->getFilePath() % ": Missmatched brackets: " %
                  last.ch % "( line " % issue(last.position) % ')' % " & " %
                  current.key % "( line " % issue(source()->getPosition()) %
                  ')');
            }
            break;

        default: /* do nothing */;
        }

        if (matches.isEmpty() || source()->atEnd()) {
            break;
        }

        current = jsFlyNext(false);
    }

    if (!matches.isEmpty()) {
        _il()->vm->cp_sig(
          issueWithPath(matches.first().position) % ": Bracket pair not found");
    }

    result.data.insert("end", current.begin);
};

void JS::flySlash(JsFlyResult & result) {
    icChar second = source()->next();

    if (second == '/') {
        while (source()->next() != '\n' && !source()->atEnd())
            ;

        result.key  = "\n";
        result.type = JsResultType::Comment;
    }
    else if (second == '*') {
        while (source()->current() != '*' && source()->next() != '/' &&
               !source()->atEnd())
            ;
        result.type = JsResultType::Comment;
    }
    else {
        auto position = source()->getPosition();

        while (!source()->next().isWhite() && source()->current() != '/' &&
               source()->atEnd())
            ;

        if (source()->current() == '/') {
            result.type = JsResultType::String;
        }
        else {
            source()->seek(position);
        }
    }
}

void JS::flyIdentifier(JsFlyResult & result) {
    while ((source()->next().isLetterOrDigit() || source()->current() == '$' ||
            source()->current() == '_') &&
           !source()->atEnd())
        ;
    result.type = JsResultType::Identifier;
}

void JS::flyString(JsFlyResult & result) {
    icChar end = source()->current();

    while (source()->next() != end && !source()->atEnd()) {
        if (source()->current() == '\\') {
            source()->next();
        }
        else if (source()->current() == '\n') {
            _il()->vm->cp_sig(
              issueWithPath(result.begin) % "Expected " % end %
              " instead of \\n");
            break;
        }
    }

    if (source()->atEnd() && source()->current() != end) {
        _il()->vm->cp_sig(
          issueWithPath(result.begin) % "End of string not found");
    }
}

void JS::flyRoundOp(JsFlyResult & result) {
    result.type = JsResultType::RoundOp;
}

void JS::flyRoundCl(JsFlyResult & result) {
    result.type = JsResultType::RoundCl;
}

void JS::flySquareOp(JsFlyResult & result) {
    result.type = JsResultType::SquareOp;
}

void JS::flySquareCl(JsFlyResult & result) {
    result.type = JsResultType::SquareCl;
}

void JS::flyCurlyOp(JsFlyResult & result) {
    result.type = JsResultType::CurlyOp;
}

void JS::flyCurlyCl(JsFlyResult & result) {
    result.type = JsResultType::CurlyCl;
}

void JS::flyCommandEnd(JsFlyResult & result) {
    result.type = JsResultType::CommandEnd;
}

void JS::flyToken(JsFlyResult & result) {
    result.key  = source()->current();
    result.type = JsResultType::Token;
}

}  // namespace icL::cp
