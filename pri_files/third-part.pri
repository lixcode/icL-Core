include($$ICL_ROOT/pri_files/lib.pri)

CONFIG += static

LIBS += -static-libstdc++ -static -static-libgcc

DESTDIR = $$ICL_ROOT/bin/$$BUILDTYPE/$$OS/lib

unix {
    target.path = $$ICL_ROOT/bin/$$BUILDTYPE/$$OS/lib
}
